<?php

namespace backend\controllers;

use Yii;
use common\models\Article;
use backend\models\ArticleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				]
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * @return array
	 */
	public function actions()
	{
		return [
			'image-upload' => [
				'class' => 'vova07\imperavi\actions\UploadFileAction',
				'url' => Yii::$app->params['staticDomain'] . 'images/',
				'path' => '@static/web/images/',
			],
			'images-get' => [
				'class' => 'vova07\imperavi\actions\GetImagesAction',
				'url' => Yii::$app->params['staticDomain'] . 'images/',
				'path' => '@static/web/images/',
				'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']],
			],
			'file-upload' => [
				'class' => 'vova07\imperavi\actions\UploadFileAction',
				'url' => Yii::$app->params['staticDomain'] . 'files/',
				'path' => '@static/web/files/',
				'uploadOnlyImage' => false,
				'unique' => false,
				'replace' => true,
				'translit' => true
			],
			'files-get' => [
				'class' => 'vova07\imperavi\actions\GetFilesAction',
				'url' => Yii::$app->params['staticDomain'] . 'files/',
				'path' => '@static/web/files/',
				// 'options' => ['only' => ['*.txt', '*.md']], // These options are by default.
			],
			'file-delete' => [
				'class' => 'vova07\imperavi\actions\DeleteFileAction',
				'url' => Yii::$app->params['staticDomain'] . 'files/', // Directory URL address, where files are stored.
				'path' => '@static/web/files/', // Or absolute path to directory where files are stored.
			]
		];
	}

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

	    foreach (Yii::$app->request->post('ArticleTranslation', []) as $language => $data) {
		    foreach ($data as $attribute => $translation) {
			    $model->translate($language)->$attribute = $translation;
		    }
	    }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

	    foreach (Yii::$app->request->post('ArticleTranslation', []) as $language => $data) {
		    foreach ($data as $attribute => $translation) {
			    $model->translate($language)->$attribute = $translation;
		    }
	    }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('article', 'The requested page does not exist.'));
    }
}
