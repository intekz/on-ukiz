<?php

namespace backend\controllers;

use common\service\api\ApiSender;
use Yii;
use common\models\News;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use backend\models\search\NewsSearch;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'img-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@static/temp/'
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        foreach (Yii::$app->request->post('NewsTranslation', []) as $language => $data) {
            foreach ($data as $attribute => $translation) {
                $model->translate($language)->$attribute = $translation;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
        	$model->user_id = Yii::$app->user->identity->getId();
            $model->save();

            $config = [
                'email' => Yii::$app->params['apiAuthEmail'],
                'pass' => Yii::$app->params['apiAuthPass'],
                'method' => 'POST',
                'url' => 'news'
            ];

            try {
				$api = new ApiSender($model->toArray(), $config);
				$api->send();
			} catch (\Exception $e) {
            	Yii::$app->session->setFlash('error', $e->getMessage());
			}

            return $this->redirect(['index']);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        foreach (Yii::$app->request->post('NewsTranslation', []) as $language => $data) {
            foreach ($data as $attribute => $translation) {
                $model->translate($language)->$attribute = $translation;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $config = [
                'email' => Yii::$app->params['apiAuthEmail'],
                'pass' => Yii::$app->params['apiAuthPass'],
                'method' => 'PATCH',
                'url' => 'news/' . $model->id
            ];

            $api = new ApiSender($model->toArray(), $config);
            $api->send();

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        $config = [
            'email' => Yii::$app->params['apiAuthEmail'],
            'pass' => Yii::$app->params['apiAuthPass'],
            'method' => 'DELETE',
            'url' => 'news/' . $model->id
        ];

        $api = new ApiSender(null, $config);
        $api->send();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
