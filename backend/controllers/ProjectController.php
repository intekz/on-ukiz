<?php

namespace backend\controllers;

use common\models\ProjectDoc;
use common\models\ProjectImage;
use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\Project;
use yii\filters\AccessControl;
use backend\models\ProjectSearch;
use yii\web\NotFoundHttpException;
use common\service\api\ApiSender;
use yii\web\UploadedFile;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				]
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * @return array
	 */
	public function actions()
	{
		return [
			'image-upload' => [
				'class' => 'vova07\imperavi\actions\UploadFileAction',
				'url' => Yii::$app->params['staticDomain'] . 'images/',
				'path' => '@static/web/images/',
			],
			'images-get' => [
				'class' => 'vova07\imperavi\actions\GetImagesAction',
				'url' => Yii::$app->params['staticDomain'] . 'images/',
				'path' => '@static/web/images/',
				'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']],
			],
			'file-upload' => [
				'class' => 'vova07\imperavi\actions\UploadFileAction',
				'url' => Yii::$app->params['staticDomain'] . 'files/',
				'path' => '@static/web/files/',
				'uploadOnlyImage' => false,
				'unique' => false,
				'replace' => true,
				'translit' => true
			],
			'files-get' => [
				'class' => 'vova07\imperavi\actions\GetFilesAction',
				'url' => Yii::$app->params['staticDomain'] . 'files/',
				'path' => '@static/web/files/',
				// 'options' => ['only' => ['*.txt', '*.md']], // These options are by default.
			],
			'file-delete' => [
				'class' => 'vova07\imperavi\actions\DeleteFileAction',
				'url' => Yii::$app->params['staticDomain'] . 'files/', // Directory URL address, where files are stored.
				'path' => '@static/web/files/', // Or absolute path to directory where files are stored.
			]
		];
	}

	/**
	 * Lists all Project models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ProjectSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * @return string|\yii\web\Response
	 * @throws \Exception
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	public function actionCreate()
	{
		$model = new Project();

		foreach (Yii::$app->request->post('ProjectTranslation', []) as $language => $data) {
			foreach ($data as $attribute => $translation) {
				$model->translate($language)->$attribute = $translation;
			}
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->saveImages($model);

			$config = [
				'email' => Yii::$app->params['apiAuthEmail'],
				'pass' => Yii::$app->params['apiAuthPass'],
				'method' => 'POST',
				'url' => 'projects'
			];

			$api = new ApiSender($model->toArray(), $config);
			$api->send();

			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Project model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		foreach (Yii::$app->request->post('ProjectTranslation', []) as $language => $data) {
			foreach ($data as $attribute => $translation) {
				$model->translate($language)->$attribute = $translation;
			}
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->saveImages($model);
			$config = [
				'email' => Yii::$app->params['apiAuthEmail'],
				'pass' => Yii::$app->params['apiAuthPass'],
				'method' => 'PATCH',
				'url' => 'projects/' . $model->id
			];

			$api = new ApiSender($model->toArray(), $config);
			$api->send();

			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * @param $id
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->delete();

		$config = [
			'email' => Yii::$app->params['apiAuthEmail'],
			'pass' => Yii::$app->params['apiAuthPass'],
			'method' => 'DELETE',
			'url' => 'projects/' . $model->id
		];

		$api = new ApiSender(null, $config);
		$api->send();

		return $this->redirect(['index']);
	}

	/**
	 * @return null|\yii\web\Response
	 */
	public function actionUploadImage()
	{
		if ($image = UploadedFile::getInstanceByName('project-image')) {
			$path = Yii::getAlias('@static/temp/');
			$name = uniqid() . '.' . $image->extension;
			$target = $path . '/' . $name;
			$status = $image->saveAs($target);

			$session = Yii::$app->session;
			$images = $session->get('project-images', []);
			$images[] = $name;
			$session->set('project-images', $images);
			return $status ? $this->asJson(['status' => $status]) : null;
		}
	}

	/**
	 * @param $id
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDeleteImg($id)
	{
		$model = ProjectImage::findOne($id);
		if (!$model) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		$model->delete();

		return $this->asJson(['status' => true]);
	}

	public function actionUploadDocs($id)
	{
		$model = $this->findModel($id);
		$docs = new ProjectDoc();

		if ($docs->load(Yii::$app->request->post())) {

		}
	}

	/**
	 * @param Project $model
	 * @throws \Exception
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	protected function saveImages(Project $model)
	{
		$session = Yii::$app->session;
		$images = $session->get('project-images', []);

		$transaction = Yii::$app->db->beginTransaction();

		try {

			$imagesToInsert = [];

			foreach ($images as $key => $image) {
				$image_path = Yii::getAlias('@static/temp/' . $image);
				$savePath = Yii::getAlias('@static/web/project/' . $model->id);
				FileHelper::createDirectory($savePath);
				$project_path = Yii::getAlias('@static/web/project/' . $model->id . '/' . $image);
				copy($image_path, $project_path);

				$imagesToInsert[] = [
					'project_id' => $model->id,
					'name' => $image
				];
			}

			ProjectImage::getDb()
				->createCommand()
				->batchInsert(ProjectImage::tableName(), ['project_id', 'name'], $imagesToInsert)
				->execute();

			$session->remove('project-images');
			$transaction->commit();

		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	/**
	 * Finds the Project model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Project the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Project::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}