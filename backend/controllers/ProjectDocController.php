<?php

namespace backend\controllers;

use Yii;
use common\models\Project;
use common\models\ProjectDoc;
use backend\models\ProjectDocSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

/**
 * ProjectDocController implements the CRUD actions for ProjectDoc model.
 */
class ProjectDocController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				]
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * @return array
	 */
	public function actions()
	{
		return [
			'img-upload' => [
				'class' => FileAPIUpload::className(),
				'path' => '@static/temp/',
				'uploadOnlyImage' => false,
			],
		];
	}

	/**
	 * Lists all ProjectDoc models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ProjectDocSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * @param $projectId
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionCreate($projectId)
	{
		$model = new ProjectDoc();
		$tender = $this->findProject($projectId);
		$model->project_id = $tender->id;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['/project/index']);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing ProjectDoc model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['/project/index']);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * @param $id
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['/project/index']);
	}

	/**
	 * Finds the ProjectDoc model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return ProjectDoc the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = ProjectDoc::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException(Yii::t('tender', 'The requested page does not exist.'));
	}

	/**
	 * @param $id
	 * @return null|Project
	 * @throws NotFoundHttpException
	 */
	protected function findProject($id)
	{
		if ((!$model = Project::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException(Yii::t('tender', 'The requested page does not exist.'));
	}
}