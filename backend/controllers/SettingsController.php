<?php
/**
 * Created by PhpStorm.
 * User: ssdd
 * Date: 8/25/17
 * Time: 8:38 AM
 */

namespace backend\controllers;


use backend\forms\PasswordChangeForm;

use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Class SettingsController
 * @package backend\controllers
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionPasswordChange()
    {
        $user = $this->findModel();
        $model = new PasswordChangeForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            Yii::$app->session->setFlash('success', Yii::t('user', 'Password change success'));
            return $this->redirect(['password-change']);
        }

        return $this->render('password-change', [
            'model' => $model
        ]);
    }

    /**
     * @return User laod model
     */
    private function findModel()
    {
        return User::findOne(Yii::$app->user->identity->getId());
    }
}