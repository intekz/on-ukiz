<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Tender;
use yii\filters\VerbFilter;
use common\models\TenderDoc;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use backend\models\TenderDocSearch;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

/**
 * TenderDocController implements the CRUD actions for TenderDoc model.
 */
class TenderDocController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'img-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@static/temp/',
                'uploadOnlyImage' => false,
            ],
        ];
    }

    /**
     * Lists all TenderDoc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TenderDocSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $tenderId
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($tenderId)
    {
        $model = new TenderDoc();
        $tender = $this->findTender($tenderId);
        $model->tender_id = $tender->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/tender/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'tender' => $tender
        ]);
    }

    /**
     * Updates an existing TenderDoc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/tender/index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/tender/index']);
    }

    /**
     * Finds the TenderDoc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TenderDoc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TenderDoc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('tender', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     * @return null|Tender
     * @throws NotFoundHttpException
     */
    protected function findTender($id)
    {
        if ((!$model = Tender::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('tender', 'The requested page does not exist.'));
    }
}
