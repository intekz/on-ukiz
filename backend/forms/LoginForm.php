<?php
namespace backend\forms;

use Yii;
use yii\base\Model;
use common\models\User;
use common\components\rbac\Role;

/**
 * Class LoginForm
 * @package backend\models
 */
class LoginForm extends Model
{
    public $email;
    public $password;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // email validated by validateRole
            // ['email', 'validateRole']
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', Yii::t('user', 'ERROR_WRONG_USERNAME_OR_PASSWORD'));
            } elseif ($user && $user->status == User::STATUS_BANNED) {
                $this->addError('email', Yii::t('user', 'ERROR_ACCOUNT_BLOCKED'));
            } elseif ($user && $user->status == User::STATUS_INACTIVE) {
                $this->addError('email', Yii::t('user', 'ERROR_ACCOUNT_NOT_CONFIRMED'));
            } elseif ($user && $user->role !== Role::ROLE_ADMIN) {
                $this->addError('email', Yii::t('user', 'ERROR_WRONG_USERNAME_OR_PASSWORD'));
            }
        }
    }

    /**
     * @param $attribute
     */
    public function validateRole($attribute)
    {
        if(!$this->hasErrors()) {
            $user = $this->getUser();
            if($user && !Yii::$app->authManager->checkAccess($user->id, Role::PERMISSION_DASHBOARD_PANEL)) {
                $this->addError('password', Yii::t('user', 'ERROR_ACCESS_TO_DASHBOARD'));
            }
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        } else {
            return false;
        }
    }
}
