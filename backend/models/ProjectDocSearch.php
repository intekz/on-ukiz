<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProjectDoc;

/**
 * ProjectDocSearch represents the model behind the search form of `common\models\ProjectDoc`.
 */
class ProjectDocSearch extends ProjectDoc
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'project_id', 'sort'], 'integer'],
			[['name', 'language', 'file'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = ProjectDoc::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'project_id' => $this->project_id,
			'sort' => $this->sort,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'language', $this->language])
			->andFilterWhere(['like', 'file', $this->file]);

		return $dataProvider;
	}
}