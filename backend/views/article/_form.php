<?php

use alexssdd\dashboard\widgets\Box;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#tab-1"><i class="fa fa-sliders" aria-hidden="true"></i> Основные</a>
        </li>
        <li>
            <a data-toggle="tab" href="#tab-2"><i class="fa fa-globe" aria-hidden="true"></i> Переводы</a>
        </li>
    </ul>
    <?php $box = Box::begin([
        'title' => $this->title,
        'renderBody' => false
    ])?>

    <?php $form = ActiveForm::begin(); ?>
    <?= $box->beginBody();?>
    <div class="tab-content">
        <div id="tab-1" class="tab-pane active">
	        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

	        <?= $form->field($model, 'text')->widget(Imperavi::className(), [
		        'settings' => [
			        'lang' => 'ru',
			        'replaceDivs' => false,
			        'minHeight' => 200,
			        'fileUpload' => Url::to(['file-upload']),
			        'fileManagerJson' => Url::to(['files-get']),
			        'imageUpload' => Url::to(['image-upload']),
			        'imageManagerJson' => Url::to(['images-get']),
			        'plugins' => [
				        'clips',
				        'fullscreen',
				        'fontcolor',
				        'fontsize',
				        'table',
				        'filemanager',
				        'imagemanager',
				        'video',
			        ],
		        ],
            ]) ?>

	        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

	        <?= $form->field($model, 'meta_desc')->textarea(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-md-4">
		            <?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>
                </div>
            </div>
        </div>
        <div id="tab-2" class="tab-pane">
	        <?php foreach (['kz', 'en'] as $language): ?>
                <div class="row">
                    <div class="col-md-12">
				        <?= $form->field($model->translate($language), "[$language]title")->textInput() ?>
				        <?= $form->field($model->translate($language), "[$language]text")->widget(Imperavi::className(), [
					        'settings' => [
						        'lang' => 'ru',
						        'replaceDivs' => false,
						        'minHeight' => 200,
						        'fileUpload' => Url::to(['file-upload']),
						        'fileManagerJson' => Url::to(['files-get']),
						        'imageUpload' => Url::to(['image-upload']),
						        'imageManagerJson' => Url::to(['images-get']),
						        'plugins' => [
							        'clips',
							        'fullscreen',
							        'fontcolor',
							        'fontsize',
							        'table',
							        'filemanager',
							        'imagemanager',
							        'video',
						        ],
					        ],
                        ]) ?>
				        <?= $form->field($model->translate($language), "[$language]meta_title")->textInput() ?>
				        <?= $form->field($model->translate($language), "[$language]meta_desc")->textarea() ?>
                    </div>
                </div>
                <br>
                <br>
	        <?php endforeach; ?>
        </div>
    </div>
    <?= $box->endBody();?>

    <?= $box->beginFooter();?>
        <?= Html::submitButton(Yii::t('article', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= $box->endFooter()?>

    <?php ActiveForm::end(); ?>
    <?php Box::end()?>
</div>
