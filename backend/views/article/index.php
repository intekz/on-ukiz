<?php

use alexssdd\dashboard\widgets\Box;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('site', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">
    <?php Box::begin([
        'title' => $this->title,
        'buttonsTemplate' => '{create}'
    ]);?>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'slug',
            //'text:ntext',
            //'meta_title',
            //'meta_desc',
            //'meta_keywords',
            //'og_title',
            //'og_desc',
            //'og_img',
            //'og_type',
            //'og_url:url',
            'status',
            //'created_at',
            //'updated_at',

            ['class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    <?php Box::end(); ?>
</div>
