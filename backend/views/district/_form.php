<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use alexssdd\dashboard\widgets\Box;

/* @var $this yii\web\View */
/* @var $model common\models\District */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-9">
        <div class="district-form">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab-1"><i class="fa fa-sliders" aria-hidden="true"></i> Основные</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2"><i class="fa fa-globe" aria-hidden="true"></i> Переводы</a>
                </li>
            </ul>

			<?php $box = Box::begin([
				'title' => $this->title,
				'buttonsTemplate' => '{cancel}',
				'renderBody' => false
			]) ?>

			<?php $form = ActiveForm::begin(); ?>

			<?php $box->beginBody() ?>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
					<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'sort')->textInput(['readOnly' => true]) ?>
                </div>
                <div id="tab-2" class="tab-pane">
					<?php foreach (['kz', 'en'] as $language): ?>
                        <div class="row">
                            <div class="col-md-10">
								<?= $form->field($model->translate($language), "[$language]name")->textInput() ?>
                            </div>
                        </div>
					<?php endforeach; ?>
                </div>
            </div>
			<?php $box->endBody() ?>

			<?php $box->beginFooter() ?>
				<?= Html::submitButton($model->isNewRecord ? Yii::t('district', 'Create') : Yii::t('district', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			<?php $box->endFooter() ?>

			<?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
