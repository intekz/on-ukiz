<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use alexssdd\dashboard\widgets\Box;

/* @var $this yii\web\View */
/* @var $model common\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">
    <div class="row">
        <div class="col-md-10">
            <?php $box = Box::begin([
                'title' => $this->title,
                'buttonsTemplate' => '{cancel}',
                'renderBody' => false
            ]) ?>

            <?php $form = ActiveForm::begin(); ?>

            <?php $box->beginBody() ?>

            <div class="row">
                <div class="col-md-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'readOnly' => true]) ?></div>
                <div class="col-md-4"><?= $form->field($model, 'city')->textInput(['maxlength' => true, 'readOnly' => true]) ?></div>
                <div class="col-md-4"><?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readOnly' => true]) ?></div>
            </div>

            <?= $form->field($model, 'message')->textarea(['maxlength' => true, 'readOnly' => true, 'rows' => 6]) ?>

            <div class="row">
                <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?></div>
            </div>

            <?php $box->endBody() ?>

            <?php $box->beginFooter() ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('feedback', 'Create') : Yii::t('feedback', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php $box->endFooter() ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
