<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use common\models\Feedback;
use alexssdd\dashboard\widgets\Box;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('feedback', 'Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">

    <?php Box::begin([
        'title' => $this->title,
    ]); ?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            // 'city',
            'email:email',
            // 'message',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function (Feedback $model) {
                    if ($model->status === $model::STATUS_NEW) {
                        $class = 'label-primary';
                    } elseif ($model->status === $model::STATUS_PROCESSED) {
                        $class = 'label-success';
                    }
                    return Html::tag('span', $model->getStatusLabel(), ['class' => 'label ' . $class]);
                },
                'filter' => Feedback::getStatuses()
            ],
            [
                'attribute'=>'created_at',
                'format'=>['DateTime','php:d.m.Y H:i']
            ],
            // 'updated_at',

            ['class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <?php Box::end()?>
</div>