<?php

use yii\widgets\Pjax;
use alexssdd\dashboard\widgets\Box;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IndustrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('industry', 'Industries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="industry-index">
<?php Box::begin([
    'title' => $this->title,
    'buttonsTemplate' => '{create}'
]); ?>
<?php Pjax::begin(); ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'sort',

            ['class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
<?php Box::end()?>
</div>
