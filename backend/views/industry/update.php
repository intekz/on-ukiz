<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Industry */

$this->title = Yii::t('industry', 'Update {modelClass}: ', [
    'modelClass' => 'Industry',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('industry', 'Industries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('industry', 'Update');
?>
<div class="industry-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
