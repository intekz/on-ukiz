<?php
use backend\widgets\Sidebar;

?>

<?= Sidebar::widget([
	'options' => ['class' => 'main-menu__list'],
	'itemOptions' => ['class' => 'main-menu__item',],
	'linkTemplate' => '<a class="main-menu__link" href="{url}">{icon} {label}</a>',
	'activeCssClass' => 'main-menu__item_active',
	'items' => [
		[
			'label' => Yii::t('app', 'Site'),
			'options' => [
				'class' => 'main-menu__item main-menu__item_title'
			],
		],
		[
			'label' => Yii::t('app', 'Slider'),
			'url' => ['/slider/index'],
			'icon' => 'far fa-images',
		],
        [
			'label' => Yii::t('app', 'Tender'),
			'url' => ['/tender/index'],
			'icon' => 'fa-shopping-basket aria-hidden="true"',
		],
        [
			'label' => Yii::t('app', 'Projects'),
			'url' => ['/project/index'],
			'icon' => 'fa-list',
		],
		[
			'label' => Yii::t('app', 'News'),
			'url' => ['/news/index'],
			'icon' => 'far fa-newspaper',
		],
		[
			'label' => Yii::t('app', 'Articles'),
			'url' => ['/article/index'],
			'icon' => 'fa-list'
		],

		/*[
			'label' => Yii::t('app', 'Text'),
			'url' => Url::to('javascript:void(0)'),
			'icon' => 'fa-compress aria-hidden="true"',
			'items' => [
				[
					'label' => Yii::t('app', 'Text'),
					'url' => ['/url/to-page'],
					'icon' => 'fa-angle-right',
				],
			]
		],*/
		[
			'label' => Yii::t('app', 'Others'),
			'options' => [
				'class' => 'main-menu__item main-menu__item_title'
			],
		],
		[
			'label' => Yii::t('app', 'Feedback'),
			'url' => ['/feedback/index'],
			'icon' => 'fa-comment',
		],
		[
			'label' => Yii::t('app', 'District'),
			'url' => ['/district/index'],
			'icon' => 'fa-globe',
		],
		[
			'label' => Yii::t('app', 'Industry'),
			'url' => ['/industry/index'],
			'icon' => 'fa-industry',
		]
	]
]);