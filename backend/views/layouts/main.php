<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="theme-color" content="#2B2E33">
	<?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <header class="header">
        <div class="header__layout">
            <div class="header__logo">
                <a class="logo" href="/"><?= Yii::$app->name ?></a>
            </div>
            <div class="header-nav clearfix">
                <div style="overflow: hidden; width: auto; max-height: none">
                    <button type="button" id="aside-toggle">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="nav">
                        <li>
							<?= Html::a(Yii::t('app', 'Logout') . '(' . Yii::$app->user->identity->email . ')', ['/site/logout'], ['data-method' => 'post'])?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <aside class="aside">
        <nav class="main-menu">
			<?= $this->render('_aside')?>
        </nav>
        <div class="aside-footer-menu">
            <ul class="aside-footer-menu__list">
                <li class="aside-footer-menu__item">
                    <a class="aside-footer-menu__link" href="<?= Url::to(['/settings/index']) ?>"><i class="fa fa-cogs"></i></a>
                </li>
                <li class="aside-footer-menu__item">
                    <a class="aside-footer-menu__link aside-footer-menu__link_dropblock" href="javascript:void(0)"><i class="fa fa-question-circle"></i></i></a>
                    <div class="aside-footer-menu__dropblock">
                        <div>тел: +7(702)721-21-21</div>
                    </div>
                </li>
                <li class="aside-footer-menu__item">
                    <a class="aside-footer-menu__link aside-footer-menu__link_dropblock" href="javascript:void(0)"><i class="fa fa-copyright"></i></a>
                    <div class="aside-footer-menu__dropblock">
                        Made in <a class="copyright__link" href="http://inte.kz" target="_blank">Intellect</a> 2012 - <?= date('Y') ?>
                    </div>
                </li>
                <li class="aside-footer-menu__item">
                    <a class="aside-footer-menu__link" href="<?= Url::to(['/site/logout']) ?>" data-method="post">
                        <i class="fa fa-power-off"></i>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
    <main class="main" role="main" id="admin-body">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>
		<?= $content ?>
    </main>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
