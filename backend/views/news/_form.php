<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use alexssdd\dashboard\widgets\Box;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-10">
        <div class="news-form">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab-1"><i class="fa fa-sliders" aria-hidden="true"></i> Основные</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2"><i class="fa fa-globe" aria-hidden="true"></i> Переводы</a>
                </li>
            </ul>

            <?php $box = Box::begin([
                'title' => $this->title,
                'buttonsTemplate' => '{cancel}',
                'renderBody' => false
            ]) ?>

            <?php $form = ActiveForm::begin(); ?>
            <?php $box->beginBody() ?>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-10"> <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-10"> <?= $form->field($model, 'desc')->textarea(['maxlength' => true, 'rows' => 6]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <?= $form->field($model, 'text')->widget(TinyMce::className(), [
                                'options' => ['rows' => 6],
                                'language' => 'ru',
                                'clientOptions' => [
                                    'plugins' => [
                                        "advlist autolink lists link charmap print preview anchor",
                                        "searchreplace visualblocks code fullscreen",
                                        "insertdatetime media table contextmenu paste"
                                    ],
                                    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                ]
                            ]);?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"> <?= $form->field($model, 'img')->widget(FileAPI::className(), ['settings' => ['url' => ['img-upload']]]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'fixed')->dropDownList(['Нет', 'Да'])?>
                        </div>
                        <div class="col-md-4">
							<?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <?php foreach (['kz', 'en'] as $language): ?>
                    <div class="row">
                        <div class="col-md-10">
                            <?= $form->field($model->translate($language), "[$language]title")->textInput(); ?>
                            <?= $form->field($model->translate($language), "[$language]desc")->textarea(); ?>
                            <?= $form->field($model->translate($language), "[$language]text")->widget(TinyMce::className(), [
                                'options' => ['rows' => 6],
                                'language' => 'ru',
                                'clientOptions' => [
                                    'plugins' => [
                                        "advlist autolink lists link charmap print preview anchor",
                                        "searchreplace visualblocks code fullscreen",
                                        "insertdatetime media table contextmenu paste"
                                    ],
                                    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                ]
                            ]);?>
                        </div>
                    </div>
                    <br>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php $box->endBody() ?>

            <?php $box->beginFooter() ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('slider', 'Create') : Yii::t('slider', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?php $box->endFooter() ?>

            <?php ActiveForm::end(); ?>
            <?php Box::end() ?>
        </div>
    </div>
</div>
