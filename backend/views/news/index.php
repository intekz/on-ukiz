<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use common\models\News;
use alexssdd\dashboard\widgets\Box;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('news', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <?php Box::begin([
            'title' => $this->title,
            'buttonsTemplate' => '{create}'
        ])?>
        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                'title',
                // 'image',
                // 'slug',
                [
                    'attribute' => 'status',
                    'format' => 'html',
                    'value' => function (News $model) {
                        if ($model->status === $model::STATUS_PUBLISHED) {
                            $class = 'label-primary';
                        } elseif ($model->status === $model::STATUS_NOT_PUBLISHED) {
                            $class = 'label-danger';
                        }
                        return Html::tag('span', $model->getStatusLabel(), ['class' => 'label ' . $class]);
                    },
                    'filter' => News::getStatuses()
                ],
                [
                    'attribute'=>'created_at',
                    'format'=>['DateTime','php:d-m-Y H:i']
                ],
                // 'updated_at',

                [
                    'class' => '\common\components\grid\ActionColumn',
                    'template' => '{view}{update}{delete}'
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>
        <?php Box::end()?>
    </div>
</div>
