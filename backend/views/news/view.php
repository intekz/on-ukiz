<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use alexssdd\dashboard\widgets\Box;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">
    <div class="row">
        <div class="col-md-10">
            <?php Box::begin([
                'title' => $this->title,
                'buttonsTemplate' => '{cancel}'
            ]); ?>
            <p>
                <?= Html::a(Yii::t('news', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('news', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('news', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'img',
                    'slug',
                    [
                        'attribute' => 'status',
                        'value' => $model->getStatusLabel(),
                    ],
                    [
                        'attribute'=>'created_at',
                        'format'=>['DateTime','php:d-m-Y H:i']
                    ],
                    [
                        'attribute'=>'updated_at',
                        'format'=>['DateTime','php:d-m-Y H:i']
                    ],
                ],
            ]) ?>
            <?php Box::end(); ?>
        </div>
    </div>
</div>
