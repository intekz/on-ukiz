<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProjectDoc */

$this->title = Yii::t('tender', 'Create Project Doc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tender', 'Project Docs'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-doc-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
