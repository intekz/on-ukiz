<?php

/* @var $this yii\web\View */
/* @var $model common\models\ProjectDoc */

$this->title = Yii::t('tender', 'Update Project Doc: {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('tender', 'Projects'), 'url' => ['/project/index']];
$this->params['breadcrumbs'][] = Yii::t('tender', 'Update');
?>
<div class="project-doc-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
