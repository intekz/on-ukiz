<?php

use common\models\ProjectImage;
use kartik\file\FileInput;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use common\models\District;
use common\models\Industry;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget as Imperavi;

use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-md-10">
        <div class="project-form">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab-1"><i class="fas fa-sliders-h" aria-hidden="true"></i> Основные данные</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2"><i class="fa fa-globe" aria-hidden="true"></i> Переводы</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-3"><i class="fa fa-info" aria-hidden="true"></i> Контактная информация</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-4"><i class="far fa-file-alt" aria-hidden="true"></i> Документы</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-5"><i class="far fa-file-image" aria-hidden="true"></i> Изображение</a>
                </li>
            </ul>
            <div class="box">
                <?php $form = ActiveForm::begin(); ?>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="box__header"></div>
                        <div class="box__body">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'industry_id')->dropDownList(
                                        ArrayHelper::map(Industry::find()->orderBy(['sort' => SORT_ASC])->all(), 'id', 'name'),
                                        ['prompt' => '']
                                    ) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'district_id')->dropDownList(
                                        ArrayHelper::map(District::find()->orderBy(['sort' => SORT_ASC])->all(), 'id', 'name'),
                                        ['prompt' => '']
                                    ) ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'desc_short')->textarea(['maxlength' => true]) ?>
                            <?= $form->field($model, 'desc')->widget(Imperavi::className(), [
                                'settings' => [
                                    'lang' => 'ru',
                                    'replaceDivs' => false,
                                    'minHeight' => 200,
                                    'fileUpload' => Url::to(['file-upload']),
                                    'fileManagerJson' => Url::to(['files-get']),
                                    'imageUpload' => Url::to(['image-upload']),
                                    'imageManagerJson' => Url::to(['images-get']),
                                    'plugins' => [
                                        'clips',
                                        'fullscreen',
                                        'fontcolor',
                                        'fontsize',
                                        'table',
                                        'filemanager',
                                        'imagemanager',
                                        'video',
                                    ],
                                ],
                            ]) ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'price', [
                                        'inputTemplate' => '<div class="input-group">{input}'.
                                            '<span class="input-group-addon">.тг</span></div>',
                                    ])->input('number', ['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'stage')->dropDownList($model->getStages()) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="box__header"></div>
                        <div class="box__body">
                            <?php foreach (['kz', 'en'] as $language): ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $form->field($model->translate($language), "[$language]name")->textInput() ?>
                                        <?= $form->field($model->translate($language), "[$language]desc_short")->textarea() ?>
                                        <?= $form->field($model->translate($language), "[$language]desc")->widget(Imperavi::className(), [
                                            'settings' => [
                                                'lang' => 'ru',
                                                'replaceDivs' => false,
                                                'minHeight' => 200,
                                                'fileUpload' => Url::to(['file-upload']),
                                                'fileManagerJson' => Url::to(['files-get']),
                                                'imageUpload' => Url::to(['image-upload']),
                                                'imageManagerJson' => Url::to(['images-get']),
                                                'plugins' => [
                                                    'clips',
                                                    'fullscreen',
                                                    'fontcolor',
                                                    'fontsize',
                                                    'table',
                                                    'filemanager',
                                                    'imagemanager',
                                                    'video',
                                                ],
                                            ],
                                        ]);?>
                                        <?= $form->field($model->translate($language), "[$language]address")->textInput() ?>
                                    </div>
                                </div>
                                <br>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-pane">
                        <div class="box__header"></div>
                        <div class="box__body">
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'phones_json')->textInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'emails_json')->textInput() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-pane">
                        <?php if ($model->isNewRecord):?>
                            <div class="box__header"></div>
                            <div class="box__body">
                                <div class="alert alert-warning">
                                    <strong>Внимание!</strong> Загрузка файлов доступна после создание проекта.
                                </div>
                            </div>
                        <?php else:?>
                            <div class="box__header">
                                <h4 class="box__title">Список документов</h4>
                                <div class="box__header_tools pull-right">
                                    <?= Html::a('Добавить', ['/project-doc/create', 'projectId' => $model->id], ['class' => 'btn btn-sm btn-primary'])?>
                                </div>
                            </div>
                            <div class="box__body">
                                <?php if ($model->projectDocs):?>
                                    <?= GridView::widget([
                                        'dataProvider' => new ActiveDataProvider(['query' => $model->getProjectDocs()]),
                                        'columns' => [
                                            ['class' => 'common\components\grid\SerialColumn'],

                                            'name',
                                            'language',

                                            [
                                                'class' => 'common\components\grid\ActionColumn',
                                                'buttons' => [
                                                    'update' => function ($url, $model) {
                                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/project-doc/update', 'id' => $model->id], [
                                                            'class' => 'btn btn-sm btn-default',
                                                            'title' => Yii::t('app', 'lead-update'),
                                                        ]);
                                                    },
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/project-doc/delete', 'id' => $model->id], [
                                                            'class' => 'btn btn-sm btn-default',
                                                            'data-method' => 'post',
                                                            'title' => Yii::t('app', 'lead-delete'),
                                                        ]);
                                                    }
                                                ],
                                            ],
                                        ],
                                    ])?>
                                <?php endif;?>
                            </div>
                        <?php endif;?>
                    </div>
                    <div id="tab-5" class="tab-pane">
                        <div class="box__header"></div>
                        <div class="box__body">
                            <?php if ($model->isNewRecord): ?>
                                <div class="alert alert-warning">
                                    <strong>Внимание!</strong> Загрузка картинок доступна после создание проекта.
                                </div>
                            <?php else: ?>
                                <?php if ($model->images):?>
                                    <?php Pjax::begin(['id' => 'pjax-project-images'])?>
                                    <?= GridView::widget([
                                        'id' => 'table-project-images',
                                        'dataProvider' => new ActiveDataProvider(['query' => $model->getImages()]),
                                        'rowOptions'=> ['class' => 'project-image'],
                                        'columns' => [
                                            ['class' => 'common\components\grid\SerialColumn'],
                                            [
                                                'attribute' => 'name',
                                                'format' => 'html',
                                                'value' => function (ProjectImage $model) {
                                                    return Html::img($model->getImgUrl(), ['style' => 'height: 50px']);
                                                },
                                                'filter' => false
                                            ],

                                            [
                                                'class' => '\common\components\grid\ActionColumn',
                                                'template' => '{delete}',
                                                'buttons' => [
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                            'href' => 'javascript:void(0);',
                                                            'title' => Yii::t('app', 'lead-delete'),
                                                            //  'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                                            'onclick' => 'Project.deleteImg('. $model->id .')'
                                                        ]);
                                                    }
                                                ]
                                            ],
                                        ],
                                    ]); ?>
                                    <?php Pjax::end()?>
                                <?php endif;?>
                                <?= FileInput::widget([
                                    'name' => 'project-image',
                                    'options'=>[
                                        'multiple'=>true
                                    ],
                                    'pluginOptions' => [
                                        'uploadUrl' => Url::to(['upload-image']),
                                        'uploadExtraData' => [
                                            'album_id' => 20,
                                            'cat_id' => 'Nature'
                                        ],
                                        'maxFileCount' => 10
                                    ]
                                ]);?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
                <div class="box__footer">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('project', 'Create') : Yii::t('project', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div><!--/.box-->
        </div>
    </div>
</div>
