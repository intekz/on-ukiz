<?php

use common\models\Industry;
use common\models\Project;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;
use alexssdd\dashboard\widgets\Box;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('project', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">
<?php Box::begin([
    'title' => $this->title,
    'buttonsTemplate' => '{create}'
])?>
<?php Pjax::begin()?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'common\components\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (Project $model) {
                    return Html::a(StringHelper::truncate($model->name, 30), ['update',  'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'industry_id',
                'value' => function (Project $model) {
                    return $model->industry ? $model->industry->name : null;
                },
                'filter' => ArrayHelper::map(
                    Industry::find()->all(),
                    'id',
                    'name'
                )
            ],
            [
                'attribute' => 'stage',
                'value' => function (Project $model) {
                    return $model->getStageLabel();
                },
                'filter' => Project::getStages()
            ],
            // 'phones_json',
            // 'emails_json:email',
            // 'sort',
            // 'moderation',
            // 'status',
			[
				'attribute' => 'status',
				'format' => 'raw',
				'value' => function (Project $model) {
					if ($model->status === $model::STATUS_PUBLISHED) {
						$class = 'label-success';
					} elseif ($model->status === $model::STATUS_NOT_PUBLISHED) {
						$class = 'label-warning';
					} else {
						$class = 'label-default';
					}
					return Html::tag('span', $model->getStatusLabel(), ['class' => 'label ' . $class]);
				},
				'filter' => Project::getStatuses()
			],

            [
                'class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
<?php Box::end()?>
</div>
