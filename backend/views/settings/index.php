<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('settings', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings">
    <ul class="settings-list">
        <li class="sittings-list__item">
            <a class="settings-list__link" href="<?= Url::to(['/settings/password-change'])?>">
                <i class="settings-list__icon fa fa-unlock-alt" aria-hidden="true"></i>
                <span class="settings-list__link_text">Смена пароля</span>
            </a>
        </li>
    </ul>
</div>
