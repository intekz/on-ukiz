<?php

use alexssdd\dashboard\widgets\Box;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::t('settings', 'Password Change');
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['/settings/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-9">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-1"><i class="fa fa-unlock-alt"></i> Смена пароля</a>
            </li>
        </ul>
        <?php $box = Box::begin(['renderBody' => false]); ?>

        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">

                <?php $form = ActiveForm::begin(); ?>
                <?php $box->beginBody();?>

                <?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'newPasswordRepeat')->passwordInput(['maxlength' => true]) ?>

                <?php $box->endBody();?>

                <?php $box->beginFooter();?>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                <?php $box->endFooter();?>

                <?php ActiveForm::end(); ?>

            </div>
            <div id="tab-2" class="tab-pane"></div>
            <div id="tab-3" class="tab-pane"></div>
        </div>

        <?php Box::end()?>
    </div>
</div>

