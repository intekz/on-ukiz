<?php

/* @var $this yii\web\View */

$this->title = 'Управляющая компания индустриальными зонами "Ontustik"';
?>
<div class="main__logo">
    <img src="/images/logo.png" alt="Logotype">
    <div class="main__company-name">Управляющая компания индустриальными зонами "Ontustik"</div>
</div>

