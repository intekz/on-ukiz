<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\forms\LoginForm */

$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login">
    <div class="login__body">
        <?php $form = ActiveForm::begin([
            'validateOnChange' => false,
            'validateOnBlur' => false
        ]);?>
        <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email'), 'autofocus' => true, 'autocomplete' => 'off'])->label(false)?>
        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton('Login', ['class' => 'btn btn-block btn-primary', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>