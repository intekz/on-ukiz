<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use alexssdd\dashboard\widgets\Box;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-md-10">
        <div class="slider-form">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-sliders" aria-hidden="true"></i> Основные</a>
                </li>
                <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-globe" aria-hidden="true"></i> Переводы</a></li>
            </ul>

            <?php $box = Box::begin([
                'title' => $this->title,
                'buttonsTemplate' => '{cancel}',
                'renderBody' => false
            ]) ?>

            <?php $form = ActiveForm::begin(); ?>
            <?php $box->beginBody() ?>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-6"> <?= $form->field($model, 'title') ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"> <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"><?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'image')->widget(FileAPI::className(), ['settings' => ['url' => ['img-upload']]]) ?>
                            <div class="text-center">Размер: 1366x450</div>
                        </div>
                        <div class="col-md-3"><?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?></div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <?php foreach (['kz', 'en'] as $language): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <h4><strong><?= Yii::t('site', $language)?></strong></h4>
                                <?= $form->field($model->translate($language), "[$language]title")->textInput(); ?>
                                <?= $form->field($model->translate($language), "[$language]description")->textarea(); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php $box->endBody() ?>

            <?php $box->beginFooter() ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('slider', 'Create') : Yii::t('slider', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?php $box->endFooter() ?>

            <?php ActiveForm::end(); ?>
            <?php Box::end() ?>
        </div>
    </div>
</div>

