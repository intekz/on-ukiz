<?php

use common\models\Slider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use alexssdd\dashboard\widgets\Box;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('slider', 'Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">
    <?php Box::begin([
        'title' => $this->title,
        'buttonsTemplate' => '{create}'
    ])?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'title',
            'link',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function (Slider $model) {
                    if ($model->status === $model::STATUS_PUBLISHED) {
                        $class = 'label-primary';
                    } elseif ($model->status === $model::STATUS_NOT_PUBLISHED) {
                        $class = 'label-danger';
                    }
                    return Html::tag('span', $model->getStatusLabel(), ['class' => 'label ' . $class]);
                },
                'filter' => Slider::getStatuses()
            ],
            [
                'attribute'=>'created_at',
                'format'=>['DateTime','php:d-m-Y H:i']
            ],
            // 'updated_at',

            [
                'class' => '\common\components\grid\ActionColumn',
                'template' => '{view}{update}{delete}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <?php Box::end()?>
</div>
