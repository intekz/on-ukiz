<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use alexssdd\dashboard\widgets\Box;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('slider', 'Sliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">
    <div class="row">
        <div class="col-md-10">
            <?php Box::begin([
                'title' => $this->title,
                'buttonsTemplate' => '{cancel}'
            ]); ?>
            <p>
                <?= Html::a(Yii::t('slider', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('slider', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('slider', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="row">
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'image',
                            'link',
                            [
                                'attribute' => 'status',
                                'value' => $model->getStatusLabel(),
                            ],
                            [
                                'attribute'=>'created_at',
                                'format'=>['DateTime','php:d-m-Y H:i']
                            ],
                            [
                                'attribute'=>'updated_at',
                                'format'=>['DateTime','php:d-m-Y H:i']
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
            <?php Box::end(); ?>
        </div>
    </div>
</div>
