<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use alexssdd\dashboard\widgets\Box;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
/* @var $model common\models\TenderDoc */
/* @var $form yii\widgets\ActiveForm */

$languages = Yii::$app->params['languages']
?>

<div class="tender-doc-form">
    <div class="row">
        <div class="col-md-9">
            <?php $box = Box::begin(['title' => $model->name]) ?>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'tender_id')->hiddenInput()->label(false) ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?>
                </div>
                <div class="col-md-9">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'language')->dropDownList(array_combine($languages, $languages)) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'file')->widget(FileAPI::className(), ['settings' => ['url' => ['img-upload']]]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('tender', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php Box::end()?>
        </div>
    </div>
</div>
