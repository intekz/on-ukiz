<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TenderDocSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tender-doc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tender_id') ?>

    <?= $form->field($model, 'language') ?>

    <?= $form->field($model, 'file_name') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('tender', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('tender', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
