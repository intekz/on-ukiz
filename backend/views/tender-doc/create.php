<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TenderDoc */
/* @var $tender \common\models\Tender */

$this->title = Yii::t('tender', 'Create Tender Doc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tender', 'Tender Docs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tender-doc-create">

    <?= $this->render('_form', [
        'model' => $model,
        'tender' => $tender
    ]) ?>

</div>
