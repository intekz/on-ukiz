<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TenderDoc */

$this->title = Yii::t('tender', 'Update Tender Doc: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('tender', 'Tenders'), 'url' => ['/tender/index']];
$this->params['breadcrumbs'][] = Yii::t('tender', 'Update');
?>
<div class="tender-doc-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
