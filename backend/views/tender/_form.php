<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\grid\GridView;
use yii\data\ActiveDataProvider;
use alexssdd\dashboard\widgets\Box;

/* @var $this yii\web\View */
/* @var $model common\models\Tender */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-md-9">
        <div class="district-form">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#tab-1"><i class="fa fa-sliders" aria-hidden="true"></i> Основные</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2"><i class="fa fa-globe" aria-hidden="true"></i> Переводы</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-3"><i class="fa fa-file-text" aria-hidden="true"></i> Документы</a>
                </li>
            </ul>
            <div class="box">
                <?php $form = ActiveForm::begin(); ?>
                <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="box__header"></div>
                            <div class="box__body">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'desc')->textarea();?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <?= $form->field($model, 'published_date')->widget(DatePicker::className(), [
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'todayHighlight' => true,
                                            ],
                                        ])?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"><?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?></div>
                                    <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?></div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="box__header"></div>
                            <div class="box__body">
                                <?php foreach (['kz', 'en'] as $language): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model->translate($language), "[$language]name")->textInput() ?>
                                            <?= $form->field($model->translate($language), "[$language]desc")->textarea();?>
                                        </div>
                                    </div>
                                    <br>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="box__header">
                                <h4 class="box__title">Список документов</h4>
                                <div class="box__header_tools pull-right">
                                    <?= Html::a('Добавить', ['/tender-doc/create', 'tenderId' => $model->id], ['class' => 'btn btn-sm btn-primary'])?>
                                </div>
                            </div>
                            <div class="box__body">
                                <?php if ($model->isNewRecord): ?>
                                    <div class="alert alert-warning">
                                        <strong>Внимание!</strong> Загрузка документов доступна после создание тендера.
                                    </div>
                                <?php else:?>
                                    <?php if ($model->getTenderDocs()):?>
                                        <?= GridView::widget([
                                            'dataProvider' => new ActiveDataProvider(['query' => $model->getTenderDocs()]),
                                            // 'filterModel' => $searchModel,
                                            'columns' => [
                                                ['class' => 'common\components\grid\SerialColumn'],

                                                'name',
                                                'language',

                                                [
                                                    'class' => 'common\components\grid\ActionColumn',
                                                    'buttons' => [
                                                        'update' => function ($url, $model) {
                                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/tender-doc/update', 'id' => $model->id], [
                                                                'class' => 'btn btn-sm btn-default',
                                                                'title' => Yii::t('app', 'lead-update'),
                                                            ]);
                                                        },
                                                        'delete' => function ($url, $model) {
                                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/tender-doc/delete', 'id' => $model->id], [
                                                                'class' => 'btn btn-sm btn-default',
                                                                'data-method' => 'post',
                                                                'title' => Yii::t('app', 'lead-delete'),
                                                            ]);
                                                        }

                                                    ],
                                                ],
                                            ],
                                        ])?>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                <div class="box__footer">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('tender', 'Create') : Yii::t('tender', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>