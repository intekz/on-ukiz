<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tender */

$this->title = Yii::t('tender', 'Create Tender');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tender', 'Tenders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tender-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
