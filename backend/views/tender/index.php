<?php

use common\models\Tender;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use alexssdd\dashboard\widgets\Box;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TenderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('tender', 'Tenders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tender-index">
<?php Box::begin([
    'title' => $this->title,
    'buttonsTemplate' => '{create}'
])?>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (Tender $model) {
                    return Html::a($model->name, ['update', 'id' => $model->id]);
                }
            ],
            // 'desc',
            // 'slug',
            [
                'attribute' => 'type',
                'value' => function (Tender $model) {
                    return $model->getTypeLabel();
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function (Tender $model) {
                    if ($model->status === $model::STATUS_PUBLISHED) {
                        $class = 'label-success';
                    } elseif ($model->status === $model::STATUS_NOT_PUBLISHED) {
                        $class = 'label-warning';
                    } else {
                        $class = 'label-default';
                    }
                    return Html::tag('span', $model->getStatusLabel(), ['class' => 'label ' . $class]);
                },
                'filter' => Tender::getStatuses()
            ],
            [
                'attribute'=>'created_at',
                'format'=>['DateTime','php:d-m-Y H:i']
            ],
            // 'updated_at',

            ['class' => 'common\components\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
<?php Box::end()?>
</div>
