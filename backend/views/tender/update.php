<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tender */

$this->title = Yii::t('tender', 'Update {modelClass}: ', [
    'modelClass' => 'Tender',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tender', 'Tenders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('tender', 'Update');
?>
<div class="tender-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
