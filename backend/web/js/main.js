Project = {
    deleteImg: function (id) {
        $.get('/project/delete-img/', {id:id}, function (res) {
            if (res.status === true) {
                var img = $('.project-image[data-key="' + id + '"]').fadeOut(200, function () {
                    img.remove()
                });
            }
        });
        return false;
    }
};