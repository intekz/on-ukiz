<?php
/**
 * Created by PhpStorm.
 * User: inte-1
 * Date: 09.02.2016
 * Time: 17:43
 */

namespace common\components\grid;


class SerialColumn extends \yii\grid\SerialColumn{

    public $options = [
        'width' => 1
    ];

}