<?php
namespace common\components\rbac;

class Rbac
{
	// Permission
	const MANAGE_PROJECT = 'managerProject';
	const MANAGE_NEWS = 'managerNews';
	const DASHBOARD_ACCESS = 'dashboardAccess';

	// Roles
	const ROLE_USER = 'user';
	const ROLE_ADMIN = 'admin';
}