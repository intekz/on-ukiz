<?php
namespace common\components\rbac;


class Role
{
    const PERMISSION_DASHBOARD_PANEL = 'permDashboardPanel';
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
}