<?php

namespace common\components\rbac\rules;

use yii\rbac\Rule;
use yii\base\InvalidCallException;

class ProjectAuthorRule extends Rule
{
	public $name = 'projectAuthor';

	public function execute($userId, $item, $params)
	{
		if (empty($params['project'])) {
			throw new InvalidCallException('Specify project.');
		}
		return $params['project']->user_id == $userId;
	}
}