<?php

return [
    'adminEmail' => 'ukiz@inte.kz',
    'supportEmail' => 'income@iz-ontustik.kz',
    'user.passwordResetTokenExpire' => 3600,
    'languages' => ['ru', 'kz', 'en'],
	'defaultLanguage' => 'ru',
    'staticDomain' => 'http://st.iz-ontustik.kz/',

	'apiSend' => false
];