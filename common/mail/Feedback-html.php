<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $feedback \common\models\Feedback */

?>
<div class="feedBack">
    <h1>Форма обратной связи #<?= $feedback->id ?></h1>
    <p>Имя: <?= Html::encode($feedback->name) ?></p>
    <p>Город: <?= Html::encode($feedback->city) ?></p>
    <p>Е-майл: <?= Html::encode($feedback->email) ?></p>
    <p>Сообщение: <?= Html::encode($feedback->message) ?></p>
    <p>Дата заказа: <?= Yii::$app->formatter->asDatetime($feedback->created_at) ?></p>
</div>