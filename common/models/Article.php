<?php

namespace common\models;

use common\models\query\ArticleQuery;
use creocoder\translateable\TranslateableBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%article}}".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $text
 * @property string $meta_title
 * @property string $meta_desc
 * @property string $meta_keywords
 * @property string $og_title
 * @property string $og_desc
 * @property string $og_img
 * @property string $og_type
 * @property string $og_url
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ArticleTranslation[] $articleTranslates
 */
class Article extends \yii\db\ActiveRecord
{

	const STATUS_PUBLISHED = 10;
	const STATUS_NOT_PUBLISHED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
			[
				'class' => SluggableBehavior::className(),
				'attribute' => 'title',
			],
			'translateable' => [
				'class' => TranslateableBehavior::className(),
				'translationAttributes' => ['title', 'text', 'meta_title', 'meta_desc'],
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text'], 'string'],
            [['status'], 'integer'],
            [['title', 'slug', 'meta_title', 'meta_desc', 'meta_keywords', 'og_title', 'og_desc', 'og_img', 'og_type', 'og_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'title' => Yii::t('article', 'Title'),
            'slug' => Yii::t('article', 'Slug'),
            'text' => Yii::t('article', 'Text'),
            'meta_title' => Yii::t('article', 'Meta Title'),
            'meta_desc' => Yii::t('article', 'Meta Desc'),
            'meta_keywords' => Yii::t('article', 'Meta Keywords'),
            'og_title' => Yii::t('article', 'Og Title'),
            'og_desc' => Yii::t('article', 'Og Desc'),
            'og_img' => Yii::t('article', 'Og Img'),
            'og_type' => Yii::t('article', 'Og Type'),
            'og_url' => Yii::t('article', 'Og Url'),
            'status' => Yii::t('article', 'Status'),
            'created_at' => Yii::t('article', 'Created At'),
            'updated_at' => Yii::t('article', 'Updated At'),
        ];
    }

	/**
	 * @return mixed
	 */
	public function getStatusLabel()
	{
		return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }

	/**
	 * @return array
	 */
    public static function getStatuses()
    {
    	return [
    		self::STATUS_PUBLISHED => Yii::t('article', 'Published'),
    		self::STATUS_NOT_PUBLISHED => Yii::t('article', 'Not Published'),
	    ];
    }

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTranslations()
	{
		return $this->hasMany(ArticleTranslation::className(), ['article_id' => 'id']);
	}

	/**
	 * @return string
	 */
	public function getTitleTranslate()
	{
		return $this->translate()->title ? $this->translate()->title : $this->title;
	}

	/**
	 * @return string
	 */
	public function getTextTranslate()
	{
		return $this->translate()->text ? $this->translate()->text : $this->text;
	}

	/**
	 * @return string
	 */
	public function getMetaTitleTranslate()
	{
		return $this->translate()->meta_title ? $this->translate()->meta_title : $this->meta_title;
	}

	/**
	 * @return string
	 */
	public function getMetaDescTranslate()
	{
		return $this->translate()->meta_desc ? $this->translate()->meta_desc : $this->meta_desc;
	}

    /**
     * @inheritdoc
     * @return \common\models\query\ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }
}
