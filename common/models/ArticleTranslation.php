<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%article_translate}}".
 *
 * @property int $id
 * @property int $article_id
 * @property string $language
 * @property string $title
 * @property string $text
 * @property string $meta_title
 * @property string $meta_desc
 * @property string $meta_keywords
 *
 * @property Article $article
 */
class ArticleTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_translate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id'], 'integer'],
            [['language'], 'string', 'max' => 16],
            [['title', 'text', 'meta_title', 'meta_desc', 'meta_keywords'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		switch ($this->language) {
			case 'kz':
				return [
					'title' => 'Title (kz)',
					'text' => 'Text (kz)',
					'meta_title' => 'Meta Title (kz)',
					'meta_desc' => 'Meta Desc (kz)',
				];
			case 'en':
				return [
					'title' => 'Title (en)',
					'text' => 'Text (en)',
					'meta_title' => 'Meta Title (en)',
					'meta_desc' => 'Meta Desc (en)',
				];
		}
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
}
