<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use Yii;
use yii\db\ActiveRecord;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "{{%district}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort
 *
 * @property DistrictTranslation[] $translations
 */
class District extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%district}}';
    }

	public function behaviors()
	{
		return [
			'sort' => [
				'class' => SortableGridBehavior::className(),
				'sortableAttribute' => 'sort',
			],
			'translateable' => [
				'class' => TranslateableBehavior::className(),
				'translationAttributes' => ['name'],
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('district', 'ID'),
            'name' => Yii::t('district', 'Name'),
            'sort' => Yii::t('district', 'Sort'),
        ];
    }

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(DistrictTranslation::className(), ['district_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getNameTranslate()
    {
        return $this->translate()->name ? $this->translate()->name : $this->name;
    }
}
