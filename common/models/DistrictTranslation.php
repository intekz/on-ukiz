<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%district_translation}}".
 *
 * @property integer $id
 * @property integer $district_id
 * @property string $name
 * @property string $language
 *
 * @property District $district
 */
class DistrictTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%district_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['district_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 16],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id']],
        ];
    }

	public function attributeLabels()
	{
		switch ($this->language) {
			case 'kz':
				return [
					'name' => 'Тақырып (kz)',
				];
			case 'en':
				return [
					'name' => 'Name (en)'
				];
		}
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }
}
