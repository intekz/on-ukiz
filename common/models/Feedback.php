<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $email
 * @property string $message
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Feedback extends ActiveRecord
{
    const STATUS_NEW = 10;
    const STATUS_PROCESSED = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'city', 'email', 'message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('feedback', 'ID'),
            'name' => Yii::t('feedback', 'Name'),
            'city' => Yii::t('feedback', 'City'),
            'email' => Yii::t('feedback', 'Email'),
            'message' => Yii::t('feedback', 'Message'),
            'status' => Yii::t('feedback', 'Status'),
            'created_at' => Yii::t('feedback', 'Created At'),
            'updated_at' => Yii::t('feedback', 'Updated At'),
        ];
    }
    /**
     * @return mixed
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW  => Yii::t('feedback', 'New'),
            self::STATUS_PROCESSED  => Yii::t('feedback', 'Processed'),
        ];
    }
}
