<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use himiklab\sortablegrid\SortableGridBehavior;
use Yii;

/**
 * This is the model class for table "{{%industry}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort
 *
 * @property IndustryTranslation[] $translations
 */
class Industry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%industry}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

	public function behaviors()
	{
		return [
			'sort' => [
				'class' => SortableGridBehavior::className(),
				'sortableAttribute' => 'sort',
			],
			'translateable' => [
				'class' => TranslateableBehavior::className(),
				'translationAttributes' => ['name'],
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('industry', 'ID'),
            'name' => Yii::t('industry', 'Name'),
            'sort' => Yii::t('industry', 'Sort'),
        ];
    }

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(IndustryTranslation::className(), ['industry_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getNameTranslate()
    {
        return $this->translate()->name ? $this->translate()->name : $this->name;
    }
}
