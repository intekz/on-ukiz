<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%industry_translation}}".
 *
 * @property integer $id
 * @property integer $industry_id
 * @property string $name
 * @property string $language
 *
 * @property Industry $industry
 */
class IndustryTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%industry_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['industry_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 16],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => Industry::className(), 'targetAttribute' => ['industry_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
		switch ($this->language) {
			case 'kz':
				return [
					'name' => 'Тақырып (kz)',
				];
			case 'en':
				return [
					'name' => 'Name (en)'
				];
		}
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(Industry::className(), ['id' => 'industry_id']);
    }
}
