<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use vova07\fileapi\behaviors\UploadBehavior;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $img
 * @property string $desc
 * @property string $text
 * @property string $slug
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property NewsTranslation[] $translations
 * @property NewsTranslation[] $newsTranslations
 */
class News extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_NOT_PUBLISHED = 2;

    const FIXED_YES = 1;
    const FIXED_NO = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['title', 'desc', 'text'],
            ],
            TimestampBehavior::className(),
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'img' => [
                        'path' => '@static/web/news/',
                        'tempPath' => '@static/temp/',
                        'url' => $this->getPath()
                    ]
                ]
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'slug'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'img', 'slug', 'desc'], 'string', 'max' => 255],
            [['slug'], 'unique'],
			['fixed', 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('news', 'ID'),
            'img' => Yii::t('news', 'Image'),
            'slug' => Yii::t('news', 'Slug'),
            'status' => Yii::t('news', 'Status'),
            'created_at' => Yii::t('news', 'Created At'),
            'updated_at' => Yii::t('news', 'Updated At'),
        ];
    }

	/**
	 * @return array
	 */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return Yii::$app->params['staticDomain'] . 'news/';
    }

    /**
     * @return string
     */
    public function getImgUrl()
    {
        return $this->img ? $this->getPath() . $this->img : $this->getPath() . 'default-news.png';
    }

    /**
     * @return mixed
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }

    /**
     * return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('app', 'Published'),
            self::STATUS_NOT_PUBLISHED => Yii::t('app', 'Not Published')
        ];
    }

	public function getTitleTranslate()
	{
		return $this->translate()->title ? $this->translate()->title : $this->title;
    }

	public function getDescTranslate()
	{
		return $this->translate()->desc ? $this->translate()->desc : $this->desc;
	}

	public function getTextTranslate()
	{
		return $this->translate()->text ? $this->translate()->text : $this->text;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTranslations()
	{
		return $this->hasMany(NewsTranslation::className(), ['news_id' => 'id']);
	}

	public function getNewsTranslations()
	{
		return $this->hasMany(NewsTranslation::className(), ['news_id' => 'id']);
	}

    public function fields()
    {
        return [
            'id',
            'title',
            'img',
            'desc',
            'text',
            'translations' => function (News $model) {
                return $model->newsTranslations ? $model->newsTranslations : null;
            }
        ];
    }
}
