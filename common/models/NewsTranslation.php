<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%news_translation}}".
 *
 * @property integer $id
 * @property integer $news_id
 * @property string $language
 * @property string $title
 * @property string $desc
 * @property string $text
 *
 * @property News $news
 */
class NewsTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['text'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'desc'], 'string', 'max' => 255],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        switch ($this->language) {
            case 'kz':
                return [
                    'title' => 'Тақырып',
                    'description' => 'Сипаттамасы',
                    'text' => 'Толық Сипаттамасы',
                ];
            case 'en':
                return [
                    'title' => 'Title',
                    'description' => 'Description',
                    'text' => 'Text',
                ];
            default:
                return [
                    'title' => 'Заголовок',
                    'description' => 'Описание',
                    'text' => 'Текст',
                ];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['news_id' => 'id']);
    }

	/**
	 * @return array
	 */
	public function fields()
	{
		return [
			'language',
			'title',
			'desc',
			'text'
		];
	}
}
