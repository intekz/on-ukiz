<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\query\ProjectQuery;
use creocoder\translateable\TranslateableBehavior;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property integer $industry_id
 * @property integer $district_id
 * @property string $name
 * @property string $slug
 * @property string $img
 * @property string $desc_short
 * @property string $desc
 * @property string $address
 * @property string $price
 * @property integer $stage
 * @property string $phones_json
 * @property string $emails_json
 * @property integer $sort
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProjectTranslation[] $translations
 * @property ProjectImage[] $images
 * @property Industry $industry
 * @property ProjectDoc[] $projectDocs
 * @property ProjectDoc[] $projectDocsLanguage
 */
class Project extends ActiveRecord
{
	/** Stage status */
	const STAGE_DURING = 10;
	const STAGE_PLANNED = 15;
	const STAGE_INVESTMENT = 20;
	const STAGE_COMPLETED = 25;

	/** Statuses */
	const STATUS_PUBLISHED = 10;
	const STATUS_NOT_PUBLISHED = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%project}}';
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
			[
				'class' => SluggableBehavior::className(),
				'attribute' => 'name',
			],
			'translateable' => [
				'class' => TranslateableBehavior::className(),
				'translationAttributes' => ['name', 'desc', 'desc_short'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'price', 'district_id', 'industry_id', 'desc_short'], 'required'],
			[['desc', 'phones_json', 'emails_json'], 'string'],
			[['price'], 'number'],
			[['stage', 'sort', 'status', 'created_at', 'updated_at', 'district_id', 'industry_id'], 'integer'],
			[['name', 'img', 'desc_short', 'address', 'slug'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('project', 'ID'),
			'district_id' => Yii::t('project', 'District'),
			'industry_id' => Yii::t('project', 'Industry'),
			'name' => Yii::t('project', 'Name'),
			'img' => Yii::t('project', 'Img'),
			'desc_short' => Yii::t('project', 'Desc Short'),
			'desc' => Yii::t('project', 'Desc'),
			'address' => Yii::t('project', 'Address'),
			'price' => Yii::t('project', 'Price'),
			'stage' => Yii::t('project', 'Stage'),
			'phones_json' => Yii::t('project', 'Phones'),
			'emails_json' => Yii::t('project', 'Emails'),
			'sort' => Yii::t('project', 'Sort'),
			'status' => Yii::t('project', 'Status'),
			'created_at' => Yii::t('project', 'Created At'),
			'updated_at' => Yii::t('project', 'Updated At'),
		];
	}

	/**
	 * @return mixed
	 */
	public function getStageLabel()
	{
		return ArrayHelper::getValue(static::getStages(), $this->stage);
	}

	/**
	 * @return array
	 */
	public static function getStages()
	{
		return [
			self::STAGE_DURING => Yii::t('project', 'Stage During'),
			self::STAGE_PLANNED => Yii::t('project', 'Stage Planned'),
			self::STAGE_COMPLETED => Yii::t('project', 'Stage Completed'),
			self::STAGE_INVESTMENT => Yii::t('project', 'Stage Investment'),
		];
	}

	/**
	 * @return mixed
	 */
	public function getStatusLabel()
	{
		return ArrayHelper::getValue(static::getStatuses(), $this->status);
	}

	/**
	 * @return array
	 */
	public static function getStatuses()
	{
		return [
			self::STATUS_PUBLISHED => Yii::t('project', 'Status Published'),
			self::STATUS_NOT_PUBLISHED => Yii::t('project', 'Status Not Published'),
		];
	}

	/**
	 * @return array
	 */
	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getIndustry()
	{
		return $this->hasOne(Industry::className(), ['id' => 'industry_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTranslations()
	{
		return $this->hasMany(ProjectTranslation::className(), ['project_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getImages()
	{
		return $this->hasMany(ProjectImage::className(), ['project_id' => 'id'])->orderBy(['sort' => SORT_DESC]);
	}

	/**
	 * @return string
	 */
	public function getMainImg()
	{
		return $this->images ? $this->images[0]->getImgUrl() : 'default.jpg';
	}

	/**
	 * @return bool
	 * @throws \yii\base\ErrorException
	 */
	public function beforeDelete()
	{
		if (!parent::beforeDelete()) {
			return false;
		}

		$removePath = Yii::getAlias('@static/web/project/' . $this->id);
		FileHelper::removeDirectory($removePath);
		return true;
	}

	/**
	 * @return string
	 */
	public function getNameTranslate()
	{
		return $this->translate()->name ? $this->translate()->name : $this->name;
	}

	/**
	 * @return string
	 */
	public function getDescTranslate()
	{
		return $this->translate()->desc ? $this->translate()->desc : $this->desc;
	}

	/**
	 * @return string
	 */
	public function getDescShortTranslate()
	{
		return $this->translate()->desc ? $this->translate()->desc : $this->desc_short;
	}

	/**
	 * @return string
	 */
	public function getAddressTranslate()
	{
		return $this->translate()->address ? $this->translate()->address : $this->address;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProjectDocs()
	{
		return $this->hasMany(ProjectDoc::className(), ['project_id' => 'id']);
	}

	public function getProjectDocsLanguage()
	{
		return $this->getProjectDocs()->andWhere(['language' => Yii::$app->language]);
	}

	/**
	 * @inheritdoc
	 * @return ProjectQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new ProjectQuery(get_called_class());
	}
}