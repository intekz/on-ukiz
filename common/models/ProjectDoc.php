<?php

namespace common\models;

use vova07\fileapi\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "{{%project_docs}}".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property string $language
 * @property string $file
 * @property int $sort
 *
 * @property Project $project
 */
class ProjectDoc extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%project_docs}}';
	}

	public function behaviors()
	{
		return [
			'uploadBehavior' => [
				'class' => UploadBehavior::className(),
				'attributes' => [
					'file' => [
						'path' => '@static/web/project/doc',
						'tempPath' => '@static/temp/',
						'url' => $this->getPath()
					]
				]
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['project_id', 'file'], 'required'],
			[['project_id', 'sort'], 'integer'],
			[['name', 'file'], 'string', 'max' => 255],
			[['language'], 'string', 'max' => 16],
			[['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('project', 'ID'),
			'project_id' => Yii::t('project', 'Project ID'),
			'name' => Yii::t('project', 'Name'),
			'language' => Yii::t('project', 'Language'),
			'file' => Yii::t('project', 'File'),
			'sort' => Yii::t('project', 'Sort'),
		];
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return Yii::$app->params['staticDomain'] . 'project/doc/';
	}

	/**
	 * @return string
	 */
	public function getFileUrl()
	{
		return $this->getPath() . $this->file;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'project_id']);
	}
}