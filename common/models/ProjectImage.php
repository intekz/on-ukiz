<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use Yii;

/**
 * This is the model class for table "{{%project_image}}".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property int $main
 */
class ProjectImage extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%project_image}}';
	}

	public function behaviors()
	{
		return [
			'sort' => [
				'class' => SortableGridBehavior::className(),
				'sortableAttribute' => 'sort'
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['project_id', 'name'], 'required'],
			[['project_id', 'main'], 'integer'],
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('project', 'ID'),
			'project_id' => Yii::t('project', 'Project ID'),
			'name' => Yii::t('project', 'Name'),
			'main' => Yii::t('project', 'Main'),
		];
	}

	/**
	 * Путь до папки с картинкой
	 * @return string
	 */
	public function getPath()
	{
		return Yii::$app->params['staticDomain'] . 'project/' . $this->project_id . '/';
	}

	/**
	 * Полный урл до картинке
	 * @return string
	 */
	public function getImgUrl()
	{
		return $this->getPath() . $this->name;
	}
}