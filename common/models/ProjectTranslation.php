<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%project_translation}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $language
 * @property string $name
 * @property string $desc
 * @property string $desc_short
 * @property string $address
 *
 * @property Project $project
 */
class ProjectTranslation extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%project_translation}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['project_id'], 'integer'],
			[['desc', 'language'], 'string'],
			[['name', 'desc_short', 'address'], 'string', 'max' => 255],
			[['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels2()
	{
		return [
			'id' => Yii::t('project', 'ID'),
			'project_id' => Yii::t('project', 'Project ID'),
			'language' => Yii::t('project', 'Language'),
			'name' => Yii::t('project', 'Name'),
			'desc' => Yii::t('project', 'Desc'),
			'desc_short' => Yii::t('project', 'Desc Short'),
			'address' => Yii::t('project', 'Address'),
		];
	}

	public function attributeLabels()
	{
		switch ($this->language) {
			case 'kz':
				return [
					'name' => 'Тақырып (kz)',
					'desc_short' => 'Сипаттамасы (kz)',
					'desc' => 'Толық Сипаттамасы (kz)',
					'address' => 'Мекенжай (kz)',
				];
			case 'en':
				return [
					'name' => 'Title (en)',
					'desc_short' => 'Short description (en)',
					'desc' => 'Text (en)',
					'address' => 'Address (en)',
				];
			default:
				return [
					'name' => 'Заголовок (ru)',
					'desc_short' => 'Описание (ru)',
					'desc' => 'Текст (ru)',
					'address' => 'Адрес (ru)',
				];
		}
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'project_id']);
	}
}