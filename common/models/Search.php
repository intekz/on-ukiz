<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%search}}".
 *
 * @property int $id
 * @property string $slug
 * @property int $status
 * @property string $title
 * @property string $text
 * @property int $type
 *
 * @property Project $project
 * @property News $news
 * @property Tender $tender
 */
class Search extends \yii\db\ActiveRecord
{
    const TYPE_PROJECT = 1;
    const TYPE_NEWS = 2;
    const TYPE_TENDER = 3;

    public static function primaryKey()
    {
        return ['id', 'type'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%search}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'type'], 'integer'],
            [['text'], 'string'],
            [['slug', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('search', 'ID'),
            'slug' => Yii::t('search', 'Slug'),
            'status' => Yii::t('search', 'Status'),
            'title' => Yii::t('search', 'Title'),
            'text' => Yii::t('search', 'Text'),
            'type' => Yii::t('search', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tender::className(), ['id' => 'id']);
    }

    public function getProjectTranslation()
    {
        return $this->hasOne(ProjectTranslation::className(), ['project_id' => 'id'])->onCondition(['t2.language' => Yii::$app->language]);
    }

    public function getNewsTranslation()
    {
        return $this->hasOne(NewsTranslation::className(), ['news_id' => 'id'])->onCondition(['t3.language' => Yii::$app->language]);
    }

    public function getTenderTranslation()
    {
        return $this->hasOne(TenderTranslation::className(), ['tender_id' => 'id'])->onCondition(['t4.language' => Yii::$app->language]);
    }

    /**
     * @return string
     */
    public function getTypeLabel()
    {
        if ($this->type) {
            switch ($this->type) {
                case self::TYPE_PROJECT:
                    return 'project';
                case self::TYPE_NEWS:
                    return 'news';
                case self::TYPE_TENDER:
                    return 'tender';
            }
        }
    }
}
