<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use vova07\fileapi\behaviors\UploadBehavior;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Slider extends ActiveRecord
{

    const STATUS_PUBLISHED = 1;
    const STATUS_NOT_PUBLISHED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['title', 'description'],
            ],
            TimestampBehavior::className(),
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => '@static/web/slider/',
                        'tempPath' => '@static/temp/',
                        'url' => $this->getPath()
                    ]
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'image'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['image', 'link', 'description', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('slider', 'ID'),
            'image' => Yii::t('slider', 'Image'),
            'link' => Yii::t('slider', 'Link'),
            'status' => Yii::t('slider', 'Status'),
            'created_at' => Yii::t('slider', 'Created At'),
            'updated_at' => Yii::t('slider', 'Updated At'),
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return Yii::$app->params['staticDomain'] . 'slider/';
    }

    /**
     * @return string
     */
    public function getImgUrl()
    {
        return $this->image ? $this->getPath() . $this->image : $this->getPath() . 'default-slider.png';
    }


    /**
     * @return mixed
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }

    /**
     * return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('app', 'Published'),
            self::STATUS_NOT_PUBLISHED => Yii::t('app', 'Not Published')
        ];
    }

	public function getTranslations()
	{
		return $this->hasMany(SliderTranslation::className(), ['slider_id' => 'id']);
	}

	public function getTitleTranslate()
	{
		return $this->translate()->title ? $this->translate()->title : $this->title;
	}

	public function getDescTranslate()
	{
		return $this->translate()->description ? $this->translate()->description : $this->description;
	}
}
