<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ie_slider_translation".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property string $language
 * @property string $title
 * @property string $description
 *
 * @property SliderTranslation $slider
 * @property SliderTranslation[] $sliderTranslations
 */
class SliderTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id'], 'integer'],
            [['language'], 'string', 'max' => 16],
            [['title', 'description'], 'string', 'max' => 255],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => SliderTranslation::className(), 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        switch ($this->language) {
            case 'kz':
                return [
                    'title' => 'Тақырып',
                    'description' => 'Сипаттамасы',
                ];
            case 'en':
                return [
                    'title' => 'Title',
                    'description' => 'Description',
                ];
            default:
                return [
                    'title' => 'Заголовок',
                    'description' => 'Описание',
                ];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(SliderTranslation::className(), ['id' => 'slider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderTranslations()
    {
        return $this->hasMany(SliderTranslation::className(), ['slider_id' => 'id']);
    }
}
