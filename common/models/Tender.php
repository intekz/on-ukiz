<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%tender}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property string $slug
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published_at
 *
 * @property TenderDoc[] $tenderDocs
 * @property TenderDoc[] $tenderDocsLanguage
 * @property TenderTranslation[] $tenderTranslations
 */
class Tender extends ActiveRecord
{
    public $published_date;
    /** Statuses */
    const STATUS_PUBLISHED = 10;
    const STATUS_NOT_PUBLISHED = 0;

    /** Type status */
    const TYPE_IMPLEMENTED = 1;
    const TYPE_EXECUTABLE = 2;
    const TYPE_PLANNED = 3;
    const TYPE_REQUIRED_INVESTMENTS = 4;
    const TYPE_ACTUAL = 5;
    const TYPE_ARCHIVE = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tender}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name', 'desc'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'type'], 'required'],
            [['type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'desc', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            ['published_date', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tender', 'ID'),
            'name' => Yii::t('tender', 'Name'),
            'desc' => Yii::t('tender', 'Desc'),
            'slug' => Yii::t('tender', 'Slug'),
            'type' => Yii::t('tender', 'Type'),
            'status' => Yii::t('tender', 'Status'),
            'created_at' => Yii::t('tender', 'Created At'),
            'updated_at' => Yii::t('tender', 'Updated At'),
        ];
    }

    /**
     * @param $value
     */
    public function setPublishedDate($value)
    {
        $this->published_at = Yii::$app->formatter->asTimestamp($value);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getPublishedDate()
    {
        return $this->published_at ? Yii::$app->formatter->asDate($this->published_at, 'php:d.m.Y') : date('d.m.Y');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenderDocs()
    {
        return $this->hasMany(TenderDoc::className(), ['tender_id' => 'id']);
    }

    public function getTenderDocsLanguage()
    {
        return $this->getTenderDocs()->andWhere(['language' => Yii::$app->language]);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TenderTranslation::className(), ['tender_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getNameTranslate()
    {
        return $this->translate()->name ? $this->translate()->name : $this->name;
    }

    /**
     * @return string
     */
    public function getDescTranslate()
    {
        return $this->translate()->desc ? $this->translate()->desc : $this->desc;
    }

    /**
     * @return mixed
     */
    public function getTypeLabel()
    {
        return ArrayHelper::getValue(static::getTYPEs(), $this->type);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_ACTUAL => Yii::t('site', 'Actual'),
            self::TYPE_ARCHIVE => Yii::t('site', 'Archive'),
        ];
    }

    /**
     * @return mixed
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('tender', 'Status Published'),
            self::STATUS_NOT_PUBLISHED => Yii::t('tender', 'Status Not Published'),
        ];
    }
}
