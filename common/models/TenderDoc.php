<?php

namespace common\models;

use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tender_doc}}".
 *
 * @property integer $id
 * @property integer $tender_id
 * @property string $language
 * @property string $name
 * @property string $file
 * @property string $type
 *
 * @property Tender $tender
 */
class TenderDoc extends \yii\db\ActiveRecord
{

    const TYPE_WORD = 'word';
    const TYPE_EXCEL = 'excel';
    const TYPE_PDF = 'pdf';
    const TYPE_TEXT = 'text';
    const TYPE_PPT = 'ppt';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tender_doc}}';
    }

    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'file' => [
                        'path' => '@static/web/tender/',
                        'tempPath' => '@static/temp/',
                        'url' => $this->getPath()
                    ]
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tender_id', 'name', 'file'], 'required'],
            [['tender_id'], 'integer'],
            [['language'], 'string', 'max' => 16],
            [['file', 'name', 'type'], 'string', 'max' => 255],
            [['tender_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tender::className(), 'targetAttribute' => ['tender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tender', 'ID'),
            'tender_id' => Yii::t('tender', 'Tender ID'),
            'language' => Yii::t('tender', 'Language'),
            'file_name' => Yii::t('tender', 'File Name'),
        ];
    }

    /**
     * @return mixed
     */
    public function getTypeLabel()
    {
        return ArrayHelper::getValue(static::getTypes(), $this->type);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_WORD => Yii::t('tender', 'Type Word'),
            self::TYPE_EXCEL => Yii::t('tender', 'Type Excel'),
            self::TYPE_PDF => Yii::t('tender', 'Type Pdf'),
            self::TYPE_TEXT => Yii::t('tender', 'Type Text'),
            self::TYPE_PPT => Yii::t('tender', 'Type Ppt'),
        ];
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return Yii::$app->params['staticDomain'] . 'tender/';
    }

    /**
     * @return string
     */
    public function getFileUrl()
    {
        return $this->getPath() . $this->file;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tender::className(), ['id' => 'tender_id']);
    }
}
