<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tender_translation}}".
 *
 * @property integer $id
 * @property integer $tender_id
 * @property string $language
 * @property string $name
 * @property string $desc
 *
 * @property Tender $tender
 */
class TenderTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tender_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['tender_id'], 'required'],
            [['tender_id'], 'integer'],
            [['language'], 'string', 'max' => 16],
            [['name', 'desc'], 'string', 'max' => 255],
            [['tender_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tender::className(), 'targetAttribute' => ['tender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels2()
    {
        return [
            'id' => Yii::t('tender', 'ID'),
            'tender_id' => Yii::t('tender', 'Tender ID'),
            'language' => Yii::t('tender', 'Language'),
            'name' => Yii::t('tender', 'Name'),
            'desc' => Yii::t('tender', 'Desc'),
        ];
    }

    public function attributeLabels()
    {
        switch ($this->language) {
            case 'kz':
                return [
                    'name' => 'Тақырып (kz)',
                    'desc' => 'Толық Сипаттамасы (kz)',
                ];
            case 'en':
                return [
                    'name' => 'Title (en)',
                    'desc' => 'Text (en)',
                ];
            default:
                return [
                    'name' => 'Заголовок (ru)',
                    'desc' => 'Текст (ru)',
                ];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTender()
    {
        return $this->hasOne(Tender::className(), ['id' => 'tender_id']);
    }
}
