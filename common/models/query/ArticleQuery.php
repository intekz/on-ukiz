<?php

namespace common\models\query;
use common\models\Article;

/**
 * This is the ActiveQuery class for [[\common\models\Article]].
 *
 * @see \common\models\Article
 */
class ArticleQuery extends \yii\db\ActiveQuery
{
    public function published()
    {
        return $this->andWhere(['status' => Article::STATUS_PUBLISHED]);
    }

	public function notPublished()
	{
		return $this->andWhere(['status' => Article::STATUS_NOT_PUBLISHED]);
	}

    /**
     * @inheritdoc
     * @return \common\models\Article[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Article|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
