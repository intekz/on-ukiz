<?php
namespace common\models\query;

use common\models\Project;
use yii\db\ActiveQuery;

/**
 * Class ProjectQuery
 * @package common\models\query
 */
class ProjectQuery extends ActiveQuery
{
	/**
	 * Published catalog
	 * @return $this
	 */
	public function published()
	{
		return $this->andWhere(['status' => Project::STATUS_PUBLISHED]);
	}

	/**
	 * @param $slug
	 * @return $this
	 */
	public function forSlug($slug)
	{
		return $this->andWhere(['slug' => $slug]);
	}

	/**
	 * @inheritdoc
	 * @return \common\models\Project[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * @inheritdoc
	 * @return \common\models\Project|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}