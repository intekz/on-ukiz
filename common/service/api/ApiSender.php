<?php
namespace common\service\api;

use Yii;
use common\models\News;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 * Class ApiSender
 * @package common\components\api
 */
class ApiSender
{
	public $url;
	public $method;
	public $baseUrl = 'http://api.on.inte.kz/';
	public $authUrl = 'auth';

	private $_model;
	private $_token;
	private $_email;
	private $_pass;

	public function __construct($model, $config = [])
	{
		if (!Yii::$app->params['apiSend']) {
			return false;
		}

		$this->_model = $model;
		$this->_email = $config['email'];
		$this->_pass = $config['pass'];
		$this->url = $config['url'];
		$this->method = $config['method'];

		$token = Yii::$app->cache->get('access_token');

		if (!$token && $token['expired_at'] < time()) {
			$data = $this->getToken();

			if (!$data['token']) {
				throw new \Exception('No authorization token');
			}
			$this->_token = $data['token'];
			Yii::$app->cache->set('access_token', $data);
		} else {
			$this->_token = $token['token'];
		}
	}

	public function send()
	{
		$client = new Client();
		$response = $client->createRequest()
			->setFormat(Client::FORMAT_JSON)
			->setMethod($this->method)
			->setUrl($this->baseUrl . $this->url)
			->setHeaders(['Authorization' => 'Bearer ' . $this->_token])
			->setData($this->_model)
			->send();

		if ($response->isOk) {
			return $response->data;
		}
		return null;
	}

	public function getToken()
	{
		$client = new Client();
		$response = $client->createRequest()
			->setMethod('post')
			->setUrl($this->baseUrl . $this->authUrl)
			->setData(['email' => $this->_email, 'password' => $this->_pass])
			->send();

		if ($response->isOk) {
			return $response->data;
		}
		return false;
	}
}