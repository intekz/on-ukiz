<?php

namespace common\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Class PhoneValidator
 * @package common\validators
 */
class PhoneValidator extends RegularExpressionValidator
{
    public $pattern = '#^\+7\(\d{3}\)\d{3}\-\d{2}\-\d{2}#is';
}