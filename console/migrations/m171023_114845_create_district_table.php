<?php

use common\util\Migration;


/**
 * Handles the creation of table `district`.
 */
class m171023_114845_create_district_table extends Migration
{
	public $tableName = '{{%district}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'sort' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
