<?php

use common\util\Migration;


/**
 * Handles the creation of table `district_translation`.
 */
class m171023_114846_create_district_translation_table extends Migration
{
	public $tableName = '{{%district_translation}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
			'district_id' => $this->integer()->notNull(),
			'name' => $this->string(),
			'language' => $this->string(16),
        ]);

        // fk
		$this->addForeignKey(
			'fk-district_translation-district_id-district-id',
			$this->tableName,
			'district_id',
			'{{%district}}',
			'id',
			'CASCADE'
		);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
