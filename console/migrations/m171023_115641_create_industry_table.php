<?php

use common\util\Migration;


/**
 * Handles the creation of table `industry`.
 */
class m171023_115641_create_industry_table extends Migration
{
	public $tableName = '{{%industry}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull()->unique(),
			'sort' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
