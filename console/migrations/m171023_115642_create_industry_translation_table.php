<?php

use common\util\Migration;


/**
 * Handles the creation of table `industry_translation`.
 */
class m171023_115642_create_industry_translation_table extends Migration
{
	public $tableName = '{{%industry_translation}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
			'industry_id' => $this->integer()->notNull(),
			'name' => $this->string(),
			'language' => $this->string(16),
        ]);

		// fk
		$this->addForeignKey(
			'fk-industry_translation-industry_id-industry-id',
			$this->tableName,
			'industry_id',
			'{{%industry}}',
			'id',
			'CASCADE'
		);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
