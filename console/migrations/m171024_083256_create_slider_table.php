<?php

use common\util\Migration;


/**
 * Handles the creation of table `slider`.
 */
class m171024_083256_create_slider_table extends Migration
{
    public $tableName = '{{%slider}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
			'title' => $this->string(),
			'description' => $this->string(),
            'image' => $this->string()->notNull(),
            'link' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
