<?php

use common\util\Migration;


/**
 * Handles the creation of table `slider_translation`.
 */
class m171024_083723_create_slider_translation_table extends Migration
{
    public $tableName = '{{%slider_translation}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'slider_id' => $this->integer()->notNull(),
            'language' => $this->string(16),
            'title' => $this->string(),
            'description' => $this->string(),
        ]);

        // ForeignKey
        $this->addForeignKey('fk-slider_translation-slider_id-slider-id', $this->tableName, 'slider_id', '{{%slider}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
