<?php

use common\util\Migration;


/**
 * Handles the creation of table `news`.
 */
class m171025_052532_create_news_table extends Migration
{
    public $tableName = '{{%news}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull()->unique(),
            'desc' => $this->string(255),
            'text' => $this->text()->notNull(),
            'img' => $this->string()->notNull(),
            'fixed' => $this->boolean(),
            'published_at' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // fk
        $this->addForeignKey(
            'fk-news-user_id-user-id',
            $this->tableName,
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
