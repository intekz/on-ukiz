<?php

use common\util\Migration;


/**
 * Handles the creation of table `news_translation`.
 */
class m171025_052620_create_news_translation_table extends Migration
{
    public $tableName = '{{%news_translation}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->notNull(),
            'language' => $this->string(16),
            'title' => $this->string(255)->notNull(),
            'desc' => $this->string(255)->notNull(),
            'text' => $this->text(),
        ]);

        // ForeignKey
        $this->addForeignKey('fk-news_translation-news_id-news-id', $this->tableName, 'news_id', '{{%news}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
