<?php

use common\util\Migration;


/**
 * Handles the creation of table `feedback`.
 */
class m171025_093017_create_feedback_table extends Migration
{
    public $tableName = '{{%feedback}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city' => $this->string(),
            'email' => $this->string(255),
            'message' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
