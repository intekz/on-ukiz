<?php

use common\util\Migration;


/**
 * Handles the creation of table `tender`.
 */
class m171110_120012_create_tender_table extends Migration
{
    public $tableName = '{{%tender}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'desc' => $this->string(255)->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'type' => $this->smallInteger()->notNull(),
            'published_at' => $this->integer(),

            'status' => $this->smallInteger()->defaultValue(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
