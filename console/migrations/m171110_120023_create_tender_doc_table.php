<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tender_doc`.
 */
class m171110_120023_create_tender_doc_table extends Migration
{
    public $tableName = '{{%tender_doc}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'tender_id' => $this->integer()->notNull(),
            'language' => $this->string(16),
            'name' => $this->string()->notNull(),
            'file' => $this->string(),
            'type' => $this->string()
        ]);

        // ForeignKey
        $this->addForeignKey('fk-tender_doc-tender_id-tender-id', $this->tableName, 'tender_id', '{{%tender}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
