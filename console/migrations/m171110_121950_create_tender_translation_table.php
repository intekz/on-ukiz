<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tender_translation`.
 */
class m171110_121950_create_tender_translation_table extends Migration
{
    public $tableName = '{{%tender_translation}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'tender_id' => $this->integer()->notNull(),
            'language' => $this->string(16),
            'name' => $this->string(255)->notNull(),
            'desc' => $this->string(255)->notNull(),
        ]);

        // ForeignKey
        $this->addForeignKey(
            'fk-tender_translation-tender_id-tender-id',
            $this->tableName,
            'tender_id',
            '{{%tender}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
