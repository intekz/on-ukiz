<?php

use common\util\Migration;


/**
 * Handles the creation of table `project`.
 */
class m171121_110153_create_project_table extends Migration
{
	public $tableName = '{{%project}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
			'id' => $this->primaryKey(),
			'district_id' => $this->integer(),
			'industry_id' => $this->integer(),

			'name' => $this->string(255)->notNull(),
			'slug' => $this->string()->notNull()->unique(),
			'img' => $this->string(255),
			'desc' => $this->text(),
			'desc_short' => $this->string(255)->notNull(),
			'address' => $this->string(255),
			'price' => $this->decimal(20),
			'stage' => $this->smallInteger()->notNull()->defaultValue(10),
			'phones_json' => $this->text(),
			'emails_json' => $this->text(),

			'sort' => $this->integer()->defaultValue(0),
			'status' => $this->smallInteger()->notNull()->defaultValue(10),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
        ]);

        // fk
		$this->addForeignKey(
			'project-district_id-district-id',
			$this->tableName,
			'district_id',
			'{{%district}}',
			'id',
			'SET NULL'
		);

		$this->addForeignKey(
			'project-industry_id-industry-id',
			$this->tableName,
			'industry_id',
			'{{%industry}}',
			'id',
			'SET NULL'
		);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
