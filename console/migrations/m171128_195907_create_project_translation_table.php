<?php

use common\util\Migration;


/**
 * Handles the creation of table `project_translation`.
 */
class m171128_195907_create_project_translation_table extends Migration
{
	public $tableName = '{{%project_translation}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
			'project_id' => $this->integer()->notNull(),
			'name' => $this->string(255)->notNull(),
            'language' => $this->string(16),
			'desc' => $this->text(),
			'desc_short' => $this->string(255),
			'address' => $this->string(255),
        ]);

        // fk
		$this->addForeignKey(
			'fk-project_translation-project_id-project-id',
			$this->tableName,
			'project_id',
			'{{%project}}',
			'id',
			'CASCADE'
		);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
