<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_image`.
 */
class m180110_114203_create_project_image_table extends Migration
{
	public $tableName = '{{%project_image}}';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable($this->tableName, [
			'id' => $this->primaryKey(),
			'project_id' => $this->integer()->notNull(),
			'name' => $this->string()->notNull(),
			'main' => $this->boolean(),
			'sort' => $this->integer()->defaultValue(0)
		]);

		$this->addForeignKey(
			'fk-project_image-project_id-project-id',
			$this->tableName,
			'project_id',
			'{{%project}}',
			'id',
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable('project_image');
	}
}