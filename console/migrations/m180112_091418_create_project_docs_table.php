<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_docs`.
 */
class m180112_091418_create_project_docs_table extends Migration
{
	public $tableName = '{{%project_docs}}';

	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable($this->tableName, [
			'id' => $this->primaryKey(),
			'project_id' => $this->integer()->notNull(),
			'name' => $this->string(),
			'language' => $this->string(16),
			'file' => $this->string()->notNull(),
			'sort' => $this->integer()
		]);

		$this->addForeignKey(
			'fk-project_docs-project_id-project-id',
			$this->tableName,
			'project_id',
			'{{%project}}',
			'id',
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable($this->tableName);
	}
}