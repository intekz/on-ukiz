<?php

use common\models\News;
use common\models\Project;
use common\util\Migration;


/**
 * Handles the creation of table `search`.
 */
class m180126_112016_create_search_table extends Migration
{
    public $tableName = '{{%search}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute('CREATE OR REPLACE VIEW ie_search AS
            SELECT `id`,`slug`,`status`,`name` AS title, `desc` AS text, 1 AS type FROM ie_project UNION
            SELECT `id`,`slug`,`status`,`name` AS title, `desc` AS text, 3 AS type FROM ie_tender UNION
            SELECT `id`,`slug`,`status`,`title`,`text`, 2 AS type FROM ie_news');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m180126_112016_create_search_table.\n";

        return false;
    }
}
