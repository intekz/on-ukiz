<?php

use common\util\Migration;


/**
 * Handles the creation of table `article`.
 */
class m180207_093858_create_article_table extends Migration
{
    public $tableName = '{{%article}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'text' => $this->text(),

            // SEO
            'meta_title' => $this->string(),
            'meta_desc' => $this->string(),
            'meta_keywords' => $this->string(),

            // OG
            'og_title' => $this->string(),
            'og_desc' => $this->string(),
            'og_img' => $this->string(),
            'og_type' => $this->string(),
            'og_url' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
