<?php

use common\util\Migration;


/**
 * Handles the creation of table `article_translate`.
 */
class m180207_095816_create_article_translate_table extends Migration
{
    public $tableName = '{{%article_translate}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'language' => $this->string(16),
            'title' => $this->string(255),
            'text' => $this->string(255),

            'meta_title' => $this->string(),
            'meta_desc' => $this->string(),
            'meta_keywords' => $this->string(),
        ]);

        // fk
        $this->addForeignKey(
            'fk-article-translate-article_id-article-id',
            $this->tableName,
            'article_id',
            '{{%article}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
