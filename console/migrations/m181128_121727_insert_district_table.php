<?php

use yii\db\Migration;

/**
 * Class m171128_121727_insert_district_table
 */
class m181128_121727_insert_district_table extends Migration
{
	public $tableName = '{{%district}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
    	// district
    	$this->batchInsert($this->tableName, ['name', 'sort'], [
    		['г.Шымкент', 1],
    		['г.Арыс', 2],
    		['г.Кентау', 3],
    		['г.Туркестан', 4],
    		['Байдибекский район', 5],
    		['Казыгуртский район', 6],
    		['Мактааральский район', 7],
    		['Ордабасинский район', 8],
    		['Отырарский район', 9],
    		['Сайрамский район', 10],
    		['Сарыагашский район', 11],
    		['Сузакский район', 12],
    		['Толебийский район', 13],
    		['Тюлькубасский район', 14],
    		['Шардаринский район', 15],
		]);

    	// translation
		$this->batchInsert('{{%district_translation}}', ['district_id', 'name', 'language'], [
			[1, 'Шымкент', 'kz'],
			[1, 'c.Shymkent', 'en'],
		]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171128_121727_insert_district_table cannot be reverted.\n";

        return false;
    }
}
