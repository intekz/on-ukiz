<?php

use yii\db\Migration;

/**
 * Class m171128_121808_insert_industry_table
 */
class m181128_121808_insert_industry_table extends Migration
{
	public $tableName = '{{%industry}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
    	// industry
    	$this->batchInsert($this->tableName, ['name', 'sort'], [
    		['Энергетика', 1],
			['Строительство', 2],
			['Недропользование', 3],
			['Агропромышленность', 4],
			['Информационные технологии смарт', 5],
		]);

		// translation
		$this->batchInsert('{{%industry_translation}}', ['industry_id', 'name', 'language'], [
			[1, 'Энергетика', 'kz'],
			[1, 'Energetics', 'en'],
		]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171128_121808_insert_industry_table cannot be reverted.\n";

        return false;
    }
}
