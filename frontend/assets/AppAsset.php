<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fancybox/jquery.fancybox.min.css',
        'css/fonts.css',
        'owl/owl.carousel.css',
        'owl/owl.theme.default.min.css',
        'css/home.css',
        'css/style.css',
        'css/language.css',
    ];
    public $js = [
        'fancybox/jquery.fancybox.min.js',
        'owl/owl.carousel.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
