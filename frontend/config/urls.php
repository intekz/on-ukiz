<?php

return [

    '' => 'site/index',
    '<_a:(contact|about|procurement|logistic|industrial|participants|procurement-item|package|feedback|sozak|ontustik|tassai|turkistan|kentau|badam|maktaaral|shardara|kazygurt|tyulkubas|baydibek|search)>' => 'site/<_a>',

    // news
    [
        'class' => 'yii\web\GroupUrlRule',
        'prefix' => 'news',
        'rules' => [
            '' => 'index',
            'add' => 'add',
            '<slug:[a-zA-Z0-9_-]{1,255}+>' => 'view',
        ],
    ],
    // projects
    [
        'class' => 'yii\web\GroupUrlRule',
        'prefix' => 'projects',
        'rules' => [
            '' => 'index',
            '<slug:[a-zA-Z0-9_-]{1,255}+>' => 'view',
        ],
    ],
    // tender
    [
        'class' => 'yii\web\GroupUrlRule',
        'prefix' => 'tender',
        'rules' => [
            '' => 'index',
            '<slug:[a-zA-Z0-9_-]{1,255}+>' => 'view',
        ],
    ],
	// article
	[
		'class' => 'yii\web\GroupUrlRule',
		'prefix' => 'article',
		'rules' => [
			'' => 'index',
			'<slug:[a-zA-Z0-9_-]{1,255}+>' => 'view',
		],
	],
];