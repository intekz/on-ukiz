<?php
/**
 * Created by PhpStorm.
 * User: alexssdd
 * Date: 07.02.18
 * Time: 17:26
 */

namespace frontend\controllers;


use common\models\Article;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ArticleController
 * @package frontend\controllers
 */
class ArticleController extends Controller
{
	/**
	 * @param $slug
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionView($slug)
	{
		$model = Article::find()->published()->andWhere(['slug' => $slug])->one();

		if (!$model) {
			throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
		}

		return $this->render('view', [
			'model' => $model
		]);
	}
}