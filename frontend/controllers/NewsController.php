<?php
namespace frontend\controllers;

use frontend\models\NewsFilter;
use Yii;
use common\models\News;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * News controller
 */
class NewsController extends Controller
{
    public function actionIndex()
    {
        $newsFilter = new NewsFilter();
        $provider = $newsFilter->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'provider' => $provider,
            'newsFilter' => $newsFilter
        ]);
    }

    public function actionView($slug)
    {
        $model = News::find()
            ->andWhere(['status'=> News::STATUS_PUBLISHED])
            ->andWhere(['slug' => $slug])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
        }

        return $this->render('view',[
            'model' => $model
        ]);
    }
}