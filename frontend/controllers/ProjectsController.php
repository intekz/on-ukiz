<?php
/**
 * Created by PhpStorm.
 * User: alexssdd
 * Date: 28.11.17
 * Time: 14:09
 */

namespace frontend\controllers;


use Yii;
use common\models\Project;
use yii\web\Controller;
use frontend\models\ProjectFilter;
use yii\web\NotFoundHttpException;

/**
 * Class ProjectsController
 * @package frontend\controllers
 */
class ProjectsController extends Controller
{
	public function actionIndex()
	{
		$projectFilter = new ProjectFilter();
		$projectCount = Project::find()->published()->count();
		$dataProvider = $projectFilter->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'projectCount' => $projectCount,
			'dataProvider' => $dataProvider,
			'projectFilter' => $projectFilter
		]);
	}

	/**
	 * @param $slug
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionView($slug)
	{
		$model = Project::find()->published()->forSlug($slug)->one();

		if (!$model) {
			throw new NotFoundHttpException();
		}

		return $this->render('view', [
			'model' => $model
		]);
	}
}