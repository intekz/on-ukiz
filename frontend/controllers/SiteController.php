<?php
namespace frontend\controllers;

use common\models\Slider;
use frontend\models\SiteSearch;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function actionSearch()
	{
		$siteSearch = new SiteSearch();
		$provider = $siteSearch->search(Yii::$app->request->queryParams);

		return $this->render('search/index', [
			'provider' => $provider,
			'siteSearch' => $siteSearch
		]);
	}

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $slider = Slider::find()
            ->andWhere(['status' => Slider::STATUS_PUBLISHED])
            ->all();

        return $this->render('index', [
            'slider' => $slider,
        ]);
    }
    public function actionFeedback()
    {
        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/site/index']);
        }
    }
    public function actionProcurement()
    {
        return $this->render('procurement');
    }
    public function actionLogistic()
    {
        return $this->render('logistic');
    }
    public function actionIndustrial()
    {
        return $this->render('industrial');
    }
    public function actionOntustik()
    {
        return $this->render('industrial/ontustik');
    }
    public function actionTassai()
    {
        return $this->render('industrial/tassai');
    }
    public function actionTurkistan()
    {
        return $this->render('industrial/turkistan');
    }
    public function actionKentau()
    {
        return $this->render('industrial/kentau');
    }
    public function actionBadam()
    {
        return $this->render('industrial/badam');
    }
    public function actionMaktaaral()
    {
        return $this->render('industrial/maktaaral');
    }
    public function actionShardara()
    {
        return $this->render('industrial/shardara');
    }
    public function actionKazygurt()
    {
        return $this->render('industrial/kazygurt');
    }
    public function actionTyulkubas()
    {
        return $this->render('industrial/tyulkubas');
    }
    public function actionBaydibek()
    {
        return $this->render('industrial/baydibek');
    }
    public function actionSozak()
    {
        return $this->render('industrial/sozak');
    }
    public function actionParticipants()
    {
        return $this->render('participants');
    }
    public function actionProcurementItem()
    {
        return $this->render('procurement_item');
    }
    public function actionPackage()
    {
        return $this->render('package');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
