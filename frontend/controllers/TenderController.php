<?php
/**
 * Created by PhpStorm.
 * User: alexssdd
 * Date: 28.11.17
 * Time: 14:09
 */

namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use common\models\Tender;
use frontend\models\TenderFilter;
use yii\web\NotFoundHttpException;

/**
 * Class ProjectsController
 * @package frontend\controllers
 */
class TenderController extends Controller
{
    public function actionIndex()
    {
        $tenderFilter = new TenderFilter();
        $dataProvider = $tenderFilter->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tenderFilter' => $tenderFilter
        ]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $model = Tender::find()->andWhere(['status' => Tender::STATUS_PUBLISHED, 'slug' => $slug])->one();

        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model
        ]);
    }
}