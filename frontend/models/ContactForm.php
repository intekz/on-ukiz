<?php

namespace frontend\models;

use common\models\Feedback;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $city;
    public $message;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'city', 'message'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('feedback', 'Name'),
            'email' => Yii::t('feedback', 'Email'),
            'city' => Yii::t('feedback', 'City'),
            'message' => Yii::t('feedback', 'Message'),
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $feedback = new Feedback();
        $feedback->name = $this->name;
        $feedback->city = $this->city;
        $feedback->email = $this->email;
        $feedback->message = $this->message;
        $feedback->status = Feedback::STATUS_NEW;

        if ($feedback->save()) {
            $this->sendMail($feedback);
            return $feedback;
        }
        return null;
    }

    public function sendMail($feedback)
    {
        return Yii::$app->mailer->compose(['html' => 'Feedback-html'], ['feedback' => $feedback])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo(Yii::$app->params['supportEmail'])
            ->setSubject(Yii::t('feedback', 'UKIZ Feedback #{id}', ['id' => $feedback->id]))
            ->send();
    }
}
