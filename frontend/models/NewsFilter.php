<?php
/**
 * Created by PhpStorm.
 * User: alexssdd
 * Date: 09.01.18
 * Time: 17:45
 */

namespace frontend\models;


use common\models\News;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class NewsFilter extends Model
{
    public $date;
    public $type;

    public function rules()
    {
        return [
            ['date', 'date', 'format' => 'php:d.m.Y']
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find()
            ->andWhere(['status' => News::STATUS_PUBLISHED])
            ->orderBy(['fixed' => SORT_DESC, 'id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        if ($this->date) {
            $date = Yii::$app->formatter->asTimestamp($this->date);
            $query->andFilterWhere(['=', 'published_at', $date]);
        }


        return $dataProvider;
    }
}