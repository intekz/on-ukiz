<?php

namespace frontend\models;


use yii\base\Model;
use common\models\Project;
use yii\data\ActiveDataProvider;

class ProjectFilter extends Model
{
	public $stage;
	public $priceFrom;
	public $priceTo;
	public $districtId;
	public $industryId;

	public function rules()
	{
		return [
			[['stage', 'districtId', 'industryId'], 'integer'],
			[['priceFrom', 'priceTo'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = Project::find()->published();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 12
			],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'stage' => $this->stage,
			'district_id' => $this->districtId,
			'industry_id' => $this->industryId
		]);
		$query->andFilterWhere(['>=', 'price', $this->priceFrom]);
		$query->andFilterWhere(['<=', 'price', $this->priceTo]);
		$query->andFilterWhere(['between', 'price', $this->priceFrom, $this->priceTo]);

		return $dataProvider;
	}
}