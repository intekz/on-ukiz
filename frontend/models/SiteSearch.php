<?php
/**
 * Created by PhpStorm.
 * User: alexssdd
 * Date: 30.01.18
 * Time: 13:59
 */

namespace frontend\models;


use common\models\Search;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SiteSearch extends Model
{
    public $q;

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            ['q', 'string', 'max' => 255]
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Search::find()->alias('t1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 12]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (Yii::$app->language == 'ru') {
            $query->andFilterWhere([
                'or',
                ['like', 't1.title', $this->q],
                ['like', 't1.text', $this->q],
            ]);
        } else {
            $query->joinWith(['projectTranslation t2', 'newsTranslation t3', 'tenderTranslation t4']);

            $query->andFilterWhere([
                'or',
                ['like', 't2.name', $this->q],
                ['like', 't2.desc', $this->q],
                ['like', 't3.title', $this->q],
                ['like', 't3.desc', $this->q],
                ['like', 't4.name', $this->q],
                ['like', 't4.desc', $this->q],
            ]);
        }




        return $dataProvider;
    }
}