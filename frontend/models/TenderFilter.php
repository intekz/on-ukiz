<?php

namespace frontend\models;


use Yii;
use yii\base\Model;
use common\models\Tender;
use yii\data\ActiveDataProvider;

class TenderFilter extends Model
{
    public $date;
    public $type;

    public function rules()
    {
        return [
            ['type', 'integer'],
            ['date', 'date', 'format' => 'php:d.m.Y']
        ];
    }

    public function search($params)
    {
        $query = Tender::find()->andWhere(['status' => Tender::STATUS_PUBLISHED]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 12
            ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['type' => $this->type]);

        if ($this->date) {
            $date = Yii::$app->formatter->asTimestamp($this->date);
            $query->andFilterWhere(['=', 'published_at', $date]);
        }

        return $dataProvider;
    }
}