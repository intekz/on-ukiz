<?php
use frontend\widgets\StateWebsiteWidget;

/* @var $this \yii\web\View */
/* @var $model \common\models\Article */

$model->meta_desc ? $this->registerMetaTag(['description' => $model->getMetaDescTranslate()]) : null;
$this->title = $model->getMetaTitleTranslate() ? $model->getMetaTitleTranslate() : $model->getTitleTranslate();
?>
<div class="procurement-item">
	<div class="procurement-item__breadcrumbs">
		<div class="container">
			<div class="procurement-item__page_name"><?= $model->getTitleTranslate();?></div>
		</div>
	</div>
	<div>
		<?= $model->getTextTranslate(); ?>
	</div>
</div>
<?= StateWebsiteWidget::widget();?>