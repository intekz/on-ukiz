<?php

use frontend\widgets\SubsidiaryWidget;
use yii\widgets\Menu;
?>
<?= SubsidiaryWidget::widget();?>
<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <?= Menu::widget([
                'options' => [
                    'class' => 'footer-top__list',
                ],
                'items' => [
                    ['label' => Yii::t('site', 'About'), 'url' => ['/site/about']],
                    ['label' => Yii::t('site', 'Projects'), 'url' => ['site/project']],
                    ['label' => Yii::t('site', 'Become a party'), 'url' => ['site/participants']],
                    ['label' => Yii::t('site', 'Industrial zone'), 'url' => ['site/industrial']],
                    ['label' => Yii::t('site', 'Procurement'), 'url' => ['/tender/index']],
                    ['label' => Yii::t('site', 'Media'), 'url' => ['news/index']],
                ],
                'activeCssClass'=>'active',
                'itemOptions'=>['class'=>'footer-top__item'],
                'linkTemplate' => '<a href="{url}" class="footer-top__link">{label}</a>',
            ]);?>
        </div>
    </div>
    <div class="footer-middle">
        <div class="container">
            <div class="footer-middle__contact">
                <div class="footer-middle__contact_title">
                    <?= Yii::t('site', 'Contacts')?>
                </div>
                <div class="footer-middle__contact_text">
                    <ul>
                        <li><a class="footer-middle__contact_link" href="tel:+77252771100">+7 (7252) 77-11-00</a></li>
                        <li><a class="footer-middle__contact_link" href="tel:+77252770586">+7 (7252) 77-05-86</a></li>
                        <li><a class="footer-middle__contact_link" href="tel:+77252393880">+7 (7252) 77-05-74</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-middle__address">
                <div class="footer-middle__address_title">
                    <?= Yii::t('site', 'Address')?>
                </div>
                <div class="footer-middle__address_text">
                    <ul>
                        <li>160000</li>
                        <li>Республика казахстан, г. Шымкент,<br> ул. Капал батыра, 5км,<br>
                            Индустриальная зона «Оңтүстік»</li>
                    </ul>
                </div>
            </div>
            <!--<div class="footer-middle__visitation">
                <div class="footer-middle__visitation_title">
                    <?= Yii::t('site', 'Number of visits')?>
                </div>
                <div class="footer-middle__visitation_text">
                    <ul>
                        <li>66455 уникальных посетителей сегодня</li>
                        <li>919999 всего посетили портал</li>
                    </ul>
                </div>
            </div>-->
            <div class="footer-middle__social">
                <ul class="footer-middle__social_list">
                    <li class="footer-middle__social_item"><a class="footer-middle__social_link facebook" href="#"><span class="icon-facebook"></span></a></li>
                    <li class="footer-middle__social_item"><a class="footer-middle__social_link youtube" href="#"><span class="icon-youtube"></span></a></li>
                    <li class="footer-middle__social_item"><a class="footer-middle__social_link instagram" href="#"><span class="icon-instagram"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom__info">
                <div class="footer-bottom__img"><img src="/images/logo.png" width="106" height="64" alt=""></div>
                <div class="footer-bottom__text">УПРАВЛЯЮЩАЯ КОМПАНИЯ ИНДУСТРИАЛЬНЫМИ ЗОНАМИ "ONTUSTIK"</div>
                <div class="footer-bottom__text">© 2018  Официальный Интернет-ресурс</div>
            </div>
        </div>
    </div>
</footer>