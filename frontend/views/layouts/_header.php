<?php

use frontend\widgets\LanguageSwitcher;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
?>
<header class="header">
    <div class="container header-container">
        <div class="header-top">
            <div class="header-top__logo">
                <a href="/"><img src="/images/logo.png" alt="Logotype"></a>
            </div>
            <div class="header-top__title">
                <div class="header-top__title_text">
                    <?= Yii::t('site', 'The management company of industrial zones "Ontustik"')?>
                </div>
            </div>
            <div class="header-top__main-logo">
                <div class="header-top__main-logo_img">
                    <a href="#"><img src="/images/logo1.png" alt=""></a>
                    <a href="#"><img src="/images/logo2.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="header-middle">
            <div class="header-middle__contact">
                <a class="header-middle__contact_link" href="<?= Url::to('/contact') ?>"><?= Yii::t('site', 'Contacts')?></a>
            </div>
            <div class="header-middle__search">
	            <?= Html::beginForm(['/site/search'], 'get')?>
                <input class="header-middle__search_input" type="text" name="q" value="<?= Html::encode($q)?>" required placeholder="<?= Yii::t('site', 'Site search')?>">
                <button class="header-middle__search_btn"><span class="icon-search"></span></button>
	            <?= Html::endForm()?>
            </div>
            <div class="header-middle__language">
                <?= LanguageSwitcher::widget()?>
            </div>
            <div class="header-middle__social">
                <ul class="header-middle__social_list">
                    <li><a class="header-middle__social_item facebook" href="#"><span class="icon-facebook"></span></a></li>
                    <li><a class="header-middle__social_item youtube" href="#"><span class="icon-youtube"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <?= Menu::widget([
                'options' => [
                    'class' => 'header-bottom__list',
                ],
                'items' => [
                    ['label' => Yii::t('site', 'About'), 'url' => ['/article/view', 'slug' => 'o-kompanii']],
                    ['label' => Yii::t('site', 'Projects'), 'url' => ['/projects/index']],
                    ['label' => Yii::t('site', 'Become a party'), 'url' => ['/article/view', 'slug' => 'stat-uchastnikom']],
                    ['label' => Yii::t('site', 'Industrial zone'), 'url' => ['site/industrial']],
                    ['label' => Yii::t('site', 'Procurement'), 'url' => ['/tender/index']],
                    ['label' => Yii::t('site', 'Media'), 'url' => ['/news/index']],
                ],
                'activeCssClass'=>'active',
                'itemOptions'=>['class'=>'header-bottom__item'],
                'linkTemplate' => '<a href="{url}" class="header-bottom__link">{label}</a>',
            ]);?>
        </div>
    </div>
</header>