<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\News */
/* @var $newsFilter \frontend\models\NewsFilter */
/* @var $provider \yii\data\ActiveDataProvider */

use frontend\widgets\CalendarWidget;
use frontend\widgets\SubsidiaryWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'УК "Транспортно-логистическая зона"';
?>
<div class="container">
    <section class="media">
        <div class="media-title"><?= Yii::t('site', 'Media')?></div>
        <div class="media-left">
            <div class="media-nav">
                <ul class="media-nav__list">
                    <li class="<?= $newsFilter->date ? '' : 'active'?>">
                        <?= Html::a(Yii::t('site', 'All News'), ['/news/index'])?>
                    </li>
                </ul>
                <div class="calendar-wrapper">
                    <?= CalendarWidget::widget([
                        'model' => $newsFilter
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="media-content">
            <?php if ($provider->getModels()):?>
            <div class="media__list">
                <?php foreach ($provider->getModels() as $model):?>
                    <div class="media__item">
                        <a class="media__item__img" href="<?= Url::to(['/news/view', 'slug' => $model->slug]) ?>" style="background-image: url('<?= $model->getImgUrl()?>')">
                            <div class="news__item_info">
                                <div class="news__item_date"><?= Yii::$app->formatter->asDate($model->getPublishedDate(), 'dd/MM/yyyy') ?></div>
                                <div class="news__item__title">
                                    <?= $model->getTitleTranslate(); ?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= LinkPager::widget([
                'options' => ['class' => 'pagination'],
                'pagination' => $provider->getPagination(),
                'registerLinkTags' => true,
            ]) ?>
        </div>
        <?php else:?>
            <div class="empty-page">
                <div class="empty-page__title"><?= Yii::t('news', 'At the moment there is no published news')?></div>
            </div>
        <?php endif;?>
    </section>
</div>