<?php

use frontend\widgets\LastNewsWidget;
use frontend\widgets\SubsidiaryWidget;

/* @var $this \yii\web\View */
/* @var $model \common\models\News */

$this->title = $model->getTitleTranslate();
?>
<div class="news-clause__breadcrumbs">
    <div class="container">
        <div class="news-clause__page_name"><?= $model->getTitleTranslate(); ?></div>
    </div>
</div>
<div class="container">
    <div class="news-clause">
        <div class="news-item">
            <div class="news-item__data"><?= Yii::$app->formatter->asDate($model->created_at, 'dd/MM/yyyy') ?></div>
            <div class="news-item__title"><?= $model->getTitleTranslate(); ?></div>
            <img class="news-item__image" src="<?= $model->getImgUrl()?>" alt="">
            <div class="news-item__typography">
                <?= $model->getTextTranslate();?>
            </div>
        </div>
        <?= LastNewsWidget::widget([
            'currentNewsId' => $model->id
        ]); ?>
    </div>
</div>
