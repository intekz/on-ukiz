<?php

use common\models\District;
use common\models\Industry;
use common\models\Project;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $projectFilter \frontend\models\ProjectFilter */
/* @var $projectCount \common\models\Project */

?>
<div class="project-nav">
	<div class="project-nav__total"><span><?= Yii::t('project', 'Total: {n, plural, =0{no projects} =1{# project} one{# project} few{# projects} many{# projects} other{# projects}}', ['n' => $projectCount])?></span></div>
	<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);?>
    <ul class="project-nav__list">
        <li>
            <?= Html::radio('ProjectFilter[stage]', false, ['value' => '', 'id' => 'project__filter_stage-all']); ?>
            <?= Html::label(Yii::t('project', 'All project') , 'project__filter_stage-all'); ?>
        </li>
        <?php foreach (Project::getStages() as $k => $stage): ?>
            <?php $id = 'project__filter_stage-' . $k; ?>
            <?php $checked = $projectFilter->stage == $k ? true : false; ?>

            <li>
                <?= Html::radio('ProjectFilter[stage]', $checked, ['id' => $id, 'value' => $k]); ?>
                <?= Html::label($stage, $id); ?>
            </li>
        <?php endforeach; ?>

	</ul>
	<div class="project__filter_title">Фильтр</div>
	<div class="project__filter_select">
        <?= $form->field($projectFilter, 'districtId')
            ->dropDownList(
                ArrayHelper::map(
                    District::find()->orderBy(['sort' => SORT_ASC])->all(),
                    'id',
                    'nameTranslate'
                ),
                ['prompt' => 'Все']
            )
            ->label(false);
        ?>
	</div>
	<div class="project__filter_select">
        <?= $form->field($projectFilter, 'industryId')
            ->dropDownList(
                ArrayHelper::map(
                    Industry::find()->orderBy(['sort' => SORT_ASC])->all(),
                    'id',
                    'nameTranslate'
                ),
                ['prompt' => 'Все']
            )
            ->label(false);
        ?>
	</div>
	<div class="project__price_title"><?= Yii::t('project', 'COST (millions tng)') ?></div>
	<div class="row">
		<div class="project__price_input">
            <?= Html::activeInput('number', $projectFilter, 'priceFrom', ['min' => 0, 'placeholder' => 'от____'])?>
		</div>
		<div class="project__price_input">
            <?= Html::activeInput('number', $projectFilter, 'priceTo', ['min' => 0, 'placeholder' => 'до____'])?>
		</div>
	</div>
	<div class="project__filter_btn">
        <?= Html::submitButton(Yii::t('project', 'Search'), ['class' => 'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>
