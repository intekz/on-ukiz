<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\Project */
?>
<div class="project__item">
	<a href="<?= Url::to(['/projects/view', 'slug' => $model->slug])?>" class="project__item_img" style="background-image: url(<?= $model->getMainImg()?>)"></a>
	<a href="<?= Url::to(['/projects/view', 'slug' => $model->slug])?>" class="project__item_title"><span><?= $model->getNameTranslate() ?></span></a>
	<div class="project__item_info">
		<div class="project-item__info_desc"><?= $model->getDescShortTranslate() ?></div>
		<div class="project-item__info_address"><?= $model->getAddressTranslate() ?></div>
		<div class="project-item__info_value"><?= Yii::$app->formatter->asDecimal($model->price, 0); ?><span> млн. тенге</span></div>
	</div>
</div>
<!--726,5 <span>млн. тенге</span>-->
