<?php

use frontend\widgets\SubsidiaryWidget;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $projectFilter \frontend\models\ProjectFilter */
/* @var $projectCount \common\models\Project */


$this->title = 'Проекты - СОЦИАЛЬНО-ПРЕДПРИНИМАТЕЛЬСКАЯ КОРПОРАЦИЯ «ШЫМКЕНТ»';
?>
<div class="container">
	<section class="project">
		<div class="project-title"><?= Yii::t('site', 'Projects')?></div>
		<div class="project-left">
            <?= $this->render('_filter', [
                'projectCount' => $projectCount,
                'projectFilter' => $projectFilter,
                'dataProvider' => $dataProvider
            ])?>
		</div>
		<div class="project-content">
			<div class="project__list">
				<?php foreach ($dataProvider->getModels() as $model): ?>
					<?= $this->render('_item', ['model' => $model])?>
				<?php endforeach;?>
			</div>

            <?= LinkPager::widget([
                'options' => ['class' => 'pagination'],
                'pagination' => $dataProvider->getPagination(),
                'registerLinkTags' => true,
            ]) ?>
		</div>
	</section>
</div>