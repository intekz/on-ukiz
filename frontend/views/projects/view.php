<?php

use frontend\widgets\SimilarProjectsWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\Project */

$this->title = 'УК "Транспортно-логистическая зона"';
?>
<div class="project-clause__breadcrumbs">
	<div class="container">
		<div class="project-clause__page_name">Проекты</div>
		<a href="<?= Url::to(['/projects/index'])?>" class="project-clause_back">Вернутся в каталог</a>
		<div class="project-clause_title"><?= $model->getNameTranslate(); ?></div>
	</div>
</div>
<div class="container project-clause">
	<div class="row">
		<div class="project-clause__block">
            <?php if ($model->images): ?>
			<section class="project-image">
                <a href="<?= $model->getMainImg()?>" class="project-image__main" style="background-image: url(<?= $model->getMainImg()?>)" data-fancybox="project-images"></a>
				<div class="clause-carousel owl-carousel">
                    <?php foreach ($model->images as $image): ?>
                        <div class="project-image__item">
                            <a href="<?= $image->getImgUrl()?>" class="project-image__thumb" style="background-image: url(<?= $image->getImgUrl()?>)" data-fancybox="project-images"></a>
                        </div>
                    <?php endforeach; ?>
				</div>
			</section>
            <?php endif; ?>
			<section class="project-contact">
				<div class="project-contact__title"><?= Yii::t('site', 'Contacts') ?></div>
				<div class="project-contact__info">
                    <div class="project-contact__address"><?= $model->getAddressTranslate(); ?></div>
                    <div class="project-contact__phone">Т: <?= $model->phones_json; ?></br></div>
                    <div class="project-contact__email">Е-mail: <a href="mailto:<?= $model->emails_json?>"><?= $model->emails_json; ?></a></div>
				</div>
			</section>
            <?php if ($model->projectDocsLanguage):?>
                <section class="detailed-info">
                    <div class="detailed-info__title"><?= Yii::t('project', 'Project detail docs')?></div>
                    <?php foreach ($model->projectDocsLanguage as $doc): ?>
                        <a href="<?= $doc->getFileUrl()?>" download="" class="detailed-info__link"><?= $doc->name?></a>
                    <?php endforeach;?>
                </section>
            <?php endif;?>
		</div>
		<div class="project-clause__block">
			<div class="about-project">
				<div class="about-project__value">
					<div class="about-project__value_title"><?= Yii::t('project', 'Sale project') ?></div>
					<div class="about-project__value_number"><?= Yii::$app->formatter->asDecimal($model->price, 0); ?><span> млн. тенге</span></div>

				</div>
				<div class="about-project__target">
					<div class="about-project__target_title"><?= Yii::t('project', 'Project characteristics') ?></div>
					<div class="about-project__target_text">
                        <?= $model->getDescTranslate(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="project-clause__block">
            <?= SimilarProjectsWidget::widget([
                'currentIndustryId' => $model->industry_id,
                'limit' => 3,
                'currentProjectId' => $model->id
            ])?>
		</div>
	</div>
</div>
<?php
$js = <<<JS
$('.clause-carousel').owlCarousel({
        margin:5,
        loop:false,
        nav:false,
        items:5
    })
JS;
$this->registerJs($js)
?>
