<?php

/* @var $this yii\web\View */

use frontend\widgets\FeedbackWidget;
use frontend\widgets\SubsidiaryWidget;
use yii\helpers\Url;

$this->title = 'Контакты';
?>
<div class="contact__breadcrumbs">
    <div class="container">
        <div class="contact__page_name">Контакты</div>
    </div>
</div>
<div class="container">
    <div class="contact__block">
        <div class="contact__info">
            <div class="contact__info_title"> </div>
            <table class="contact__table">
                <tr>
                    <th>Адрес:</th>
                    <td>г.Шымкент,  ул. Капал батыра, 5км,<br>
                        Индустриальная зона «Оңтүстік»
                    </td>
                </tr>
                <tr>
                    <th>Телефоны:</th>
                    <td>8 (7252) 77-11-00 (приемная)</br>
                        8 (7252) 77-05-86 (отдел мониторинга)</br>
                        8 (7252) 77-05-74 (бухгалтерия)</br>
                    </td>
                </tr>
                <tr>
                    <th>Е-майл:</th>
                    <td> income@iz-ontustik.kz</td>
                </tr>
                <tr>
                    <th>Режим работы:</th>
                    <td >Пн. — Пт.: 09.00 – 19.00</br>
                        Обед: 13:00 - 15:00</br>
                        Выходной: Суббота, Воскресенье</br>
                    </td>
                </tr>
            </table>
        </div>
        <div class="contact__map">
            <div class="contact__map_title">Схема проезда</div>
            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A94d02c0cd9af9d946b0d8e73fb394ed1a3ca48c3a080438235a8192b98ff7855&amp;lang=ru_RU&amp;scroll=true&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
        </div>
    </div>
</div>
<?= FeedbackWidget::widget();?>
