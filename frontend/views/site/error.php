<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">
    <div class="error-page">
        <div class="error-page__title">Страница не найдена</div>
        <a class="error-page__link" href="/">На главную страницу</a>
    </div>
</div>
