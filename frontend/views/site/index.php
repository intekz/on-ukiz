<?php

/* @var $this yii\web\View */
/* @var $slider \common\models\Slider */

use frontend\widgets\FeedbackWidget;
use frontend\widgets\NewsWidget;
use frontend\widgets\StateWebsiteWidget;
use frontend\widgets\SubsidiaryWidget;
use yii\helpers\Url;

$this->title = 'Управляющая компания индустриальными зонами «ONTUSTIK»';
?>
<?php if ($slider): ?>
    <section class="main-carousel owl-carousel owl-theme">
        <?php foreach ($slider as $item): ?>
            <div class="item">
                <div class="main-carousel__img" style="background-image: url('<?= $item->getImgUrl() ?>');"></div>
                <div class="main-carousel__title"><?= $item->title ?></div>
                <?php if ($item->description):?>
                    <div class="main-carousel__desc"><?= $item->description ?></div>
                <?php endif;?>
                <?php if ($item->link):?>
                    <div class="main-carousel__btn">
                        <a href="<?= $item->link ?>" class="main-carousel__link"><?= Yii::t('site', 'Learn More')?></a>
                    </div>
                <?php endif;?>
            </div>
        <?php endforeach;?>
    </section>
<?php endif;?>
<section class="info-graphics">
    <div class="container">
        <div class="info-graphics__list">
            <div class="info-graphics__item">
                <a href="javascript:void(0);"  class="modal-trigger" data-modal="modal-name">
                    <div class="info-graphics__icon">
                        <span class="icon-map"></span>
                    </div>
                    <div class="info-graphics__title">
                        <?= Yii::t('site', 'Industrial zones')?>
                    </div>
                </a>
            </div>
            <div class="info-graphics__item">
                <a href="<?=Url::to('/news')?>">
                    <div class="info-graphics__icon">
                        <span class="icon-calendar"></span>
                    </div>
                    <div class="info-graphics__title">
                        <?= Yii::t('site', 'Calendar of events')?>
                    </div>
                </a>
            </div>
            <div class="info-graphics__item">
                <a href="<?=Url::to(['article/view', 'slug' => 'otchety-po-deyatelnosti'])?>">
                    <div class="info-graphics__icon">
                        <span class="icon-file-text-o"></span>
                    </div>
                    <div class="info-graphics__title">
                        <?= Yii::t('site', 'Activity Reports')?>
                    </div>
                </a>
            </div>
            <div class="info-graphics__item" id="menu">
                <a href="#contact">
                    <div class="info-graphics__icon">
                        <span class="icon-envelope-o"></span>
                    </div>
                    <div class="info-graphics__title">
                        <?= Yii::t('site', 'Feedback')?>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="indicators">
    <div class="container">
        <div class="indicators__title">
            <?= Yii::t('site', 'Indicators')?>
        </div>
        <div class="indicators__list">
            <div class="indicators__item">
                <div class="indicators__item_title">
                    <?= Yii::t('site', 'The volume of attracted investments')?>
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 75%">75%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">70%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 73%">73%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 62%">62%</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    <?= Yii::t('site', 'Number of completed projects')?>
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 75%">75%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">70%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 73%">73%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 62%">62%</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    <?= Yii::t('site', 'Number of jobs created')?>
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 75%">75%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">70%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 73%">73%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 62%">62%</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    <?= Yii::t('site', 'Number of services rendered')?>
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 75%">75%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">70%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 73%">73%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 62%">62%</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    <?= Yii::t('site', 'Number of processed hits')?>
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 75%">75%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">70%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 73%">73%</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 62%">62%</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal" id="modal-name">
    <div class="modal-sandbox"></div>
    <div class="modal-box">
        <div class="modal-body">
            <div class="close-modal">&#10006;</div>
            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A895b07a13577c8a6d0135fde37cd1c680e8f0e46f25004c3544312fb6bf2de0d&amp;source=constructor" width="100%" height="450" frameborder="0"></iframe>
        </div>
    </div>
</div>
<?= NewsWidget::widget();?>
<?= FeedbackWidget::widget();?>
<?= StateWebsiteWidget::widget();?>
<?php
$js = <<<JS
$(".modal-trigger").click(function(e){
  e.preventDefault();
  dataModal = $(this).attr("data-modal");
  $("#" + dataModal).css({"display":"block"});
});

$(".close-modal, .modal-sandbox").click(function(){
  $(".modal").css({"display":"none"});
});
JS;
$this->registerJs($js)
?>
<!-- Модальное Окно  -->
<div id="overlay">
    <div class="popup">
        <a href="http://egov.kz/cms/ru" target="_blank"><img src="images/banner.jpg"  width="600px" height="auto" alt="Daumed Banner"></a>
        <button class="close" title="Закрыть" onclick="document.getElementById('overlay').style.display='none';"></button>
    </div>
</div>
<script type="text/javascript">
    var delay_popup = 5000;
    setTimeout("document.getElementById('overlay').style.display='block'", delay_popup);
</script>
<style>
    #overlay {
        position: fixed;
        top: 0;
        left: 0;
        display: none;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.65);
        z-index: 999;
        -webkit-animation: fade .6s;
        -moz-animation: fade .6s;
        animation: fade .6s;
        overflow: auto;
    }
    .popup {
        top: 5%;
        left: 0;
        right: 0;
        font-size: 14px;
        margin: auto;
        width: 85%;
        min-width: 320px;
        max-width: 499px;
        position: absolute;
        background: #fff;
        z-index: 1000;
        -webkit-box-shadow: 0 15px 20px rgba(0,0,0,.22),0 19px 60px rgba(0,0,0,.3);
        -moz-box-shadow: 0 15px 20px rgba(0,0,0,.22),0 19px 60px rgba(0,0,0,.3);
        -ms-box-shadow: 0 15px 20px rgba(0,0,0,.22),0 19px 60px rgba(0,0,0,.3);
        box-shadow: 0 15px 20px rgba(0,0,0,.22),0 19px 60px rgba(0,0,0,.3);
        -webkit-animation: fade .6s;
        -moz-animation: fade .6s;
        animation: fade .6s;
    }
    .close {
        color: #fff !important;
        opacity: 1;
        top: 0px;
        right: -145px;
        width: 32px;
        height: 32px;
        position: absolute;
        border: none;
        cursor: pointer;
        outline: none;

    }
    .close:before {
        color: rgba(43, 208, 21, 0.9);
        content: "X";
        font-family:  Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: normal;
        text-decoration: none;
        text-shadow: 0 -1px rgba(0, 0, 0, 0.9);
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        transition: all 0.5s;
    }
</style>
<!-- /.Модальное Окно  -->
