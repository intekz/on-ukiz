<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальная зона «КТИЗ «Бадам»';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальная зона «КТИЗ «Бадам»</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус – строящаяся</p>
                        <p>Общая площадь территории индустриальной зоны   «КТИЗ «Бадам» – 203 га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО «KAZKIOTI»</li>
                                <li>•	ТОО «Karlskrona LB AC»</li>
                                <li>•	ТОО "Green Technology industries"</li>
                                <li>•	ТОО "GMT investiment Kazahstan A.E."</li>
                                <li>•	ТОО "Madeyra- Company"</li>
                                <li>•	ТОО «TurKaz ffp»</li>
                                <li>•	ТОО «Gold Aluminum»</li>
                                <li>•	ТОО "ФЕБ строй"</li>
                                <li>•	ТОО "Актив Трайд"</li>
                                <li>•	ТОО "Aqua Therm (Аква терм)"</li>
                                <li>•	ТОО "MEB Co"</li>
                                <li>•	ТОО "Kazmerblock investment"</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 1474</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>КТИЗ Бадам</td>
                                    <td>35/10 кВ ТМ1х6300 кВа </td>
                                    <td>3 МВт/час</td>
                                    <td>4000 м3/час</td>
                                    <td>28 м3/час</td>
                                    <td>8,3 км</td>
                                    <td>27 км</td>
                                    <td>3 км</td>
                                    <td>1,5 км</td>
                                    <td>отсутствуют</td>
                                    <td>8,5 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>•	Электричество–14, 29/кВт;
                                    <ul>
                                        <li>7:00–19:00–22,13 тг/кВт</li>
                                        <li>19:00–23:00–53,75 тг/кВт</li>
                                        <li>23:00–7:00–7,24 тг/кВт</li>
                                    </ul>
                                </li>
                                <li>•	Газ–34,75 тг/м3</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о районе</strong>
                        <p>Ордабасинский район – является одним из развитых земледельческих и животноводческих районов Южно-Казахстанской области. Здесь имеются месторождения бетониотовых глин и минеральных красок, а также минеральные термальные источники.</p>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – Темирлановка</li>
                            <li>•	численность населения – 117,2 тыс. человек</li>
                            <li>•	территория – 2,7 тыс. кв. км</li>
                            <li>•	расстояние до областного центра – 60 км</li>
                            <li>•	количество сельских округов: 10</li>
                            <li>•	населенных пунктов — 59</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <strong>Животноводство:</strong>
                        <ul>
                            <li>•	КРС  — мясное направление;</li>
                            <li>•	МРС — тонкорунное овцеводство;</li>
                            <li>•	Коневодство.</li>
                        </ul>
                        <strong>Растениеводство:</strong>
                        <ul>
                            <li>•	зерновые и зернобобовые;</li>
                            <li>•	подсолнечник;</li>
                            <li>•	хлопководство;</li>
                            <li>•	овощи, фрукты, виноград, бахчевые.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Земли сельскохозяйственного назначения  94,4 тыс. га, в т. ч.:</strong>
                        <ul>
                            <li>•	посевная 53,1 тыс. га;</li>
                            <li>•	поливные 33,3 тыс. га;</li>
                            <li>•	богара 61,1 тыс. га</li>
                            <li>•	пастбище 117,8;</li>
                            <li>•	многолетние насаждения 1,0 тыс. га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего 115,1 тыс. человек, в т. ч.:</strong>
                        <ul>
                            <li>•	экономически активное  57,5 тыс. человек, в  том числе:</li>
                            <li>•	безработные  2,2 тыс. человек;</li>
                            <li>•	самозанятые –  32,5 тыс.человек.</li>
                            <li>•	среднемесячная заработная плата –  64 989 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	Юридические лица -721, действующие — 247 или 34%;</li>
                            <li>•	Крестьянские хозяйства  – 5593, действующие — 3785 или 68%;</li>
                            <li>•	Индивидуальные предприниматели-6314, действующие 4032  или 64 %.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	Объем валовой продукции сельского хозяйства – 17 945,8  млн. тенге;</li>
                            <li>•	В т. ч. — объем валовой продукции растениеводства – 8 346,5  млн. тенге;</li>
                            <li>•	Объем валовой продукции животноводства – 9 588,9  млн. тенге.</li>
                            <li>•	Объем производства промышленной продукции – 8 254,0  млн. тенге.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>