<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальная зона «Байдибек»';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальная зона «Байдибек»</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус – действующая</p>
                        <p>Общая площадь территории индустриальной зоны “Байдибек ” – 57 га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО "NB Partners"</li>
                                <li>•	ТОО "Agro Kamf Group"</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 136</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Байдибек</td>
                                    <td>от ПС 110/10 кВ XXII ПартСъезд </td>
                                    <td>2500 кВт/час</td>
                                    <td>отсутствуют</td>
                                    <td>57 м3/час</td>
                                    <td>8,5 км</td>
                                    <td>отсутствуют</td>
                                    <td>1,76 км</td>
                                    <td>1,7 км</td>
                                    <td>отсутствуют</td>
                                    <td>1,06 км</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о городе</strong>
                        <p>Байдибекский район – крупный производитель и поставщик сельскохозяйственной продукции. В районе имеются большие пастбищныеземли, развито животноводство и растениеводство.</p>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – с. Шаян</li>
                            <li>•	численность населения – 54,1 тыс. человек</li>
                            <li>•	территория – 7,2 тыс. кв. км</li>
                            <li>•	расстояние до областного центра – 110 км</li>
                            <li>•	количество сельских округов – 11</li>
                            <li>•	населенных пунктов – 52</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <ul>
                            <li>•	развитие животноводства: овцеводства, КРС, лошадей;</li>
                            <li>•	производство зерновых, масличных культур</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Всего земель сельхозназначения – 122,9 тыс. га, в том числе:</strong>
                        <ul>
                            <li>•	Посевные – 78 тыс. га</li>
                            <li>•	орошаемые – 11,4 тыс. га</li>
                            <li>•	пастбища – 399,9 тыс. га</li>
                            <li>•	многолетние насаждения – 0,3 тыс.  га</li>
                            <li>•	сенокосы – 32,0 тыс. га</li>
                        </ul>
                        <strong><i>Неосвоенные  земли –  44,9 тыс. га </i></strong>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего населения – 54,1 тыс. чел.; в т. ч.</strong>
                        <ul>
                            <li>•	экономически активное – 27,8 тыс. чел;</li>
                            <li>•	безработные -1,4 тыс. человек;</li>
                            <li>•	самозанятое – 15,8 тыс. человек</li>
                            <li>•	среднемесячная заработная плата – 63 745</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	юридические лица – 329, в т.ч. действующие -164 или  50%;</li>
                            <li>•	фермерские хозяйства  – 1669,  в т.ч. действующие -1597 или  96%;</li>
                            <li>•	субъекты МСБ – 1998, в т.ч. действующие  – 1761 или  89%</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	Объем валовой продукции сельского хозяйства – 8 140,5  млн. тенге;</li>
                            <li>•	В т. ч. – объем валовой продукции растениеводства –2622,9  млн. тенге;</li>
                            <li>•	Объем валовой продукции животноводства – 5 717,5 млн. тенге.</li>
                            <li>•	Объем производства промышленной продукции – 2 348,3 млн. тенге</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>