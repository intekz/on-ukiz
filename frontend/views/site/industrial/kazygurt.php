<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальная зона «Казыгурт»';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальная зона «Казыгурт»</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус – действующая</p>
                        <p>Общая площадь территории индустриальной зоны “Казыгурт ” – 40 га</p>

                        <div>Рабочие места, чел. – 189</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Казыгурт</td>
                                    <td>от ПС 35/10кВ  Шарапхана</td>
                                    <td>3500 кВт/час</td>
                                    <td>2500 м3/час</td>
                                    <td>15 м3/час</td>
                                    <td>7,4 км</td>
                                    <td>5,3 км</td>
                                    <td>5,5 км</td>
                                    <td>отсутствуют</td>
                                    <td>4,83 км</td>
                                    <td>2,39 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>•	Электричество–22 тг/кВт;</li>
                                <li>•	Газ–34,75 тг/м3</li>
                                <li>•	Вода–252,8 тг/м3</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о районе</strong>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – с. Казыгурт</li>
                            <li>•	численность населения – 106,9 тыс. человек</li>
                            <li>•	территория – 4,1 тыс. кв. км</li>
                            <li>•	расстояние до областного центра – 74 км</li>
                            <li>•	количество сельских округов – 13</li>
                            <li>•	населенных пунктов – 60</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <strong>Животноводство:</strong>
                        <ul>
                            <li>•	овцеводство (племенное тонкорунное);</li>
                            <li>•	КРС – мясное;</li>
                            <li>•	коневодство (племенное).</li>
                        </ul>
                        <strong>Растениеводство:</strong>
                        <ul>
                            <li>•	зерновые (пшеница, ячмень);</li>
                            <li>•	кукуруза;</li>
                            <li>•	масличные культуры (сафлор, подсолнечник).</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Земли сельхозназначения, всего – 102,7 тыс. га, в том числе</strong>
                        <ul>
                            <li>•	посевная 73,0 тыс. га.</li>
                            <li>•	богара – 92,1 тыс. га;</li>
                            <li>•	пастбища – 124,87 тыс. га;</li>
                            <li>•	многолетние насаждения – 2,2 тыс. га;</li>
                            <li>•	сенокосы – 28,8 тыс. га</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего население -106,9 тыс. человек в том числе:</strong>
                        <ul>
                            <li>•	экономически активное – 50 тыс. человек;</li>
                            <li>•	безработные -1,8 тыс.человек;</li>
                            <li>•	самозанятое -20,2 тыс.человек;</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	Юридические лица 424, действующие 219 или 52%;</li>
                            <li>•	Крестьянские хозяйства 3646, действующие 2967 или 81%;</li>
                            <li>•	Индивидуальные предприниматели – 4070, действующие 3186 или 78 %</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	Объем валовой продукции сельского хозяйства – 15 119,7  млн. тенге;</li>
                            <li>•	В т. ч. – объем валовой продукции растениеводства – 5 457,6  млн. тенге;</li>
                            <li>•	Объем валовой продукции животноводства – 9 647,7  млн. тенге.</li>
                            <li>•	Объем производства промышленной продукции – 5 048 236 млн. тенге.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>