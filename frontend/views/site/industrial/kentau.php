<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальные зоны "Кентау"';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальные зоны "Кентау"</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус - действующая</p>
                        <p>Общая площадь территории индустриальной зоны  – 25 га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО «Электропривод»</li>
                                <li>•	ТОО «Электродеталь»</li>
                                <li>•	ИП  «Ауезов»</li>
                                <li>•	ТОО «Бескаска» </li>
                                <li>•	ТОО "КенМед"</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 150</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Кентау</td>
                                    <td>110/6 кВ ТМ2х6300 кВа </td>
                                    <td>4620 кВт/час</td>
                                    <td>отсутствуют</td>
                                    <td>17 м3/час</td>
                                    <td>1,24 км</td>
                                    <td>отсутствуют</td>
                                    <td>2,5 км</td>
                                    <td>1,9 км</td>
                                    <td>3,58 км</td>
                                    <td>2,63 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>•	Электричество–14, 29/кВт;</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о городе</strong>
                        <p>Кентау – город областного подчинения в Южно-Казахстанской области. Промышленность представлена машиностроением и горно-обогатительным производством. Развивается также животноводство и сельское хозяйство.</p>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – город Кентау</li>
                            <li>•	численность населения – 89,8 тыс. чел.</li>
                            <li>•	площадь  района – 600 кв. км.</li>
                            <li>•	расстояние до областного центра – 260 км</li>
                            <li>•	количество сельских округов – 4</li>
                            <li>•	населенных пунктов – 3</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <ul>
                            <li>•	Машиностроение (экскаваторный, трансформаторный заводы)</li>
                            <li>•	Сельское хозяйство:</li>
                            <li>•	Животноводство (КРС, МРС);</li>
                            <li>•	Растениеводство: зерновые, овощи, фрукты, бахча, кормовые.</li>
                            <li>•	Добыча полезных ископаемых</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Общая площадь земель с/х назначения – 49,0 тыс.га:</strong>
                        <ul>
                            <li>•	в т.ч. посевная 38,1 тыс. га;</li>
                            <li>•	орошаемые земли -42,9 тыс. га;</li>
                            <li>•	богара -6,1 тыс. га;</li>
                            <li>•	пастбище – 320,7 тыс. га</li>
                            <li>•	многолетние насаждения -4,3 тыс. га;</li>
                            <li>•	сенокосы -9,7 тыс. га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Сельскохозяйственные угодья – всего 46 401 га.</strong>
                        <strong>Площадь земель с/х назначения – 17 557 га. </strong>
                        <strong>в том числе:</strong>
                        <ul>
                            <li>•	Пашни – 2 779 га;</li>
                            <li>•	поливные – 2 779 га,</li>
                            <li>•	залежи – 3 992 га,</li>
                            <li>•	пастбища – 8 695 га</li>
                        </ul>
                        <strong>Площадь неиспользуемых свободных земель – 23208 га, из них:</strong>
                        <ul>
                            <li>•	поливные – 400 га,</li>
                            <li>•	залежи – 1400 га и</li>
                            <li>•	пастбища – 21 408 га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <p>Всего население 89,8 тыс. человек (3% ЮКО) в т. ч. : </p>
                        <ul>
                            <li>•	экономически активное – 44,2 тыс. человек (49% города);</li>
                            <li>•	безработные – 4,4 тыс. человек  (10% экон. актив.);</li>
                            <li>•	самозанятое – 31,4 тыс. человек (35% города).</li>
                            <li>•	Обеспеченность  квалифицированными специалистами в связи с наличием в городе: технического лицея, многопрофильного горно-технического колледжа, колледжа при трансформаторном заводе, который готовит специалистов в области энергетики и автоматики, филиала Казахско-турецкого университета, частных колледжей.</li>
                            <li>•	Среднемесячная заработная плата – 72 756 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	юридические лица – 493,  действующие – 171  или 35%;</li>
                            <li>•	крестьянские хозяйства – 868,  действующие – 533 или 61%;</li>
                            <li>•	субъекты МСБ – 1361,  действующие – 704  или 52%</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	Объем валовой продукции сельского хозяйства – 1 046,9  млн. тенге;</li>
                            <li>•	В т. ч. – объем валовой продукции растениеводства – 412,9  млн. тенге;</li>
                            <li>•	Объем валовой продукции животноводства – 634,0  млн. тенге.</li>
                            <li>•	Объем производства промышленной продукции – 20 649,8 млн. тенге.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>