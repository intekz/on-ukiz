<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальная зона «МАКТААРАЛ»';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальная зона «МАКТААРАЛ»</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус – действующая</p>
                        <p>Общая площадь территории индустриальной зоны “ Мактарал ” – 28га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО «Береке ММА»</li>
                                <li>•	ТОО "Soldatos"</li>
                                <li>•	ИП "Жумабаев"</li>
                                <li>•	ТОО "Корпорация IMZA"</li>
                                <li>•	КХ "Айбек"</li>
                                <li>•	ИП «Раджапов У.»</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 212</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Махтаарал</td>
                                    <td>35/10 кВ ТМ1х6300 кВа </td>
                                    <td>4950 кВт/час</td>
                                    <td>2000 м3/час</td>
                                    <td>50 м3/час</td>
                                    <td>6,5 км</td>
                                    <td>1 км</td>
                                    <td>2,6 км</td>
                                    <td>отсутствуют</td>
                                    <td>отсутствуют</td>
                                    <td>1,85 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>•	Электричество–22 тг/кВт;</li>
                                <li>•	Газ–34,75 тг/м3</li>
                                <li>•	Вода–100 тг/м3</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о районе</strong>
                        <p>Мактааральский район – является крупным производителем и поставщиком хлопка, овощей, винограда, бахчевых культур. В структуре отрасли сельского хозяйства, где важнейшими видами продукции являются хлопок, зерно, овощи, бахчевые культуры, а также, развито животноводство.
                            В перспективе экономика района будет основываться на кластерном развитии, при котором максимально задействуют потенциал в следующих направлениях:
                        </p>
                        <ul>
                            <li> – Сельское хозяйство;</li>
                            <li> – Пищевая промышленность;</li>
                            <li> – Логистика;</li>
                            <li> – Легкая промышленность.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – город Жетысай</li>
                            <li>•	численность населения – 300,3 тыс. чел.</li>
                            <li>•	площадь  района – 1,9 тыс. га</li>
                            <li>•	расстояние до областного центра – 232 км</li>
                            <li>•	количество сельских округов – 24</li>
                            <li>•	населенных пунктов – 182</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <strong>Развитие растениеводства: </strong>
                        <ul>
                            <li>•	Хлопок-волокно – основная экспортнообразующая отрасль экономики региона;</li>
                            <li>•	Бахчевые культуры;</li>
                            <li>•	Зернобобовые, кукуруза, виноград, фрукты.</li>
                        </ul>
                        <strong>Развитие животноводства: </strong>
                        <ul>
                            <li>•	МРС;</li>
                            <li>•	КРС;</li>
                            <li>•	коневодство;</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>сельхозугодья: всего 159,6  тыс. га, в т. ч:</strong>
                        <ul>
                            <li>•	посевная 131,3 тыс. га</li>
                            <li>•	пастбища – 5,4 тыс. га</li>
                            <li>•	орошаемые земли – 131,3 тыс. га</li>
                            <li>•	многолетние насаждения – 1,4 тыс. га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего 297 тыс. человек в т. ч.: </strong>
                        <ul>
                            <li>•	экономически активное население  128,6 тыс. человек;</li>
                            <li>•	безработные  4,2 тыс. человек   или 3,3%;</li>
                            <li>•	самозанятое  – 85 тыс. человек.</li>
                            <li>•	среднемесячная заработная плата – 63 259 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	юридические лица – 1129, действующие – 289  или 25%;</li>
                            <li>•	крестьянские  хозяйства -19062, действующие – 16227 или 85%;</li>
                            <li>•	субъекты МСБ – 19062, действующие – 16516 или 87%.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	Объем валовой продукции сельского хозяйства – 49 072,5  млн. тенге;</li>
                            <li>•	В т. ч. – объем валовой продукции растениеводства – 37 755,3  млн. тенге;</li>
                            <li>•	Объем валовой продукции животноводства – 11 007,6  млн. тенге.</li>
                            <li>•	Объем производства промышленной продукции – 9 448,1  млн. тенге.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>