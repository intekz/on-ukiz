<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальная зона «ШАРДАРА»';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальная зона «ШАРДАРА»</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус – действующая</p>
                        <p>Общая площадь территории индустриальной зоны “ ШАРДАРА ” – 35га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО «Seikhun» </li>
                                <li>•	ТОО «АқЖол-2008 ЛТД»</li>
                                <li>•	ИП "Арынов Шалкар"</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 212</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Шардара</td>
                                    <td>35/6 кВ ТМ2х4000 кВа </td>
                                    <td>4000 кВт/час</td>
                                    <td>отсутствуют</td>
                                    <td>18 м3/час</td>
                                    <td>1 км</td>
                                    <td>отсутствуют</td>
                                    <td>2,95 км</td>
                                    <td>отсутствуют</td>
                                    <td>отсутствуют</td>
                                    <td>1,34 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>•	Электричество–22 тг/кВт</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о районе</strong>
                        <p>Шардаринский район – основу экономику района составляет сельское хозяйство. Выращивается хлопок, бахчевые культуры, овощи и фрукты. В районе также развито животноводство и рыбное хозяйство. На территории района построено Шардаринское водохранилище площадью в 40 тыс. га и объемом воды – 5 200 млн. м3.</p>
                        <p>В перспективе экономика района будет основываться на кластерном развитии, при котором максимально задействуют потенциал в следующих направлениях:</p>
                        <ul>
                            <li> – Сельское хозяйство;</li>
                            <li> – Пищевая промышленность;</li>
                            <li> – Рыбное хозяйство;</li>
                            <li> – Туристический кластер.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – город Шардара</li>
                            <li>•	численность населения – 78,5 тыс. чел.</li>
                            <li>•	площадь – 13 тыс. кв. км</li>
                            <li>•	расстояние до областного центра – 241 км</li>
                            <li>•	количество сельских округов – 10</li>
                            <li>•	населенных пунктов – 26</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <ul>
                            <li>•	Растениеводство: хлопок, бахчевые, рис, зернобобовые: кукуруза, масляничные: подсолнечник.</li>
                            <li>•	Животноводство:  КРС, МРС.</li>
                            <li>•	Рыбоводство </li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Сельскохозяйственные угодья всего  260 724 тыс. га, в том числе:</strong>
                        <ul>
                            <li>•	посевная площадь – 63,5  тыс. га, в том  числе поливная 58  тыс. га;</li>
                            <li>•	неосвоенные  земли  – 5,5 тыс. га;</li>
                            <li>•	пастбища – 177 800  тыс. га;</li>
                            <li>•	овощи, бахча,  картофель  – 12,5 тыс. га;</li>
                            <li>•	многолетние насаждения – 0,5тыс. га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего 297 тыс. человек в т. ч.: </strong>
                        <ul>
                            <li>•	экономически активное – 128,6 тыс. человек;</li>
                            <li>•	безработные  – 4,2 тыс. человек   или 3,3%;</li>
                            <li>•	самозанятое  –  85 тыс. человек.</li>
                            <li>•	среднемесячная заработная плата – 77 284 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	юридические лица- 1156, действующие – 289  или 25%;</li>
                            <li>•	крестьянские  хозяйства -18 933, действующие-15 692 или 83%;</li>
                            <li>•	индивидуальные предприниматели 18 933, действующие 16 516 – 87%.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>