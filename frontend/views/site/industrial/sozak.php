<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальные зоны "Созак"';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальные зоны "Созак"</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус - действующая</p>
                        <p>Общая площадь территории индустриальной зоны “Созак” – 50 га</p>
                        <p>Рабочие месиа, чел. - 3447</p>
                        <div>Список проектов:
                            <ul>
                                <li>• ТОО «UKO-SStar»</li>
                                <li>• ТОО «Благо Лизинг НПЗ» </li>
                                <li>• ПК «Дархан-Ас» </li>
                                <li>• ТОО «Сырлы Созақ»</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 279</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Созак</td>
                                    <td>от ПС 110/35/10 кВ Чулаккорган</td>
                                    <td>840 кВт/час</td>
                                    <td>отсутствуют</td>
                                    <td>10 м3/час</td>
                                    <td>10,26 км</td>
                                    <td>отсутствуют</td>
                                    <td>1 км</td>
                                    <td>1 км</td>
                                    <td>5,68 км</td>
                                    <td>5,68 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            Тарифы на 27.07.2017г.
                            <ul>
                                <li>• Электричество:</li>
                                <li>
                                    <ul>
                                        <li>7:00–19:00–22,13 тг/кВт</li>
                                        <li>19:00–23:00–53,75 тг/кВт</li>
                                        <li>23:00–7:00–7,24 тг/кВт</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о районе</strong>
                        <p>Сузакский район – является промышленным районом, в сфере сельского хозяйства небольшими темпами развивается животноводство, на территории района имеются пастбищные земли. В районе занимаются разведением и откормом местных пород животноводства – верблюдов, МРС и лошадей.</p>
                        <p>Основные направления района и дальнейшие перспективы:</p>
                        <ul>
                            <li>– Сельское хозяйство;</li>
                            <li>– Пищевая промышленность;</li>
                            <li>– Промышленность;</li>
                            <li>– Производство строительных материалов.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – с. Шолаккорган</li>
                            <li>•	численность населения – 56,9 тыс. человек</li>
                            <li>•	территория – 4 105 тыс. га</li>
                            <li>•	расстояние до областного центра – 189 км</li>
                            <li>•	количество сельских округов – 12</li>
                            <li>•	населенных пунктов – 38</li>
                            <li>•	район занимает  35 % территории ЮКО</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <strong>Добыча полезных ископаемых:</strong>
                        <ul>
                            <li>•	уран, фосфориты, тальк, хризотил асбест,  глины бентонитовые, суглинки, пески кварцевые, полевые шпаты.</li>
                        </ul>
                        <strong>Животноводство:</strong>
                        <ul>
                            <li>•	верблюдоводство, овцеводство, коневодство.</li>
                        </ul>
                        <strong>Растениеводство:</strong>
                        <ul>
                            <li>•	овощи, картофель, бахчевые, многолетние насаждения.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Земли, всего 2 260,9 га, в том числе:</strong>
                        <ul>
                            <li>•	орошаемые – 4,6 тыс. га</li>
                            <li>•	пастбища – 2 604,2 тыс. га</li>
                            <li>•	многолетние насаждения – 331 га</li>
                            <li>•	сенокосы – 11,8 тыс. га</li>
                        </ul>
                        <strong><i>Неиспользуемые свободные земли 10,396 тыс. га</i></strong>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего 56, 9 тыс. человек, в том числе:</strong>
                        <ul>
                            <li>•	экономически активное  – 36,5 тыс. человек, в т. ч.:</li>
                            <li>•	безработные  – 1,7 тыс. человек (5%);</li>
                            <li>•	самозанятое  – 11,3 тыс. человек</li>
                            <li>•	среднемесячная заработная плата – 164 443 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес:</strong>
                        <ul>
                            <li>•	Зарегистрированные юридические лица-187, действующие-65</li>
                            <li>•	Зарегистрированные фермерские хозяйства – 826, действующие- 536</li>
                            <li>•	Зарегистрированные субъекты МСБ- 1013, действующие-601</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <p>Объем валовой продукции сельского хозяйства – 6 329,0  млн. тенге;</p>
                        <p>В т. ч. – объем валовой продукции растениеводства –601,2  млн. тенге;</p>
                        <p>           – объем валовой продукции животноводства – 5 727,1 млн. тенге</p>
                        <p>Объем производства промышленной продукции – 177 146,7 млн. тенге.</p>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>