<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальные зоны "Оңтүстік"';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальные зоны "Тассай"</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус - действующая</p>
                        <p>Общая площадь территории индустриальной зоны “ ТАССАЙ ” – 89 га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО "Мега Смарт"</li>
                                <li>•	ТОО «Казтехникас» </li>
                                <li>•	ТОО «Marai E7 Group»</li>
                                <li>•	ТОО «Шымкай» </li>
                                <li>•	ТОО "Нур Алем"</li>
                                <li>•	ТОО "M GROUP KZ"</li>
                                <li>•	ТОО "ДезФумЭкс"</li>
                                <li>•	ТОО «Фармасинтез-Зерде» </li>
                                <li>•	АО «Asiatrafo»</li>
                                <li>•	ИП "Петухова И.В."</li>
                                <li>•	ТОО "Платан"</li>
                                <li>•	ТОО "Шым Техникс Групп"</li>
                                <li>•	ТОО "ОПЧС-Строй"</li>
                                <li>•	ТОО "New Technology Ltd"</li>
                                <li>•	ТОО «Ди oil gas»</li>
                                <li>•	ТОО "ЭкоФарм Интернейшнл"</li>
                                <li>•	ТОО «НПО Зерде»</li>
                                <li>•	ТОО "Болашак 7"</li>
                                <li>•	ТОО "АлиКа Групп"</li>
                                <li>•	ТОО "АлиКа Групп"</li>
                                <li>•	ТОО "Жеңіс Құрылыс"</li>
                                <li>•	ТОО "PROF-AKS"</li>
                                <li>•	ТОО «Astana LRS пласт» </li>
                                <li>•	ТОО «MAXIMUS TRADE</li>
                                <li>•	ТОО «Вали пласт» </li>
                                <li>•	ИП "Джуманова"</li>
                                <li>•	ТОО «LED Kazakhstan»</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 1704</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Тассай</td>
                                    <td>35/10кВ ТМ2х16000 кВа</td>
                                    <td>10 МВт/час</td>
                                    <td>500 м3/час</td>
                                    <td>73 м3/час</td>
                                    <td>5,8 км</td>
                                    <td>2,3 км</td>
                                    <td>3,3 км</td>
                                    <td>2,7 км</td>
                                    <td>3,9 км</td>
                                    <td>2,7 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>• Электричество–22 тг/кВт;</li>
                                <li>• Газ–34,75 тг/м3</li>
                                <li>• Вода–267 тг/м3</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о городе</strong>
                        <p><i>Шымкент</i>— областной центр Южно-Казахстанской области, входит в тройку крупнейших городов Казахстана и является одним из крупнейших промышленных и торговых центров страны. Расстояние до Астаны – 1458 км. Шымкент  один из ведущих промышленных и экономических центров Казахстана.</p>
                    </div>
                    <div>
                        <strong>Текстильная промышленность.</strong>
                        <ul>
                            <li>– Наличие сырьевой базы: – ЮКО – единственный производитель хлопка-сырца в РК. ( Рентабельность выращивания хлопка-сырца во многих хозяйствах области достигает 25–30% против 15–20% по другим культурам). Годовое производствохлопка-сырца в ЮКО составляет  396,7 тыс. тонн, посевная площадь хлопчатника составляет  140,6 га, урожайность хлопка составила 28,7 ц / га.</li>
                            <li>– Импорт в ЮКО текстильной продукции составил 4 116,0 долларов США.</li>
                            <li>– Кластер легкой промышленности планируется реализовать на территории СЭЗ «Оңтүстік»в г. Шымкенте.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Фармацевтическая отрасль.</strong>
                        <ul>
                            <li>– Высокая внутренняя потребность в лекарственных препаратах, подтверждаемая импортом и уровнем расходов на эти цели.Объем импорта фармацевтических средств  в Республике Казахстан занимает 98%, против 2% экспорта.</li>
                            <li>– Крайняя импортозависимость фармпроизводства (импорт удовлетворяет 85% внутреннего потребления лекарственными средствами, а фармацевтическая промышленность Казахстана удовлетворяет только 15% внутреннего потребления), ключевой задачей в создании регионального кластера остается наращивание производственных мощностей предприятий фармацевтической промышленности Казахстана по созданию экспортоориентированных производств.</li>
                            <li>– Импорт медицинского оборудования, инвентаря, метпрепаратов составил 30,4 млн. дол  США. Объем государственных закупок на мед. оборудование, технику в ЮКО за 2013 год составил 127,4 млн. тенге.</li>
                            <li>– Присутствие  профессионального научно-производственного и кадрового потенциала,  сосредоточеного на базе Южно-Казахстанской государственной фармацевтической Академии.</li>
                            <li>– Сосредоточение в ЮКО основных фармпредприятий республики, выпускающих 23% общего объема фармпродукции.</li>
                            <li>– Кластер легкой промышленности планируется реализовать на территории СЭЗ «Оңтүстік»в г. Шымкенте.</li>
                            <li>– В Казахстане в официальной и народной медицине при¬меняются более 250 видов лекарственных растений, однако, еще большее количество можно рассматривать в качестве заменителей к об¬щепризнанным фармакопейным растениям. Практика использования лекарственных растений в последние годы расширяется в связи с их дешевизной, комплексным лечебным действием на организм, относительно низкой  токсичностью и возможностью длительного применения без побочных эффектов. Особая ценность лекарственных растений заключается в том, что они являются легко возобновляе¬мыми источниками сырья.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>