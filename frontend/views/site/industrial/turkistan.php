<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальные зоны "Туркестан"';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальные зоны "Туркестан"</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус - действующая</p>
                        <p>Общая площадь территории индустриальной зоны “Оңтүстік” – 40,63 га (в резерве 103га)</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО  «Туркестан Трасса»</li>
                                <li>•	ТОО «Азрет-ЖолҚұрылыс»</li>
                                <li>•	ТОО «Kadiris»</li>
                                <li>•	ИП "Grand Mix"</li>
                                <li>•	ТОО "Стройсервис-ХХІ"</li>
                                <li>•	ИП "Grand Mix"</li>
                                <li>•	ТОО «Мирас ДАМУ лтд»</li>
                                <li>•	ИП «Ебергенов Б.»</li>
                                <li>•	ТОО «Жардем-НГ»</li>
                                <li>•	ТОО «Жилстрой Индустрия»</li>
                                <li>•	ТОО "Айбек ЛТД"</li>
                                <li>•	ТОО "MOBELLA"</li>
                                <li>•	ТОО «Cordial»</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 559</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Туркестан</td>
                                    <td>от ПС 110/10 Коммунальная</td>
                                    <td>1800 кВт/час</td>
                                    <td>3000 м3/час</td>
                                    <td>80 м3/час</td>
                                    <td>80 м3/час</td>
                                    <td>1,1 км</td>
                                    <td>1,87 км</td>
                                    <td>3,7 км</td>
                                    <td>7,9 км</td>
                                    <td>4,21 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>
                                    • Электричество–22 тг/кВт;
                                    <ul>
                                        <li>С 7:00 до 19:00 за 1кВт – 22,13тг</li>
                                        <li>С 19:00 до 23:00 за 1кВт – 53,75тг</li>
                                        <li>С 23:00 до 7:00 за 1кВт – 7,24тг</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о городе</strong>
                        <p>В Туркестане действуют предприятия машиностроения, легкой, пищевой промышленности, производства строительных материалов, биохимической промышленности. К крупным предприятиям Туркестана относят также предприятия текстильной и швейной промышленности – ОАО «Яссы», КХ «Туран», ТОО «Корпорация Ак-Алтын», ТОО «ШТФ – Туркестан». В районе развито животноводство, растениеводство. Особое место в сельском хозяйстве региона занимают выращивание зерновых культур и высоких сортов хлопка-сырца.
                            В перспективе экономика района будет основываться на кластерном развитии, при котором максимально задействуют потенциал в следующих направлениях:
                        </p>
                        <ul>
                            <li>– Сельское хозяйство;</li>
                            <li>– Пищевая промышленность</li>
                            <li>– Легкая промышленность;</li>
                            <li>– Производство строительных материалов;</li>
                            <li>– Туристический кластер</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – г. Туркестан</li>
                            <li>•	численность населения – 243,9 тыс. человек</li>
                            <li>•	территория – 7,4  тыс. кв. км</li>
                            <li>•	расстояние до областного центра – 160 км</li>
                            <li>•	количество сельских округов – 12</li>
                            <li>•	населенных пунктов – 35</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Традиционные направления</strong>
                        <ul>
                            <li>•	Паломнический туризм</li>
                            <li>•	Животноводство: МРС, КРС, свиньи, птицы</li>
                            <li>•	Растениеводство: пшеница, ячмень, сафлор, помидоры, огурцы, лук, картофель, капуста, бахча, хлопок, подсолнух, кукуруза, яблоки, виноград.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Общая площадь земель с/х назначения – 49,0 тыс.га:</strong>
                        <ul>
                            <li>•	в т.ч. посевная 38,1 тыс. га;</li>
                            <li>•	орошаемые земли -42,9 тыс. га;</li>
                            <li>•	богара -6,1 тыс. га;</li>
                            <li>•	пастбище – 320,7 тыс. га</li>
                            <li>•	многолетние насаждения -4,3 тыс. га;</li>
                            <li>•	сенокосы -9,7 тыс. га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего населения – 245,1 тыс. чел.: </strong>
                        <ul>
                            <li>•	экономически активное – 85,5 тыс. человек,   в том числе:</li>
                            <li>•	безработные – 4,7 тыс. человек или 5,5%;</li>
                            <li>•	самозанятое – 54,3 тыс.человек или 63,5%.</li>
                            <li>•	среднемесячная заработная плата – 82 322 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	юридические лица – 519, действующие – 177 или 34%;</li>
                            <li>•	крестьянские хозяйства – 4847, действующие – 3496 или 72%;</li>
                            <li>•	индивидуальные предприниматели – 5366, действующие-3673 или 68%.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	 объем валовой продукции сельского хозяйства – 21 097,9  млн. тенге;</li>
                            <li>•	 в т. ч. – объем валовой продукции растениеводства – 12 367,7  млн. тенге;</li>
                            <li>•	 объем валовой продукции животноводства – 8 723,0  млн. тенге.</li>
                            <li>•	 объем производства промышленной продукции – 9 852,3 млн. тенге.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>