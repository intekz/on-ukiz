<?php

/* @var $this yii\web\View */

$this->title = 'Индустриальная зона «Тюлькубас»';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">Индустриальная зона «Тюлькубас»</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul class="tabs">
                <li class="industrial__item active" rel="tab1"><div class="industrial__link" href="#">Общая информация</div></li>
                <li class="industrial__item" rel="tab2"><div class="industrial__link" href="#">Список проектов</div></li>
                <li class="industrial__item" rel="tab3"><div class="industrial__link" href="#">Информация о районе</div></li>
            </ul>
        </div>
        <div class="tab_container">
            <section id="tab1" class="industrial__block tab_content">
                <div class="industrial__block_img">
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                    <div class="industrial__img">
                        <a href="/images/project-item.png" class="industrial__img_min" style="background-image: url('/images/project-item.png')" data-fancybox="zone"></a>
                    </div>
                </div>
                <div class="industrial__block_typography">
                    <div>
                        <p>Статус – действующая</p>
                        <p>Общая площадь территории индустриальной зоны “Тюлькубас ” – 55 га</p>
                        <div>Список проектов:
                            <ul>
                                <li>•	ТОО «Еңбекші 2016»</li>
                                <li>•	ТОО «Кен-Тау» </li>
                                <li>•	ТОО "Әмір-Мәнсүр"</li>
                                <li>•	ТОО "NutriCorn"</li>
                                <li>•	ТОО "KazMegaStroi"</li>
                                <li>•	ТОО "Жамбул бакшасы"</li>
                                <li>•	ПК "ДОРСЕРВИС"</li>
                            </ul>
                        </div>
                        <div>Рабочие места, чел. – 1474</div>
                        <div>
                            <div>Инфраструктура</div>
                            <table>
                                <tr>
                                    <td>ИЗ</td>
                                    <td>ПС</td>
                                    <td>Мощность электросн-я</td>
                                    <td>Мощность газосн-я</td>
                                    <td>Мощность водосн-я</td>
                                    <td>Сети электросн-я</td>
                                    <td>Сети газосн-я</td>
                                    <td>Сети водосн-я</td>
                                    <td>Сети канали-й</td>
                                    <td>Сети телеф-й</td>
                                    <td>Авто-дорога</td>
                                </tr>
                                <tr>
                                    <td>Тюлькубас</td>
                                    <td>35/6 кВ ТМ2х2500 кВа </td>
                                    <td>2500 кВт/час</td>
                                    <td>1300 м3/час</td>
                                    <td>139 м3/час</td>
                                    <td>3,93 км</td>
                                    <td>3,7 км</td>
                                    <td>3,61 км</td>
                                    <td>отсутствуют</td>
                                    <td>отсутствуют</td>
                                    <td>1 км</td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <p>Тарифы на 27.07.2017г.</p>
                            <ul>
                                <li>•	Электричество–22 тг/кВт;</li>
                                <li>•	Газ–34,75 тг/м3.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tab2" class="industrial__block tab_content">
                На данный момент Список проектов отсутствуют
            </section>
            <section id="tab3" class="industrial__block tab_content">
                <div class="industrial__block_typography">
                    <div>
                        <strong>Информация о городе</strong>
                        <p>Тюлькубасский район отличается большим разнообразием видов продукции сельского хозяйства. Сельскохозяйственные товаропроизводители, в основном, занимаются выращиванием зерновых и зернобобовых, технических и кормовых культур. На территории района расположен Аксу-Джабаглинский заповедник, включенный в число важнейших природных ресурсов Казахстана.
                            В перспективе экономика района будет основываться на кластерном развитии, при котором максимально задействуют потенциал в следующих направлениях:
                        </p>
                        <ul>
                            <li> – Сельское хозяйство;</li>
                            <li> – Пищевая промышленность;</li>
                            <li> – Логистика;</li>
                            <li> – Туристический кластер;</li>
                            <li> – Производство строительных материалов.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Общая информация о регионе</strong>
                        <ul>
                            <li>•	административный центр – пос.Турар Рыскулова</li>
                            <li>•	территория –2,3 тыс.кв. км</li>
                            <li>•	расстояние до областного центра – 98 км</li>
                            <li>•	количество сельских округов: 15</li>
                            <li>•	населенных пунктов – 63</li>
                            <li>•	численность населения – 107,2 тыс. человек</li>
                            <li>•	плотность населения – 47 чел./кв. км</li>
                            <li>•	средняя зарплата – 67 277 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Земельные ресурсы:</strong>
                        <strong>Земли с/х назначения  64,1 тыс. га, в т. ч.:</strong>
                        <ul>
                            <li>•	посевная 61,8 тыс. га;</li>
                            <li>•	орошаемые 11,2 тыс. га;</li>
                            <li>•	богара 52,9 тыс. га;</li>
                            <li>•	пастбище 56,7 тыс. га;</li>
                            <li>•	многолетние насаждения 3,4 тыс. га;</li>
                            <li>•	сенокосы 7,4 га.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Трудовые ресурсы:</strong>
                        <strong>Всего население  – 107,2 тыс. человек,  в том числе:</strong>
                        <ul>
                            <li>•	экономически активное – 55,3 тыс. человек  в  том числе:</li>
                            <li>•	безработные  2,4 тыс. человек (4%);</li>
                            <li>•	самозанятое  34,1 тыс. человек (62%).</li>
                            <li>•	средняя заработная плата – 71 072 тенге</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Малый и средний бизнес</strong>
                        <ul>
                            <li>•	Юридические лица – 395, действующие – 196 или 50%;</li>
                            <li>•	Крестьянские хозяйства – 2350: действующие – 2350 или 100%;</li>
                            <li>•	Индивидуальные предприниматели – 2745: действующие 2546 или 92 %.</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Основные показатели развития сельского хозяйства района:</strong>
                        <ul>
                            <li>•	Объем валовой продукции сельского хозяйства – 13 601,8  млн. тенге;</li>
                            <li>•	Объем валовой продукции растениеводства – 4 012,6  млн. тенге</li>
                            <li>•	Объем валовой продукции животноводства – 9 574,6  млн. тенге.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>