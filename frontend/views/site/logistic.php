<?php

/* @var $this yii\web\View */


$this->title = 'Логистические зоны';
?>
<div class="site-logistic">
    <div class="container">
        <div class="logistic__title">Логистические зоны</div>
        <section class="logistic__block">
            <ul class="tabs">
                <li class="active" rel="tab1">
                    <div class="logistic__img">
                        <div href="#" class="logistic__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
                <li rel="tab1">
                    <div class="logistic__img">
                        <div href="#" class="logistic__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
                <li rel="tab1">
                    <div class="logistic__img">
                        <div href="#" class="logistic__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
                <li rel="tab1">
                    <div class="logistic__img">
                        <div href="#" class="logistic__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
            </ul>
            <div class="tab_container">
                <div id="tab1" class="tab_content logistic__block_typography">
                    <div>
                        <p>Статус - действующая</p>
                        <p>Общая площадь территории индустриальной зоны - 337 га</p>
                        <p>Рабочие месиа, чел. - 3447</p>
                        <p>Количество проектов -85</p>
                    </div>
                    <div>
                        <p>Тарифы</p>
                        <ul>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                        </ul>
                    </div>
                    <div>
                        <p>Индустриальная зона</p>
                        <ul>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                        </ul>
                    </div>
                </div>
                <div id="tab2" class="tab_content">

                </div>
                <div id="tab3" class="tab_content">

                </div>
                <div id="tab4" class="tab_content">

                </div>
            </div>
        </section>
    </div>
    </div>
</div>
