<?php

/* @var $this yii\web\View */

use frontend\widgets\FeedbackWidget;
use frontend\widgets\NewsWidget;
use frontend\widgets\ParentsWidget;
use frontend\widgets\SubsidiaryWidget;

$this->title = 'Стать инвестором';
?>
<div class="site-participants">
    <div class="container">
        <div class="participants__title">Стать инвестором</div>
        <div class="participants__main_list">
            <ul class="tabs1">
                <li class="participants__main_item active1" rel="tab11"><div class="participants__main_link">Помощь бизнесу</div></li>
                <li class="participants__main_item" rel="tab22"><div class="participants__main_link">Нормативно-правовая база</div></li>
                <li class="participants__main_item" rel="tab33"><div class="participants__main_link">Инфраструктура</div></li>
            </ul>
        </div>
        <div class="tab_container1">
            <div id="tab11" class="tab_content1">
                <div class="participants__list">
                    <ul class="tabs">
                        <li class="participants__item active" rel="tab1"><div class="participants__link">Порядок оформления виз</div></li>
                        <li class="participants__item" rel="tab2"><div class="participants__link">Регистрация предприятия</div></li>
                        <li class="participants__item" rel="tab3"><div class="participants__link">Государственная регистрация</div></li>
                    </ul>
                </div>
                <div class="tab_container">
                    <section id="tab1" class="participants__block tab_content">
                        <div class="participants__scroll">
                            <div class="participants__info">
                                <p>Для оформления визовой поддержки приглашающая организация предоставляет в МИД следующие документы:</p>
                                <ul>
                                    <li>1.Письмо-приглашение установленного образца.</li>
                                    <li>2.Оригиналы платежного поручения или квитанции об уплате консульского сбора и услуг. </li>
                                    <li>
                                        <div>3.Организация, обратившаяся в МИД впервые в текущем году,  нотариально заверенные копии текущим годом: </div>
                                        <ul>
                                            <li>•	устава,</li>
                                            <li>•	свидетельства о регистрации в Министерстве юстиции Республики Казахстан,</li>
                                            <li>•	оригинал справки об отсутствии/наличии задолженности налогоплательщика,</li>
                                            <li>•	лицензии на туристскую деятельность в случае приглашения туристов.</li>
                                        </ul>
                                    </li>
                                    <li>4. Доверенность от организации. </li>
                                    <li>5. Данные на приглашаемое лицо установленного образца в электронном виде. </li>
                                    <li>6. В случае оформления визы категории «инвесторская»,  ходатайство Комитета по инвестициям Министерства индустрии и новых технологий Республики Казахстан. </li>
                                </ul>
                                <p>Письмо-приглашение готовится в 3-х экземплярах: два экземпляра сдаются в МИД, третий – отправляется приглашаемым лицам.</p>
                                <p>Поступившие заявки обрабатываются в течение 3-5  суток и после направления указания о выдаче виз в загранучреждения МИД  сообщает номер визовой поддержки, который приглашающая организация должна передать иностранцу для обращения в консульское учреждение за казахстанской визой.</p>
                                <p>Контакты МИД в г. Астана:<br>
                                    Тел: +7 7172 72 04 70, 72 04 71, факс: 72 04 60;<br>
                                    Адрес: ул. Кабанбай батыра, дом 28, 3 подъезд.
                                </p>
                                <p>
                                    Представительство МИД в г. Алматы:<br>
                                    Тел: +7 (727) 272 09 39, 272 03 11; факс: 72 08 43<br>
                                    Адрес: ул. Айтеке би, дом 65.

                                </p>
                            </div>
                        </div>
                    </section>
                    <section id="tab2" class="participants__block tab_content">
                        <div class="participants__scroll">
                            <div class="participants__info">
                                <p>Государственной регистрации подлежат все юридические лица, создаваемые на территории Республики Казахстан, независимо от целей их создания, рода и характера их деятельности, состава участников (членов).</p>
                                <p>Филиалы и представительства юридических лиц, расположенные на территории Республики Казахстан, подлежат учетной регистрации без приобретения ими права юридического лица.</p>
                                <p>Государственную регистрацию юридических лиц и учетную регистрацию филиалов и представительств осуществляют территориальные органы юстиции (за исключением юридических лиц – участников регионального финансового центра города Алматы Национального Банка Республики Казахстан).</p>
                                <p>Государственную (учетную) регистрацию, перерегистрацию и регистрацию ликвидации банков, общественных и религиозных объединений с республиканским и региональным статусами, в том числе политических партий, филиалов и представительств иностранных и международных некоммерческих неправительственных объединений, производит Комитет регистрационной службы и оказания правовой помощи Министерства юстиции Республики Казахстан.</p>
                                <p>Государственная регистрация, перерегистрация, ликвидация общественных и религиозных объединений с местным статусом, фондов и объединений юридических лиц, учетная регистрация, перерегистрация, снятие с учета филиалов и представительств общественных и религиозных объединений в соответствующей области и городов Астаны и Алматы осуществляется Департаментами юстиции областей, городов Астаны и Алматы.</p>
                                <p>Государственную (учетную) регистрацию, перерегистрацию и регистрацию ликвидации) созданных, реорганизованных и ликвидированных юридических лиц, учетную регистрацию (перерегистрацию и снятия с учетной регистрации) филиалов и представительств в соответствующей области, кроме тех, которые подлежат регистрации в Комитете, осуществляют районные (городские) Управления юстиции Министерства юстиции Республики Казахстан.</p>
                                <strong style="font-family: 'PTSansBold';">Виды государственных услуг в сфере регистрации юридических лиц</strong>
                                <p>В настоящее время все услуги по линии регистрации юридических лиц  оказываются через Центры обслуживания населения:</p>
                                <ul>
                                    <li>•	государственная регистрация (перерегистрация) юридических лиц, учетная регистрация (перерегистрация) их филиалов и представительств;</li>
                                    <li>•	государственная регистрация прекращения деятельности юридического лица, снятие с учетной регистрации филиала и представительства</li>
                                    <li>•	выдача дубликата устава (положения) юридического лица, не относящегося к субъекту частного предпринимательства, а также акционерного общества, их филиалов и представительств;</li>
                                    <li>•	выдача справки из Государственной базы данных «Юридические лица»;</li>
                                    <li>•	государственная регистрация внесенных изменений и дополнений в учредительные документы юридического лица, не относящегося к субъекту частного предпринимательства, а также акционерного общества, положения об их филиалах (представительствах).</li>
                                </ul>
                                <p>Перечень необходимых документов, представляемых в Центры обслуживания населения, установлен Инструкцией по государственной регистрации юридических лиц и учетной регистрации филиалов и представительств, утвержденной приказом Министра юстиции Республики Казахстан от 12 апреля 2007 года № 112.</p>
                                <p><strong style="font-family: 'PTSansBold';">Сроки государственной регистрации (перерегистрации) юридических лиц, учетной регистрации (перерегистрации) филиалов  (представительств) и выдачи документов.</strong></p>
                                <p>Государственная регистрация (перерегистрация) юридических лиц, относящихся к субъектам частного предпринимательства, учетная регистрация (перерегистрация) их филиалов (представительств), а также государственная регистрация (перерегистрация) юридических лиц – участников регионального финансового центра города Алматы, за исключением акционерных обществ, их филиалов (представительств), осуществляющих деятельность на основании устава, не являющегося типовым, должны быть произведены <strong style="font-family: 'PTSansBold';">не позднее одного рабочего дня</strong>, следующего за днем подачи заявления с приложением необходимых документов.</p>
                                <p>Государственная регистрация (перерегистрация), регистрация внесенных изменений и дополнений в учредительные документы юридических лиц, не относящихся к субъектам частного предпринимательства, а также акционерных обществ, осуществляющих деятельность на основании устава, не являющегося типовым, за исключением политических партий, учетная регистрация (перерегистрация) их филиалов (представительств) должны быть произведены <strong style="font-family: 'PTSansBold';">не позднее десяти рабочих дней</strong>, следующих за днем подачи заявления с приложением необходимых документов.</p>
                                <p>Для юридических лиц, относящихся к субъектам частного предпринимательства, их филиалов (представительств), за исключением акционерных обществ, осуществляющих деятельность на основании устава, не являющегося типовым, их филиалов (представительств), выдача справки о государственной регистрации (перерегистрации) юридического лица, справки об учетной регистрации (перерегистрации) филиала (представительства), возврат заявления о государственной регистрации (перерегистрации) юридического лица (в случае осуществления деятельности на основании типового устава) производятся <strong style="font-family: 'PTSansBold';">на следующий рабочий день</strong> после подачи заявления с приложением необходимых документов.</p>
                                <p>Для юридических лиц, не относящихся к субъектам частного предпринимательства, а также акционерных обществ, осуществляющих деятельность на основании устава, не являющегося типовым, за исключением политических партий, их филиалов (представительств), выдача справки о государственной регистрации (перерегистрации) юридического лица, справки об учетной регистрации (перерегистрации) филиала (представительства), а также возврат устава (положения) производятся <strong style="font-family: 'PTSansBold';">не позднее четырнадцати рабочих дней</strong> со дня подачи заявления с приложением необходимых документов.</p>
                                <p>В соответствии со статьей 12 Закона  «О государственной регистрации юридических лиц и учетной регистрации филиалов и представительств» документом, подтверждающим государственную регистрацию (перерегистрацию) юридического лица, учетную регистрацию (перерегистрацию) филиала (представительства),является справка, выдаваемая регистрирующим органом по форме, установленной Правительством Республики Казахстан (Постановление Правительства Республики Казахстан от 15 декабря 2009 года № 2121)</p>
                                <p>Выдача указанной справки реализована на веб-портале <strong style="text-decoration: underline;font-family: 'PTSansBold';">«электронного правительства»</strong>, которая в настоящее  время является общедоступной.</p>
                                <p>Отказ в государственной регистрации юридических лиц и учетной регистрации филиалов (представительств), а также уклонение от такой регистрации, равно как и иные споры между учредителями юридического лица и регистрирующими органами могут быть обжалованы в суде.</p>
                                <p style="font-style: italic;font-family: 'PTSansBold';"><strong>Электронные услуги в сфере регистрации юридических лиц:</strong></p>
                                <p>Для государственной регистрации юридического лица, относящегося к субъекту малого предпринимательства, в регистрирующий орган учредителем (учредителями) подается уведомление о начале осуществления предпринимательской деятельности посредством заполнения в форме электронного документа по форме, установленной Министерством юстиции Республики Казахстан, и заполняется на веб-портале «электронного правительства».</p>
                                <p>Уплата регистрационного сбора осуществляется через платежный шлюз «электронного правительства» или к уведомлению о начале осуществления предпринимательской деятельности прилагается электронная копия квитанции или иного документа, подтверждающая уплату в бюджет регистрационного сбора за государственную регистрацию юридического лица.</p>
                                <p>Уставы (положения) юридических лиц, относящихся к субъектам малого предпринимательства, их филиалов и представительств в процессе государственной регистрации не представляются.</p>
                                <p>При указанной процедуре регистрации исключена возможность отказа в государственной регистрации субъектов малого предпринимательства.</p>
                                <p>Регистрация субъектов среднего и крупного предпринимательства в альтернативном порядке по желанию субъекта осуществляется через веб-портал «электронного правительства».</p>
                                <p>В целях упрощения административных процедур на веб-портале «электронного правительства» реализована возможность выдачи следующих электронных справок:</p>
                                <ul>
                                    <li>•	«О зарегистрированном юридическом лице, филиале или представительстве»;</li>
                                    <li>•	«О наличии филиалов и представительств юридического лица»;</li>
                                    <li>•	«Об участии юридического лица в других юридических лицах»;</li>
                                    <li>•	«Об участии физического лица в других юридических лицах»;</li>
                                    <li>•	«О всех регистрационных действиях юридического лица, филиала или представительства»;</li>
                                    <li>•	«О последних изменениях в учредительные документы юридического лица»;</li>
                                    <li>•	«О зарегистрированном юридическом лице, филиале или представительства на заданную дату»;</li>
                                    <li>•	«О наложенных обременениях (арест) на юридическое лицо». </li>
                                </ul><br>
                                <p><strong style="font-family: 'PTSansBold';">Контакты Министерства юстиции Республики Казахстан:</strong><br>
                                    г. Астана, ул.Орынбор, д.8, Дом Министерств, 13 подъезд<br>
                                    канцелярия тел.: +7 7172 74 07 37, 74 07 97<br>
                                    email: kanc@minjust.kz
                                </p>
                            </div>
                        </div>
                    </section>
                    <section id="tab3" class="participants__block tab_content">
                        <div class="participants__scroll">
                            <div class="participants__info">
                                <p><strong style="font-family: 'PTSansBold';">Меры государственной поддержки, оказываемые участникам индустриальных зон: </strong></p>
                                <ul>
                                    <li>•	Рассмотрение в приоритетном порядке заявок предпринимателей реализуемых проектов на территории ИЗ для участия в программе «Дорожная карта бизнеса 2020» (субсидирование процентной ставки и частичное гарантирование залогового имущества по кредитам банков); </li>
                                    <li>•	Предоставление земельных участков без проведения аукциона субъектом индустриально-инновационной деятельности для реализации индустриально-инновационных проектов; </li>
                                    <li>•	Предоставление земельного участка на безвозмездной основе в частную собственность инвесторов, реализовавших инвестиционные проекты по Карте Индустриализации (ГП «ФИИР») </li>
                                    <li>•	Обеспечение участников ИЗ инженерно-коммуникационной инфраструктурой; </li>
                                    <li>•	Обеспечение подготовки и переподготовки квалифицированных кадров субъектов участников ИЗ; </li>
                                    <li>•	Рассмотрение в приоритетном порядке проектов, реализуемых на территории ИЗ при предоставлении льготного кредита и финансового лизинга за счет средств региональных институтов развития, программы «Даму-регионы», а также местного бюджета.</li>
                                </ul>
                                <br>
                                <ul>
                                    <li><a href="/documents/Дорожная карта бизнеса 2020.pdf" download><span class="icon-file-pdf-o"> </span> Дорожная карта бизнеса 2020</a></li>
                                    <li><a href="/documents/ГП «ФИИР».pdf" download><span class="icon-file-pdf-o"> </span> ГП «ФИИР»</a></li>
                                    <li><a href="/documents/Перечень приоритетных видов деятельности для реализации инвестиционных проектов.pdf" download><span class="icon-file-pdf-o"> </span> Перечень приоритетных видов деятельности для реализации инвестиционных проектов</a></li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div id="tab22" class="tab_content1">
                <div class="participants__normative">
                    <ul>
                        <li><a href="/participant/Государственная программа индустриально-инновационного развития Республики Казахстан на 2015-2019 годы.pdf" download >1.	Государственная программа индустриально-инновационного развития Республики Казахстан на 2015-2019 годы</a></li>
                        <li><a href="/participant/Закон Республики Казахстан Об инвестициях.pdf" download >2.	Закон Республики Казахстан Об инвестициях</a></li>
                        <li><a href="/participant/Перечень приоритетных видов деятельности для реализации инвестиционных проектов.pdf" download >3.	Перечень приоритетных видов деятельности для реализации инвестиционных проектов</a></li>
                        <li><a href="/participant/Правила предоставления инвестиционной субсидии.pdf" download >4.	Правила предоставления инвестиционной субсидии</a></li>
                        <li><a href="/participant/Закон Республики Казахстан О государственной поддержке индустриально-инновационной деятельности.pdf" download >5.	Закон Республики Казахстан О государственной поддержке индустриально-инновационной деятельности</a></li>
                        <li><a href="/participant/Закон Республики Казахстан О специальных экономических зонах в РК.pdf" download >6.	Закон Республики Казахстан О специальных экономических зонах в РК</a></li>
                        <li><a href="/participant/Закон Республики Казахстан О валютном регулировании и валютном контроле.pdf" download >7.	Закон Республики Казахстан О валютном регулировании и валютном контроле</a></li>
                        <li><a href="/participant/Закон Республики Казахстан Об Акционерных Обществах.pdf" download >8.	Закон Республики Казахстан Об Акционерных Обществах</a></li>
                        <li><a href="/participant/Правила приема, регистрации и рассмотрения заявки на предоставление инвестиционных преференций.pdf" download >9.	Правила приема, регистрации и рассмотрения заявки на предоставление инвестиционных преференций</a></li>
                        <li><a href="/participant/Правила и условия выдачи разрешений иностранному работнику на трудоустройство и работодателям на привлечение иностранной рабочей силы.pdf" download >10.	Правила и условия выдачи разрешений иностранному работнику на трудоустройство и работодателям на привлечение иностранной рабочей силы</a></li>
                        <li><a href="/participant/Закон Республики Казахстан O государственной регистрации юридических лиц и учетной регистрации филиалов и представительств.pdf" download >11.	Закон Республики Казахстан O государственной регистрации юридических лиц и учетной регистрации филиалов и представительств</a></li>
                        <li><a href="/participant/Закон РК О товариществах с ограниченной и дополнительной ответственностью.pdf" download >12.	Закон РК О товариществах с ограниченной и дополнительной ответственностью</a></li>
                        <li><a href="/participant/Закон Республики Казахстан О частном предпринимательстве.pdf" download >13.	Закон Республики Казахстан О частном предпринимательстве</a></li>
                        <li><a href="/participant/Правила выдачи виз Республики Казахстан.pdf" download >14.	Правила выдачи виз Республики Казахстан</a></li>
                        <li><a href="/participant/Правила об организации деятельности «одного окна» для инвесторов.pdf" download >15.	Правила об организации деятельности «одного окна» для инвесторов</a></li>
                    </ul>
                </div>
            </div>
            <div id="tab33" class="tab_content1">
                <div class="participants__list">
                    <ul class="tabs2">
                        <li class="participants__item active2" rel="tab01"><div class="participants__link">Состояние инфраструктуры</div></li>
                        <li class="participants__item" rel="tab02"><div class="participants__link">Тарифы</div></li>
                    </ul>
                </div>
                <div class="tab_container2">
                    <section id="tab01" class="participants__block tab_content2">
                        <div class="participants__scroll">
                            <div class="participants__info">
                                <img width="680" src="/images/table.png" alt="">
                                <div class="participants__download">
                                    <a class="participants__download_link" href="/documents/table.pptx" download>Cкачать</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="tab02" class="participants__block tab_content2">
                        <div class="participants__scroll">
                            <div class="participants__info">
                                <p>Тарифы на 27.07.2017г.</p>
                                <p>Индустриальная зона «Оңтүстік»</p>
                                <div>•	Электричество–12,50 тг/кВт;</div>
                                <div>•	Газ–34,75 тг/м3</div><br>
                                <p>Индустриальная зона «Тассай»</p>
                                <div>•	Электричество–22 тг/кВт;</div>
                                <div>•	Газ–34,75 тг/м3</div><br>
                                <p>Индустриальная зона «КТИЗ Бадам»</p>
                                <div>•	Электричество:</div>
                                <ul>
                                    <li>7:00–19:00–22,13 тг/кВт</li>
                                    <li>19:00–23:00–53,75 тг/кВт</li>
                                    <li>23:00–7:00–7,24 тг/кВт</li>
                                </ul>
                                <div>•	Газ–34,75 тг/м3</div><br>
                                <p>Индустриальная зона «Казыгурт»</p>
                                <div>•	Электричество–22 тг/кВт;</div>
                                <div>•	Газ–34,75 тг/м3</div>
                                <div>•	Вода–252,8 тг/м3</div><br>
                                <p>Индустриальная зона «Тюлькубас»</p>
                                <div>•	Электричество–22 тг/кВт;</div>
                                <div>•	Газ–34,75 тг/м3.</div><br>
                                <p>Индустриальная зона «Шардара»</p>
                                <div>•	Электричество–22 тг/кВт</div><br>
                                <p>Индустриальная зона «Махтаарал»</p>
                                <div>•	Электричество–22 тг/кВт;</div>
                                <div>•	Газ–34,75 тг/м3</div>
                                <div>•	Вода–100 тг/м3</div><br>
                                <p>Индустриальная зона «Созак»</p>
                                <div>•	Электричество:</div>
                                <ul>
                                    <li>7:00–19:00–22,13 тг/кВт</li>
                                    <li>19:00–23:00–53,75 тг/кВт</li>
                                    <li>23:00–7:00–7,24 тг/кВт</li>
                                </ul><br>
                                <p>Индустриальная зона «Туркестан»</p>
                                <div>•	Электричество:</div>
                                <ul>
                                    <li>С 7:00 до 19:00 за 1кВт – 22,13тг</li>
                                    <li>С 19:00 до 23:00 за 1кВт – 53,75тг</li>
                                    <li>С 23:00 до 7:00 за 1кВт – 7,24тг</li>
                                </ul><br>
                                <p>Индустриальная зона «Кентау»</p>
                                <div>•	Электричество–14, 29/кВт;</div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>
</div>
<section class="stages-implementation">
    <div class="container">
        <img src="/images/stages-implementation.png" alt="" class="stages-implementation__img">
    </div>
</section>
<section class="company-indicators">
    <div class="container">
        <div class="company-indicators__title">показатели компании</div>
    </div>
    <div class="container">
        <div class="indicators__list">
            <div class="indicators__item">
                <div class="indicators__item_title">
                    Состояние  <br>
                    индустриальных зон
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">9</div>
                        </div>
                        <div class="indicators-progress__text"><small>Действующие</small></div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 20%">2</div>
                        </div>
                        <div class="indicators-progress__text"><small>Строящиеся</small></div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 10%">1</div>
                        </div>
                        <div class="indicators-progress__text"><small>Планируемые</small></div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    Количество<br>
                    реализованных проектов
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 77%">13 проектов</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 80%">16 проектов</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 82%">17 проектов</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">10 проектов</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2013</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 60%">7 проектов</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2012</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 62%">8 проектов</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2011</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 55%">4 проекта</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2010</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 50%">3 проекта</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    количество <br>
                    созданных рабочих мест
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2017</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 58%">462 рабочих места</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2016</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">618 рабочих мест</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2015</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 85%">1043 рабочих места</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2014</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 78%">745 рабочих мест</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2013</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 65%">590 рабочих мест</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2012</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 52%">336 рабочих мест</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2011</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 50%">335 рабочих мест</div>
                        </div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title">2010</div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 54%">436 рабочих места</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="indicators__item">
                <div class="indicators__item_title">
                    Количество проектов <br>
                    по отраслям
                </div>
                <ul class="indicators-progress__list">
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 55%">9</div>
                        </div>
                    </li>
                    <div class="indicators-progress__text"><small>по отраслям</small></div>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 89%">33</div>
                        </div>
                        <div class="indicators-progress__text"><small>Стройиндустрия и деревообработка </small></div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">12</div>
                        </div>
                        <div class="indicators-progress__text"><small>Производство резинотехнических и пластмассовых изделий </small></div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 55%">9</div>
                        </div>
                        <div class="indicators-progress__text"><small>Химическая</small></div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 70%">12</div>
                        </div>
                        <div class="indicators-progress__text"><small>Металлургия</small></div>
                    </li>
                    <li class="indicators-progress__item">
                        <div class="indicators-progress__title"></div>
                        <div class="indicators-progress__block">
                            <div class="indicators-progressed" style="width: 50%">8</div>
                        </div>
                        <div class="indicators-progress__text"><small>Машиностроение</small></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?= ParentsWidget::widget();?>
<?= FeedbackWidget::widget();?>