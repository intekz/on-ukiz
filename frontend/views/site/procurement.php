<?php

/* @var $this yii\web\View */

use frontend\widgets\SubsidiaryWidget;
use yii\helpers\Url;

$this->title = 'УК "Транспортно-логистическая зона"';
?>
<div class="container">
    <section class="procurement">
        <div class="procurement-title">ЗАКУПКИ</div>
        <div class="procurement-left">
            <div class="procurement-nav">
                <ul class="procurement-nav__list">
                    <li><a href="#">Реализованный</a></li>
                    <li><a href="#">Реализуемые</a></li>
                    <li><a href="#">Планируемые</a></li>
                    <li><a href="#">Требующие инвестиций</a></li>
                </ul>
                <div class="calendar-wrapper">
                    <div id="divCal">
                        <table><thead><tr><td colspan="7">Октября 2017</td></tr></thead><tbody><tr class="days"><td>Пн</td><td>Вт</td><td>Ср</td><td>Чт</td><td>Пт</td><td>Сб</td><td>Вс</td></tr><tr><td class="normal">1</td><td class="normal">2</td><td class="normal">3</td><td class="normal">4</td><td class="normal">5</td><td class="today">6</td><td class="normal">7</td></tr><tr><td class="normal">8</td><td class="normal">9</td><td class="normal">10</td><td class="normal">11</td><td class="normal">12</td><td class="normal">13</td><td class="normal">14</td></tr><tr><td class="normal">15</td><td class="normal">16</td><td class="normal">17</td><td class="normal">18</td><td class="normal">19</td><td class="normal">20</td><td class="normal">21</td></tr><tr><td class="normal">22</td><td class="normal">23</td><td class="normal">24</td><td class="normal">25</td><td class="normal">26</td><td class="normal">27</td><td class="normal">28</td></tr><tr><td class="normal">29</td><td class="normal">30</td><td class="normal">31</td><td class="not-current">1</td><td class="not-current">2</td><td class="not-current">3</td><td class="not-current">4</td></tr></tbody></table>
                    </div>
                </div>
            </div>
        </div>
        <div class="procurement-content">
            <div class="procurement__list">
                <div class="procurement__item">
                    <div class="procurement__item_title">
                        <a href="<?= Url::to(['/procurement-item'])?>" class="procurement__item_title_text">
                            ТОО «Управляющая компания индустриальными зонами «Оңтүстік» объявляет конкурс по определению партнеров для осуществления услуги по техническому надзору участникам Индустриальных зон Южно-Казахстанской
                        </a>
                    </div>
                    <div class="procurement__item_info">
                        <div class="procurement-item__info_desc">
                            Заявки принимается до 17 часов 00 минут
                            04 апреля 2017 года, по адресу ЮКО,
                            г. Шымкент, территория бывшего фосфорного
                            завода, б/н, здание индустриальной зоны
                            «Оңтүстік», кабинет 103.
                        </div>
                    </div>
                </div>
                <div class="procurement__item">
                    <div class="procurement__item_title">
                        <a href="<?= Url::to(['/procurement-item'])?>" class="procurement__item_title_text">
                            ТОО «Управляющая компания индустриальными зонами «Оңтүстік» объявляет конкурс по определению партнеров для осуществления услуги по техническому надзору участникам Индустриальных зон Южно-Казахстанской
                        </a>
                    </div>
                    <div class="procurement__item_info">
                        <div class="procurement-item__info_desc">
                            Заявки принимается до 17 часов 00 минут
                            04 апреля 2017 года, по адресу ЮКО,
                            г. Шымкент, территория бывшего фосфорного
                            завода, б/н, здание индустриальной зоны
                            «Оңтүстік», кабинет 103.
                        </div>
                    </div>
                </div>
                <div class="procurement__item">
                    <div class="procurement__item_title">
                        <a href="<?= Url::to(['/procurement-item'])?>" class="procurement__item_title_text">
                            ТОО «Управляющая компания индустриальными зонами «Оңтүстік» объявляет конкурс по определению партнеров для осуществления услуги по техническому надзору участникам Индустриальных зон Южно-Казахстанской
                        </a>
                    </div>
                    <div class="procurement__item_info">
                        <div class="procurement-item__info_desc">
                            Заявки принимается до 17 часов 00 минут
                            04 апреля 2017 года, по адресу ЮКО,
                            г. Шымкент, территория бывшего фосфорного
                            завода, б/н, здание индустриальной зоны
                            «Оңтүстік», кабинет 103.
                        </div>
                    </div>
                </div>
                <div class="procurement__item">
                    <div class="procurement__item_title">
                        <a href="<?= Url::to(['/procurement-item'])?>" class="procurement__item_title_text">
                            ТОО «Управляющая компания индустриальными зонами «Оңтүстік» объявляет конкурс по определению партнеров для осуществления услуги по техническому надзору участникам Индустриальных зон Южно-Казахстанской
                        </a>
                    </div>
                    <div class="procurement__item_info">
                        <div class="procurement-item__info_desc">
                            Заявки принимается до 17 часов 00 минут
                            04 апреля 2017 года, по адресу ЮКО,
                            г. Шымкент, территория бывшего фосфорного
                            завода, б/н, здание индустриальной зоны
                            «Оңтүстік», кабинет 103.
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                <ul>
                    <li><a class="active" href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                </ul>
            </div>
        </div>
    </section>
</div>

