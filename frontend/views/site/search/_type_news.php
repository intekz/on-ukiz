<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $model \common\models\Search */
?>
<div class="search__item">
    <?= Html::a($model->news->getTitleTranslate(), ['/news/view', 'slug' => $model->slug], ['class' => 'search__title'])?>
    <?= Html::a(Yii::t('site', 'Media'), ['/news/index'], ['class' => 'search__category'])?>
    <div class="search__desc"><?= StringHelper::truncate($model->news->getTextTranslate(), 500, false, null,true);?></div>
</div>
