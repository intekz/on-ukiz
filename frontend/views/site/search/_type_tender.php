<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $model \common\models\Search */
?>
<div class="search__item">
    <?= Html::a($model->tender->getNameTranslate(), ['/tender/view', 'slug' => $model->slug], ['class' => 'search__title'])?>
    <?= Html::a(Yii::t('site', 'Procurement'), ['/tender/index'], ['class' => 'search__category'])?>
    <div class="search__desc"><?= StringHelper::truncate($model->tender->getDescTranslate(), 500, false, null,true);?></div>
</div>
