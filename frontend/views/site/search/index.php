<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $siteSearch \frontend\models\SiteSearch */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $model \common\models\Search */

$this->title = 'Результаты';
$this->params['q'] = $siteSearch->q;
?>
<div class="search__breadcrumbs">
    <div class="container">
        <div class="search__page_name">
            <?= Yii::t('site', 'Search result')?>:
            <div>"<?= Html::encode($siteSearch->q)?>"</div>
        </div>
    </div>
</div>
<div class="container">
    <div class="search">
        <?php if ($provider->getModels()): ?>
            <?php foreach ($provider->getModels() as $model): ?>
                <?= $this->render('_type_' . $model->getTypeLabel(), [
                    'model' => $model
                ])?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?= LinkPager::widget([
            'options' => ['class' => 'pagination'],
            'pagination' => $provider->getPagination(),
            'registerLinkTags' => true,
        ]) ?>
    </div>
</div>
