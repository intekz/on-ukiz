<?php

/* @var $this yii\web\View */


use frontend\widgets\SubsidiaryWidget;

$this->title = 'Приватизация';
?>
<div class="site-industrial">
    <div class="container">
        <div class="industrial__title">ИНДУСТРИАЛЬНЫЕ ЗОНЫ "ONTUSTIK"</div>
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/iousr4x13E4?rel=0" frameborder="0" allowfullscreen></iframe>
        <div class="industrial__list">
            <ul>
                <li class="industrial__item"><a class="industrial__link" href="#">Общая информация</a></li>
                <li class="industrial__item"><a class="industrial__link" href="#">Список проектов</a></li>
                <li class="industrial__item"><a class="industrial__link" href="#">Информация о районе</a></li>
            </ul>
        </div>
        <section class="industrial__block">
            <ul class="tabs">
                <li class="active" rel="tab1">
                    <div class="industrial__img">
                        <div href="#" class="industrial__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
                <li rel="tab1" class="">
                    <div class="industrial__img">
                        <div href="#" class="industrial__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
                <li rel="tab1" class="">
                    <div class="industrial__img">
                        <div href="#" class="industrial__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
                <li rel="tab1" class="tab_last">
                    <div class="industrial__img">
                        <div href="#" class="industrial__img_min" style="background-image: url('/images/project-item.png')"></div>
                    </div>
                </li>
            </ul>
            <div class="tab_container">
                <div id="tab1" class="tab_content industrial__block_typography" style="display: block;">
                    <div>
                        <p>Статус - действующая</p>
                        <p>Общая площадь территории индустриальной зоны - 337 га</p>
                        <p>Рабочие месиа, чел. - 3447</p>
                        <p>Количество проектов -85</p>
                    </div>
                    <div>
                        <p>Тарифы</p>
                        <ul>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                        </ul>
                    </div>
                    <div>
                        <p>Индустриальная зона</p>
                        <ul>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                            <li>Электричество -12,50тг/кВт;</li>
                            <li>Газ - 34,75 тг/м3</li>
                        </ul>
                    </div>
                </div>
                <div id="tab2" class="tab_content" style="display: none;">

                </div>
                <div id="tab3" class="tab_content" style="display: none;">

                </div>
                <div id="tab4" class="tab_content" style="display: none;">

                </div>
            </div>
        </section>
    </div>
</div>