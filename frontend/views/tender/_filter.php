<?php

use common\models\Tender;
use frontend\widgets\CalendarWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \frontend\models\TenderFilter */
?>
<div class="procurement-left">
    <div class="procurement-nav">
        <ul class="procurement-nav__list">
            <?php foreach (Tender::getTypes() as $k => $type): ?>
                <li><a href="<?= Url::to(['index', 'TenderFilter[type]' => $k, 'TenderFilter[date]' => $model->date])?>"><?= $type; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div class="calendar-wrapper">
            <div id="divCal">
                <?= CalendarWidget::widget(['model' => $model]) ?>
            </div>
        </div>
    </div>
</div>