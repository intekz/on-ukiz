<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\Tender */
?>
<div class="procurement__item">
    <div class="procurement__item_title">
        <a href="<?= Url::to(['/tender/view', 'slug' => $model->slug])?>" class="procurement__item_title_text">
            <?= $model->getNameTranslate() ?>
        </a>
    </div>
    <div class="procurement__item_info">
        <div class="procurement-item__info_desc">
            <?= $model->getDescTranslate() ?>
        </div>
    </div>
</div>