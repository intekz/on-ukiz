<?php

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $tenderFilter \frontend\models\TenderFilter */

use frontend\widgets\SubsidiaryWidget;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Закупки - СОЦИАЛЬНО-ПРЕДПРИНИМАТЕЛЬСКАЯ КОРПОРАЦИЯ «ШЫМКЕНТ»';
?>
<div class="container">
    <section class="procurement">
        <div class="procurement-title"><?= Yii::t('site', 'Procurement')?></div>
        <?= $this->render('_filter', ['model' => $tenderFilter])?>

        <div class="procurement-content">
            <div class="procurement__list">
                <?php foreach ($dataProvider->getModels() as $model): ?>
                    <?= $this->render('_item', ['model' => $model])?>
                <?php endforeach;?>
            </div>
            <?= LinkPager::widget([
                'options' => ['class' => 'pagination'],
                'pagination' => $dataProvider->getPagination(),
                'registerLinkTags' => true,
            ]) ?>
        </div>

    </section>

</div>

