<?php
/* @var $this yii\web\View */
/* @var $model \common\models\Tender */
?>
<div class="procurement-item">
    <div class="procurement-item__breadcrumbs">
        <div class="container">
            <div class="procurement-item__page_name">
                <?= $model->getNameTranslate(); ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="procurement-item__page_desc">
            <?= $model->getDescTranslate(); ?>
        </div>
        <?php if ($model->getTenderDocs()):?>
        <ul class="procurement-item__list">
            <?php foreach ($model->tenderDocsLanguage as $doc): ?>
                <li class="<?= $doc->type ?>"><a href="<?= $doc->getFileUrl()?>" download><?= $doc->name?></a></li>
            <?php endforeach; ?>
        </ul>
        <?php endif;?>
    </div>
</div>
