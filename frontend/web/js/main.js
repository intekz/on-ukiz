$('.main-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    dots:true,
    items:1,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false
});
/* Tabs
_______________________________*/

$(".tab_content").hide();
$(".tab_content:first").show();
/* if in tab mode */
$("ul.tabs li").click(function() {
    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#"+activeTab).fadeIn();
    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");
    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
});
$(".tab_drawer_heading").click(function() {
    $(".tab_content").hide();
    var d_activeTab = $(this).attr("rel");
    $("#"+d_activeTab).fadeIn();
    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");
    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
});
$('ul.tabs li').last().addClass("tab_last");
/* Tabs 1
_______________________________*/

$(".tab_content1").hide();
$(".tab_content1:first").show();
/* if in tab mode */
$("ul.tabs1 li").click(function() {
    $(".tab_content1").hide();
    var activeTab1 = $(this).attr("rel");
    $("#"+activeTab1).fadeIn();
    $("ul.tabs1 li").removeClass("active1");
    $(this).addClass("active1");
    $(".tab_drawer_heading1").removeClass("d_active1");
    $(".tab_drawer_heading1[rel^='"+activeTab1+"']").addClass("d_active1");
});
$(".tab_drawer_heading1").click(function() {
    $(".tab_content1").hide();
    var d_activeTab1 = $(this).attr("rel");
    $("#"+d_activeTab1).fadeIn();
    $(".tab_drawer_heading1").removeClass("d_active1");
    $(this).addClass("d_active1");
    $("ul.tabs1 li").removeClass("active1");
    $("ul.tabs1 li[rel^='"+d_activeTab1+"']").addClass("active1");
});
$('ul.tabs1 li').last().addClass("tab_last1");
/* Tabs 2
_______________________________*/

$(".tab_content2").hide();
$(".tab_content2:first").show();
/* if in tab mode */
$("ul.tabs2 li").click(function() {
    $(".tab_content2").hide();
    var activeTab2 = $(this).attr("rel");
    $("#"+activeTab2).fadeIn();
    $("ul.tabs2 li").removeClass("active2");
    $(this).addClass("active2");
    $(".tab_drawer_heading2").removeClass("d_active2");
    $(".tab_drawer_heading2[rel^='"+activeTab2+"']").addClass("d_active2");
});
$(".tab_drawer_heading2").click(function() {
    $(".tab_content2").hide();
    var d_activeTab2 = $(this).attr("rel");
    $("#"+d_activeTab2).fadeIn();
    $(".tab_drawer_heading2").removeClass("d_active2");
    $(this).addClass("d_active2");
    $("ul.tabs2 li").removeClass("active2");
    $("ul.tabs2 li[rel^='"+d_activeTab2+"']").addClass("active2");
});
$('ul.tabs2 li').last().addClass("tab_last2");
/* Scroll
_______________________________*/
$(document).ready(function(){
    $("#menu").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1000 мс
        $('body,html').animate({scrollTop: top}, 1000);
    });
});