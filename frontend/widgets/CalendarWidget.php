<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class CalendarWidget
 * @package frontend\widgets
 */
class CalendarWidget extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('calendar', [
            'model' => $this->model
        ]);
    }
}