<?php

namespace frontend\widgets;

use frontend\models\ContactForm;
use yii\base\Widget;

/**
 * Class FeedbackWidget
 * @package frontend\widgets
 */
class FeedbackWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $model = new ContactForm();

        return $this->render('feedback', [
            'model' => $model
        ]);
    }
}