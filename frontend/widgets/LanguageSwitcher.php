<?php
namespace frontend\widgets;

use Yii;
use yii\widgets\Menu;

/**
 * Class LanguageSwitcher
 * @package frontend\widgets
 */
class LanguageSwitcher extends Menu
{
	public $options = ['class' => 'header-middle__language_list'];
	public $itemOptions = ['class' => 'header-middle__language_item'];

	private static $_labels;
	private $_isError;

	public function init()
	{
		$route = Yii::$app->controller->route;
		$appLanguage = Yii::$app->language;
		$params = $_GET;
		$this->_isError = $route === Yii::$app->errorHandler->errorAction;

		array_unshift($params, '/' . $route);

		foreach (Yii::$app->urlManager->languages as $language) {
			$isWildcard = substr($language, -2) === '-*';
			if ($language === $appLanguage || $isWildcard && substr($appLanguage, 0, 2) === substr($language, 0, 2)) {
				$activeItem = true;
			} else {
				$activeItem = null;
			}
			if ($isWildcard) {
				$language = substr($language, 0, 2);
			}
			$params['language'] = $language;
			$this->items[] = [
				'label' => self::label($language),
				'url' => $params,
				'active' => $activeItem
			];
		}
		parent::init();
	}

	public function run()
	{
        return parent::run();

	}

	public static function label($code)
	{
		if (self::$_labels === null) {
			self::$_labels = [
				'ru' => Yii::t('language', 'Ru'),
				'kz' => Yii::t('language', 'Kz'),
				'en' => Yii::t('language', 'En'),
			];
		}

		return isset(self::$_labels[$code]) ? self::$_labels[$code] : null;
	}
}