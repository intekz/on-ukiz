<?php
namespace frontend\widgets;


use common\models\News;
use yii\base\Widget;

/**
 * Class LastNewsWidget
 * @package frontend\widgets
 */
class LastNewsWidget extends Widget
{
	public $currentNewsId;

	public $limit = 3;

	public function init()
	{
		parent::init();
	}

	public function run()
	{
		$news = News::find()
            ->andWhere(['status' => News::STATUS_PUBLISHED])
			->andFilterWhere(['not in', 'id', $this->currentNewsId])
			->orderBy(['fixed' => SORT_DESC, 'id' => SORT_DESC])
			->limit($this->limit)
			->all();

		return $this->render('last-news', ['news' => $news]);
	}
}