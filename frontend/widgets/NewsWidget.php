<?php

namespace frontend\widgets;

use common\models\News;
use yii\base\Widget;
use yii\helpers\VarDumper;

/**
 * Class NewsWidget
 * @package frontend\widgets
 */
class NewsWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $news = News::find()
            ->andWhere(['status' => News::STATUS_PUBLISHED])
            ->orderBy(['fixed' => SORT_DESC, 'id' => SORT_DESC])
            ->limit(5)
            ->all();

        return $this->render('news', [
            'news' => $news,
        ]);
    }
}