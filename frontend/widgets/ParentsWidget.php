<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class ParentsWidget
 * @package frontend\widgets
 */
class ParentsWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {

        return $this->render('parents');
    }
}