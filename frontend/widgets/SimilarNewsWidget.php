<?php

namespace frontend\widgets;

use common\models\News;
use yii\base\Widget;
use yii\helpers\VarDumper;

/**
 * Class SimilarNewsWidget
 * @package frontend\widgets
 */
class SimilarNewsWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $news = News::find()
            ->where(['status' => News::STATUS_PUBLISHED])
            ->limit(3)
            ->all();
        return $this->render('similar-news', [
            'news' => $news,
        ]);
    }
}