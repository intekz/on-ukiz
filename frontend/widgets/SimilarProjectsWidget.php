<?php
/**
 * Created by PhpStorm.
 * User: alexssdd
 * Date: 11.01.18
 * Time: 16:48
 */

namespace frontend\widgets;


use common\models\Project;
use yii\jui\Widget;

class SimilarProjectsWidget extends Widget
{
    public $currentIndustryId;
    public $currentProjectId;
    public $limit;

    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $projects = Project::find()
            ->published()
            ->andFilterWhere(['not in', 'id', $this->currentProjectId])
            ->andFilterWhere(['industry_id' => $this->currentIndustryId])
            ->orderBy(['id' => SORT_DESC])
            ->limit($this->limit)
            ->all();

        return $this->render('similar-projects', [
            'projects' => $projects,
        ]);
    }
}