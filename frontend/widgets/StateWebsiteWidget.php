<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class StateWebsiteWidget
 * @package frontend\widgets
 */
class StateWebsiteWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {

        return $this->render('state-website');
    }
}