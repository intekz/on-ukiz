<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class SubsidiaryWidget
 * @package frontend\widgets
 */
class SubsidiaryWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {

        return $this->render('subsidiary');
    }
}