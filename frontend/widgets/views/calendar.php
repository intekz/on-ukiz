<?php
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form \yii\widgets\ActiveForm */
?>
<div class="well well-sm" style="background-color: #fff; width:210px">
    <?php $form = ActiveForm::begin();?>
    <?= $form->field($model, 'date')->widget(DatePicker::className(), [
        'type' => DatePicker::TYPE_INLINE,
        'value' => $model->date,
	    'language' => Yii::$app->language == 'kz' ? 'kk' : Yii::$app->language,
        'pluginOptions' => [
            'todayHighlight' => true,
            'format' => 'dd.M.yyyy'
        ],
        'options' => ['id' => 'calendar-date-picker'],
    ])->label(false)?>
    <?php ActiveForm::end(); ?>
</div>
<?php
$formName = $model->formName();
$url = Url::to(['index',   $formName . '[type]' => $model->type, $formName . '[date]' => '']);

$js = <<<JS

$('#calendar-date-picker-kvdate').on('changeDate', function(e) {
    var date = new Date(e.date);
    var dateString;
    
    dateString = ('0' + date.getDate()).slice(-2) + '.'
             + ('0' + (date.getMonth()+1)).slice(-2) + '.'
             + date.getFullYear();
    
    // var convertDate = date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear();
    var url = "$url" + dateString;
    
    window.location.href = url;
});
JS;
$this->registerJs($js)
?>