<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<section class="feedback" id="contact">
    <div class="feedback__title"><?= Yii::t('site', 'Contact Us')?></div>
    <div class="feedback__desc">
        <?= Yii::t('site', 'You can post a wish or ask a question - online. To do this, fill in all necessary forms and click on the "Send" button.')?>
    </div>
    <div class="container">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/site/feedback']),
            'id' => 'feedback-form'
        ])?>
            <div class="feedback__form_block">
                <?= $form->field($model, 'message')->textarea(['rows' => 9, 'class' => 'feedback__input']) ?>
            </div>
            <div class="feedback__form_block">
                <?= $form->field($model, 'name')->textInput(['class' => 'feedback__input']) ?>
                <?= $form->field($model, 'city')->textInput(['class' => 'feedback__input']) ?>
                <?= $form->field($model, 'email')->textInput(['class' => 'feedback__input']) ?>
            </div>
            <div class="feedback__btn">
                <?= Html::submitButton( Yii::t('feedback', 'Send'), ['name' => 'feedback-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>