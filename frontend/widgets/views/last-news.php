<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $news \common\models\News[] */

?>
<?php if ($news):?>
	<div class="news-similar">
		<div class="news-similar__title">Последии новости:</div>
		<?php foreach ($news as $model): ?>
			<div class="news-similar__item">
				<a class="media__item__img" href="<?= Url::to(['/news/view', 'slug' => $model->slug]) ?>" style="background-image: url('<?= $model->getImgUrl()?>')">
					<div class="news__item_info">
						<div class="news__item_date"><?= Yii::$app->formatter->asDate($model->created_at, 'dd/MM/yyyy') ?></div>
						<div class="news__item__title">
							<?= $model->title ? $model->title : $model->translate($model->getDefaultLanguage())->title ?>
						</div>
					</div>
				</a>
			</div>
		<?php endforeach;?>
		<div class="news__btn">
			<a class="news__btn_link" href="<?= Url::to(['/news/index']) ?>">Смотреть все новости</a>
		</div>
	</div>
<?php endif;?>