<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $news \common\models\News[] */
?>
<?php if ($news):?>
    <section class="news">
        <div class="news__title"><?= Yii::t('site', 'News')?></div>
        <div class="news__btn">
            <a class="news__btn_link" href="<?= Url::to(['/news/index'])?>"><?= Yii::t('site', 'View all news')?></a>
        </div>
        <div class="news__list">
            <?php foreach ($news as $model):?>
                <a class="news__item" href="<?= Url::to(['/news/view', 'slug' => $model->slug]) ?>" style="background-image: url('<?= $model->getImgUrl()?>')" data-id="<?= $model->id?>">
                    <div class="news__item_info">
                        <div class="news__item_date"><?= Yii::$app->formatter->asDate($model->created_at, 'dd/MM/yyyy') ?></div>
                        <div class="news__item__title">
                            <?= $model->getTitleTranslate(); ?>
                        </div>
                    </div>
                </a>
            <?php endforeach;?>
        </div>
    </section>
<?php endif;?>