<?php
/**
 * Created by PhpStorm.
 * User: inte
 * Date: 16.10.2017
 * Time: 14:35
 */
?>
<section class="parents">
    <div class="parents-page__title">Партнеры</div>
    <div class="container">
        <div class="parents-carousel owl-carousel">
            <div class="item">
                <a href="#"><img src="/images/parents1.png" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/images/parents2.png" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/images/parents3.png" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/images/parents4.png" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/images/parents5.png" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/images/parents6.png" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/images/parents3.png" alt=""></a>
            </div>
        </div>
    </div>
</section>
<?php
$js = <<<JS
$('.parents-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items:6,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false
});
JS;
$this->registerJs($js)
?>