<?php
/* @var $news \common\models\News[] */

use yii\helpers\Url;

?>
<?php if ($news):?>
    <div class="news-similar">
        <div class="news-similar__title">Последии новости:</div>
        <?php foreach ($news as $item): ?>
            <div class="news-similar__item">
                <a class="media__item__img" href="<?= Url::to(['/news/view', 'slug' => $item->slug]) ?>" style="background-image: url('<?= $item->getImgUrl()?>')">
                    <div class="news__item_info">
                        <div class="news__item_date"><?= Yii::$app->formatter->asDate($item->created_at, 'dd/MM/yyyy') ?></div>
                        <div class="news__item__title">
                            <?= $item->title?>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach;?>
        <div class="news__btn">
            <a class="news__btn_link" href="<?= Url::to(['/news']) ?>">Смотреть все новости</a>
        </div>
    </div>
<?php endif;?>
