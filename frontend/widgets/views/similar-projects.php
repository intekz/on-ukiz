<?php

/* @var $projects \common\models\Project[] */

use yii\helpers\Url;

?>
<div class="similar-projects__title"><?= Yii::t('project', 'Similar projects')?></div>
<?php if ($projects): ?>
    <?php foreach ($projects as $item): ?>
        <div class="similar-projects__item">
                <a href="<?= Url::to(['/projects/view', 'slug' => $item->slug])?>" class="project__item_img" style="background-image: url(<?= $item->getMainImg()?>)"></a>
                <a href="<?= Url::to(['/projects/view', 'slug' => $item->slug])?>" class="project__item_title"><?= $item->getNameTranslate()?></a>
            </div>
    <?php endforeach; ?>
<?php endif; ?>