<?php

?>
    <section class="state-website">
        <div class="container">
            <div class="state-website-carousel owl-carousel">
                <div class="item">
                    <a href="http://adilet.zan.kz/rus" target="_blank"><img src="/images/state1.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="http://kazpatent.kz/ru" target="_blank"><img src="/images/state2.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="http://naceks.kz/ru" target="_blank"><img src="/images/state3.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="http://www.akorda.kz/ru" target="_blank"><img src="/images/123.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="http://egov.kz/cms/ru" target="_blank"><img src="/images/state5.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="https://primeminister.kz/ru" target="_blank"><img src="/images/state6.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="http://ru.memkyzmet.kz/"target="_blank"><img src="/images/state7.png" alt=""></a>
                </div>
                <div class="item">
                    <a href="#"><img src="/images/state8.png" alt=""></a>
                </div>
            </div>
        </div>
    </section>
<?php
$js = <<<JS
$('.state-website-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items:8,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false
});
JS;
$this->registerJs($js)
?>