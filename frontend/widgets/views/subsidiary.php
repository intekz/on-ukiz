<?php
/**
 * Created by PhpStorm.
 * User: inte
 * Date: 02.10.2017
 * Time: 10:27
 */
?>
<section class="subsidiary">
    <div class="subsidiary-carousel owl-carousel">
        <div class="item">
            <a href="#"><img src="/images/subsidiary1.png" alt=""></a>
        </div>
        <div class="item">
            <a href="#"><img src="/images/subsidiary2.png" alt=""></a>
        </div>
        <div class="item">
            <a href="#"><img src="/images/subsidiary3.png" alt=""></a>
        </div>
        <div class="item">
            <a href="#"><img src="/images/subsidiary4.png" alt=""></a>
        </div>
        <div class="item">
            <a href="#"><img src="/images/subsidiary5.png" alt=""></a>
        </div>
    </div>
</section>
<?php
$js = <<<JS
$('.subsidiary-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:false,
    items:5,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false
});
JS;
$this->registerJs($js)
?>